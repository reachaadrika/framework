<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author $author
 */

$name=explode(",",$author['author']); 
?>

<h1 class="display-3 header-txt text-left">View Author Details</h1>

<div class="row justify-content-md-center">
<div class="col-lg boxed">
        <div class="capital-heading">
            <?= __('View Author') ?>
            <?php if ($access_granted): ?>
                <?= $this->Html->link('Edit Author', ['prefix' => 'Admin', 'action' => 'edit', $author->id], ['class' => 'btn btn-warning', 'style' => "float: right;"]) ?>
            <?php endif ?>
        </div>

        <table class="table-bootstrap mx-0">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Full Name') ?></th>
                    <td><?= h($author->author) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('First Name') ?></th>
                    <td>
                    <?php if($author->first != null): 
                    echo($author->first);
                    else:
                    echo($name[1]); 
                    endif; ?>                   
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Last Name') ?></th>
                    <td> 
                    <?php if($author->last != null):
                    echo($author->last);
                    else: 
                    echo($name[0]);
                    endif; ?>
                     </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Institution') ?></th>
                    <td><?= h($author->institution) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Email') ?></th>
                    <td><?= h($author->email) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Last Modified') ?></th>
                    <td><?= h($author->modified) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('ORCID ID') ?></th>
                    <td><?= h($author->orcid_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('East Asian Name Order') ?></th>
                    <td><?= h($author->east_asian_order==0 ? "No" : "Yes") ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="boxed mx-0">
    <?php if (!empty($publications)): ?>
        <div class="capital-heading"><?= __('Related Publications') ?></div>
        <div class="table-responsive">
        <table class="table-bootstrap mx-0">
            <thead>
                <tr>
                    <th scope="col">BibTeX key</th>
                    <th scope="col">Authors</th>
                    <th scope="col">Title</th>
                    <th scope="col">Journal</th>
                    <th scope="col">Designation</th>
                    <th scope="col">Reference</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($publications as $publication): ?>
                <tr>
                    <td>
                        <?= $this->Html->link(
                                        h($publication->bibtexkey),
                                        ['prefix' => false, 'action' => 'view', $publication->id]) ?>
                    </td>
                    <td align="left" nowrap="nowrap">
                        <?php foreach ($publication->authors as $author): ?>
                        <?= $this->Html->link(
                                        h($author->author),
                                        ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $author->id]) ?><br>
                        <?php endforeach ?>
                    </td>
                    <td align="left">
                        <?= (isset($publication->title) and !empty($publication->title)) ? '<b>Title: </b>'.h($publication->title) : '' ?><br>
                        <?= (isset($publication->book_title) and !empty($publication->book_title)) ? '<b>Book Title: </b>'.h($publication->book_title) : '' ?><br>
                        <?= (isset($publication->chapter) and !empty($publication->chapter)) ? '<b>Chapter: </b>'.h($publication->chapter) : '' ?>
                    </td>
                    <td align="left">
                        <?= isset($publication->journal->journal) ? '<b>Journal: </b>'.h($publication->journal->journal) : '' ?><br>
                        <?= (isset($publication->volume) and !empty($publication->volume)) ? '<b>Volume: </b>'.h($publication->book_title) : '' ?><br>
                        <?= (isset($publication->number) and !empty($publication->number)) ? '<b>Number: </b>'.h($publication->chapter) : '' ?>
                    </td>
                    <td><?= h($publication->designation) ?></td>
                    <td>
                        <?= $this->Scripts->formatReference($publication->toArray(), 'bibliography', [
                                'template' => 'chicago-author-date',
                                'format' => 'html'
                            ]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php endif; ?>
</div>

<!-- Staff View -->
<div class="staff view large-9 medium-8 columns content">
    <h3><?= h($staff->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Author') ?></th>
            <td><?= $staff->has('author') ? $this->Html->link($staff->author->id, ['controller' => 'Authors', 'action' => 'view', $staff->author->id]) : '' ?></td>
        </tr>
        <tr>
        <th scope="row"><?= __('Image') ?></th>
        <td>
        <?php
        $url = Router::url('/',true).'webroot/staff-img/'.h($staff->id);
        echo "<img class='staff-img' src='$url' alt='Image' onerror=this.src='/images/default.png'>";
        ?>
        </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Staff Type') ?></th>
            <td><?= $staff->has('staff_type') ? $this->Html->link($staff->staff_type->id, ['controller' => 'StaffTypes', 'action' => 'view', $staff->staff_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sequence') ?></th>
            <td><?= h($staff->sequence) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($staff->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Cdli Title') ?></h4>
        <?= $this->Text->autoParagraph(h($staff->cdli_title)); ?>
    </div>
    <div class="row">
        <h4><?= __('Contribution') ?></h4>
        <?= $this->Text->autoParagraph(h($staff->contribution)); ?>
    </div>
</div>
