<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article $article
 */

use Cake\ORM\TableRegistry;

?>

<div class="row justify-content-md-center">
  <div class="col-lg-10" >
    <h2 class="article_title">
      <?= $article->title ?>              
    </h2>
    <div class="article_table">
      <div class="row">
        <div class="col-8" valign="top" >
          <p class="article_no">
            <?php if ($article->article_type == 'cdlj'){ ?>
             Cuneiform Digital Library Journal  
            <?php }
            elseif ($article->article_type == 'cdlb'){ ?>
             Cuneiform Digital Library Bulletin 
            <?php }
            else { ?>
             Cuneiform Digital Library Note 
            <?php } ?>
          </p>
          <p class="article_no light-text">
            <?php if ($article->article_type == 'cdlj'){ ?>
              ISSN 1540-8779    
            <?php } elseif ($article->article_type == 'cdlb') { ?>
              ISSN 1540-8760  
            <?php } else { ?>
              ISSN 1546-6566 
            <?php } ?>
          </p>
          <p class="article_no">Published on <?= date("Y-m-d", strtotime($article['created'])); ?></p>
          <p class="article_no"><span class="italic_font">© Cuneiform Digital Library Initiative </span></p>
        </div>
        <div class="col-4" nowrap="nowrap" valign="top">
          <div class="article_author">
            <?php foreach ($article['authors'] as $author): ?>
              <div class="article_author1">
                <p class="author1">
                  <?php $name=explode(",",$author['author']);
                  if (!empty($name[1]) && !empty($name[0])){ ?>
                    <span class="bold_font"><?= $name[1].' '.$name[0] ?></span>
                  <?php }
                  else { ?>
                    <span class="bold_font"><?= $author['author'] ?></span>
                  <?php } ?>
                </p>
                <p class="author1 light-text">
                  <?php if (!empty($author['email'])): ?>
                    <span><?= $author->email?></span>
                  <?php endif; ?>
                </p>
                <p class="author1 light-text">
                  <?php if (!empty($author['institution'])): ?>
                    <span class="italic_font light-text"><?= $author->institution ?></span>
                  <?php endif; ?>
                </p>
              </div>
            <?php endforeach; ?>        
          </div>
        </div>
      </div>
      <div class="cycle-tab">
        <ul class="nav nav-tabs mb-3">
          <li class="cycle-tab-item active">
            <a class="nav-link article_link" role="tab" data-toggle="tab" href="#home">Article</a>
          </li>
          <?php if (!empty($article->review_rounds)) {  ?>
          <li class="cycle-tab-item"> 
            <a class="nav-link article_link" role="tab" data-toggle="tab" href="#profile">Reviews</a>
          </li>
          <?php } ?>
          <li class="download_button">
            <button class="btn btn-sm" type="button" ><span class="fa fa-download"></span> Download PDF</button>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <div id="pArticleContent">
              <?= $article->content_html ?>
            </div>
          </div>
          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <?php $sequence = 0;
            $file_revisions = glob("./webroot/pubs/ojs/1/articles/$article->submission_id/submission/review/revision/*.pdf");
            $file_submissions = glob("./webroot/pubs/ojs/1/articles/$article->submission_id/submission/review/*.pdf");
            $sequence_revision = 0;
            $sequence_submission = 0;
            $sequence_file = 0;
            foreach ($article->review_rounds as $review_round): ?>
              <div>
                <h2 class="review_hi">Review Round <?= $review_round->round ?></h2>
                <div >
                  <p >
                    <?php $uploader_id = $ojs_file[$sequence_file]['submission_file']['uploader_user_id']-1; 
                    $date=explode("T",$ojs_file[$sequence_file]['submission_file']['date_uploaded'])?>
                    <span class="bo">Author : </span>
                    <span><?= h($user_ojs_first[$uploader_id]['setting_value'])." ".h($user_ojs_last[$uploader_id]['setting_value']) ?> , <?= h($date[0]) ?> </span>
                    <?php $sequence_file = $sequence_file + 1; ?>
                  </p>
                  <p class="review_fi">
                    <span class="fa fa-paperclip fi_icon"></span>
                    <?php if (!empty($file_submissions)) { ?>
                      <a href=""><?= basename($file_submissions[$sequence_submission]) ?></a>
                      <?php $sequence_submission=$sequence_submission+1; 
                    } else { ?>
                      <a href="">Submission.pdf</a>
                    <?php } ?>
                  </p>
                </div>
                <div>
                  <?php foreach ($review_round->review_assignments as $review_assignment):  ?>
                    <?php $reviewer_id=$review_assignment->reviewer_id-1; ?>
                    <p >
                      <span class="bo">Reviewer : </span>
                      <span> <?= h($user_ojs_first[$reviewer_id]['setting_value'])." ".h($user_ojs_last[$reviewer_id]['setting_value']) ?>, <?= h($review_assignment->date_completed) ?> </span>
                    </p>
                    <div class="comment more">
                      <?= $submission_comment[$sequence]['comments']; ?>
                      <?php $sequence = $sequence + 1; ?>
                    </div>
                  <?php endforeach; ?>
                </div>
                <div>
                  <p>
                    <?php $uploader_revision_id = $ojs_file[$sequence_file]['submission_file']['uploader_user_id']-1;
                    $date=explode("T",$ojs_file[$sequence_file]['submission_file']['date_uploaded'])?>
                    <span class="bo">Author : </span>
                    <span> <?= h($user_ojs_first[$uploader_revision_id]['setting_value'])." ".h($user_ojs_last[$uploader_revision_id]['setting_value']) ?> , <?= h($date[0]) ?> </span>
                    <?php $sequence_file = $sequence_file + 1; ?>
                  </p>
                  <p class="review_fi">
                    <span class="fa fa-paperclip fi_icon" style="color:blue"></span>
                    <?php if (!empty($file_revisions)) { ?>
                      <a href=""><?= basename($file_revisions[$sequence_revision]) ?></a>
                      <?php $sequence_revision=$sequence_revision+1; 
                    } else { ?>
                      <a href="">Revision.pdf</a>
                    <?php } ?>
                  </p>
                </div> 
              </div>
            <?php endforeach; ?>
            <div>
              <div class="accordion journal-accordion">
                <?= $this->Accordion->partOpen('Discussion', 'Discussion', 'h2') ?>
                  <div>
                    <?php foreach ($article->queries as $query):  ?>
                      <?php if ($query->stage_id == '3'): ?>
                        <?php foreach ($query->notes as $note):  ?>
                          <?php if ($note->user_id != 1) { ?>
                            <div class="discuss">
                              <p>
                                <?php $note_author_id = $note->user_id - 1;
                                if (in_array($note->user_id, $reviewer)) { ?>
                                  <span class="bo">Reviewer : </span>
                                <?php } else { ?>
                                  <span class="bo">Author : </span>
                                <?php } ?>
                                <span><?= h($user_ojs_first[$note_author_id]['setting_value'])." ".h($user_ojs_last[$note_author_id]['setting_value']) ?>, <?= $note->date_created ?> </span>
                                </p>
                              <p>
                                <?=  $note->contents ?>                          
                              </p>
                            </div>
                          <?php } ?>
                        <?php endforeach; ?>
                      <?php endif;  ?>
                    <?php endforeach; ?>
                  </div>
                <?= $this->Accordion->partClose() ?>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>   
  </div>   
  <script
    type="text/javascript"
    src="https://code.jquery.com/jquery-3.3.1.min.js"
  ></script>
  <script
    type="text/javascript"
    src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.js"
  ></script>
</div>
<script src="/assets/js/journals_dashboard.js"></script>

