<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Posting $posting
 */

$this->assign('title', $posting->title);
?>
<div class="row justify-content-md-center posting">

    <?php if($posting->posting_type_id == 1){?>
        <div class = "col-lg-5 text-white py-4 px-5 left-panel">
            <?php $image = $this->ArtifactImages->getMainImage($posting->artifact_id); ?><br/>
            <img
                src = '<?=$image['thumbnail']?>'
                alt = '<?= $posting->title.' ';?> '
                class="img-fluid">
            <div class="pt-2 text-left"><?= date_format($posting->publish_start, 'F d, Y') ?></div>
        </div>

        <div class = "col-lg-7 padding px-5 right-panel">
            <h1 class="text-left"><?= $this->Text->autoParagraph(h($posting->title)); ?></h1>
            <div class="pt-3 text-justify"><?= html_entity_decode($posting->body); ?></div><br/>
            <div class="text-left"><strong><?= __('Posted By : ') ?><?= $this->Html->link("Author", ['controller' => 'authors', 'action' => 'view', $posting->created_by]);?></strong></div>
        </div>

    <?php } else if($posting->posting_type_id == 2){?>
        <div class = "col-lg px-4">
            <h1 class="text-left display-3"><?= $this->Text->autoParagraph(h($posting->title)); ?></h1>
            <div class="mt-4 text-justify"><?= html_entity_decode($posting->body); ?></div><br/>
            <div class="text-left"><strong><?= __('Created By : ') ?><?= $this->Html->link("Author", ['controller' => 'authors', 'action' => 'view', $posting->created_by]);?></strong></div><br/>
            <div class="text-left"><strong><?= __('Modified By : ') ?><?= $this->Html->link("Author", ['controller' => 'authors', 'action' => 'view', $posting->modified_by]);?></strong></div>
        </div>

    <?php } else if($posting->posting_type_id == 3){?>
        <div class = "col-lg-5 text-white py-4 px-5 left-panel">
            <div>
                <?php $image = $this->ArtifactImages->getMainImage($posting->artifact_id); ?><br/>
                <img
                    src = '<?=$image['thumbnail']?>'
                    alt = '<?= $posting->title.' ';?> '
                    class="img-fluid"><br/><br/>
            </div>
            <strong><?= $this->Html->link("See the artifact", [
                        'controller' => 'artifacts', 'action' => 'view', $posting->artifact_id
                    ], [
                        'class' => 'pt-3 text-white'
                    ]
            );?></strong>
        </div>

        <div class = "col-lg-7 padding px-5 right-panel">
            <h1 class="text-left"><?= $this->Text->autoParagraph(h($posting->title)); ?></h1>
            <div class="pt-3 text-justify"><?= html_entity_decode($posting->body); ?></div>
        </div>
    <?php } ?>

</div>
