<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReviewAssignment $reviewAssignment
 * @var \Cake\Collection\CollectionInterface|string[] $submissions
 * @var \Cake\Collection\CollectionInterface|string[] $reviewRounds
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Review Assignments'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="reviewAssignments form content">
            <?= $this->Form->create($reviewAssignment) ?>
            <fieldset>
                <legend><?= __('Add Review Assignment') ?></legend>
                <?php
                    echo $this->Form->control('submission_id', ['options' => $submissions]);
                    echo $this->Form->control('reviewer_id');
                    echo $this->Form->control('competing_interests');
                    echo $this->Form->control('recommendation');
                    echo $this->Form->control('date_assigned', ['empty' => true]);
                    echo $this->Form->control('date_notified', ['empty' => true]);
                    echo $this->Form->control('date_confirmed', ['empty' => true]);
                    echo $this->Form->control('date_completed', ['empty' => true]);
                    echo $this->Form->control('date_acknowledged', ['empty' => true]);
                    echo $this->Form->control('date_due', ['empty' => true]);
                    echo $this->Form->control('date_response_due', ['empty' => true]);
                    echo $this->Form->control('last_modified', ['empty' => true]);
                    echo $this->Form->control('reminder_was_automatic');
                    echo $this->Form->control('declined');
                    echo $this->Form->control('cancelled');
                    echo $this->Form->control('reviewer_file_id');
                    echo $this->Form->control('date_rated', ['empty' => true]);
                    echo $this->Form->control('date_reminded', ['empty' => true]);
                    echo $this->Form->control('quality');
                    echo $this->Form->control('review_round_id', ['options' => $reviewRounds, 'empty' => true]);
                    echo $this->Form->control('stage_id');
                    echo $this->Form->control('review_method');
                    echo $this->Form->control('round');
                    echo $this->Form->control('step');
                    echo $this->Form->control('review_form_id');
                    echo $this->Form->control('unconsidered');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
