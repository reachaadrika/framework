<?php

    // Returns modified Advance Search Link
    function modifyAdvancedSearchLike($searchableFields) {
        $newGET = [];

        foreach ($_GET as $param => $value) {
            if (in_array($param, $searchableFields)) {
                $newGET[$param] = $value;
            }
        }

        return $newGET;
    }

    // When page per size changes
    function modifyPageNumberIfPerSizeChanges($pageSize) {
        if (array_key_exists('Page',$_GET)) {
            unset($_GET['Page']);
        }

        $newGET = array_merge($_GET, ['PageSize' => $pageSize]);

        return $newGET;
    }

    //highlight matching inscriptions
    function highlightInscriptions($line, $inscriptionsQuery) {
        $inscriptionsKeywords = preg_split('/\s+/', $inscriptionsQuery);
        foreach($inscriptionsKeywords as $keyword) {
            if (empty($keyword)) {
                continue;
            }
            $pattern = '/'.$keyword.'/';
            $line = preg_replace($pattern, '<span style="background-color: #FFFF00;">' . '$0' . '</span>', $line);
        }
        return $line;
    }

    function processInscriptions($line, $inscriptionsQuery) {
        //Translitertaions
        $processedLine = "";
        if (preg_match("/^[0-9]+/", $line)) {
            //Highlight matching inscriptions 
            if(!empty($inscriptionsQuery)) {
                $line = highlightInscriptions($line, $inscriptionsQuery);
            }
            $processedLine = $line."<br>";
        }
        //Translations 
        elseif (preg_match("/^(#tr)/", $line)) {
            $line = substr($line, 5);
            //Highlight matching inscriptions 
            if(!empty($inscriptionsQuery)) {
                $line = highlightInscriptions($line, $inscriptionsQuery);
            }
            $processedLine =  "<i> &emsp;".$line."</i> <br>";
        }
        //Comments
        elseif (preg_match("/^(\$ )/", $line) || preg_match("/^(# )/", $line) ) {
            $line = substr($line, 2);
            //Highlight matching inscriptions 
            if(!empty($inscriptionsQuery)) {
                $line = highlightInscriptions($line, $inscriptionsQuery);
            }
            $processedLine =  "<i> &emsp;".$line."</i><br>";
        }
        //structure
        elseif (preg_match("/^@/", $line))  {
            $line = substr($line, 1);
            //Highlight matching inscriptions
            if(!empty($inscriptionsQuery)) {
                $line = highlightInscriptions($line, $inscriptionsQuery);
            }
            $processedLine =  "<b>".$line."</b><br>";
        }
        return $processedLine;
    }

    // Search Category
    $searchCategory = [
        // $category =. $placeholder
        'keyword' => 'Free Search',
        'publication' => 'Publications',
        'collection' => 'Collections',
        'provenience' => 'Proveniences',
        'period' => 'Periods',
        'inscription' => 'Inscriptions',
        'id' => 'Identification numbers'
    ];

    // Fetch Searched parameters
    $searchBar = [];
    if ($isAdvancedSearch != 1) {
        foreach ($_GET as $key => $value) {
            $checkType = substr($key, 0, -1);

            if (array_key_exists($key, $searchCategory)) {
                array_push($searchBar, [$key, trim($value, '" || \'')]);
            } elseif (array_key_exists($checkType, $searchCategory) || $checkType === 'operator') {
                array_push($searchBar, [$checkType, trim($value, '" || \'')]);
            }
        }
    }

    // Sort By
    $filterOptions = [
        'r' => 'Relevance',
        'l' => 'Lorem',
        'ipsum' => 'dolor'];

    // Full Filter Status
    $fullFilterStatus = $searchSettings['full:sidebar'] && $LayoutType == 1;
    // Compact Filter Status
    $compactFilterStatus = $searchSettings['compact:sidebar'] && $LayoutType == 2;

    $mappingSearchSettings = [
        'object' => [
            'collection' => 'Museum Collections',
            'period' => 'Period',
            'provenience' => 'Provenience',
            'atype' => 'Artifact type',
            'materials' => 'Material',
            'image_type' => 'Image Type',
        ],
        'textual' => [
            'genres' => 'Genre',
            'languages' => 'Language'
        ],
        'publication' => [
            'authors' => 'Authors',
            'year' => 'Date of publication'
        ],
        'inscription' => [
            'transliteration' => 'Transliteration',
            'translation' => 'Translation'
        ]
    ];

    // Default Filters populate
    $filterArray = [
        'object' => [],
        'textual' => [],
        'publication' => [],
        'inscription' => []
    ];

    // Filters applied
    $filteredArray = [
        'object' => [],
        'textual' => [],
        'publication' => [],
        'inscription' => []
    ];

    // Applied Filters
    $appliedFilters = [];

    foreach ($mappingSearchSettings as $type => $subType) {
        foreach ($subType as $key => $value) {
             $prefix = $fullFilterStatus ? 'full:' : ($compactFilterStatus ? 'compact:' : '');

            if (($fullFilterStatus || $compactFilterStatus || !($fullFilterStatus || $compactFilterStatus)) &&  is_array($searchSettings[$prefix.$type]) && in_array($key, $searchSettings[$prefix.$type])) {
                $filterArray[$type] = array_merge_recursive($filterArray[$type], [$key => $filters[$key]]);

                $filterApplied = array_keys(array_filter($filterArray[$type][$key], function ($value) {
                    return $value;
                }));
                $filteredArray[$type] = array_merge_recursive($filteredArray[$type], [$key => $filterApplied ]);

                $appliedFilters = array_merge($appliedFilters, $filterApplied);
            }
        }
    }

    echo $this->Html->css('filter');
    echo $this->Html->script('searchResult.js', ['defer' => true]);

    // By Default Javascript Status set. If <noscript> block is executed, it will be unset.
    $JAVASCRIPT_STATUS = 1;

    // Temporary fix
    $param = 1;
?>

<div class="">
    <h1 class="container px-0 search-heading header-txt display-3 text-left">
        <?= __('Search the CDLI collection')?>
    </h1>
    <?= $this->Form->create(null, [
        'type'=>'GET',
        'url' => [
            'controller' => 'Search',
            'action' => 'index'
        ]
    ]) ?>

        <div>
            <div id="dynamic_field">
                <div>
                    <noscript>
                        <?php $JAVASCRIPT_STATUS = 0; ?>
                        <style>
                            .default-search-block,
                            .default-add-search-btn {
                                display:none;
                            }
                        </style>
                        <div id="dynamic_field">
                            <div>
                                <div class="rectangle-2 container p-3">
                                    <div class="search-page-grid" id="1">
                                        <?= $this->Form->label('input1', null, ['hidden']); ?>
                                        <?= $this->Form->text('', [
                                            'name' => empty($searchBar) ? 'publication' : $searchBar[0][0],
                                            'id' => 'input1',
                                            'value' => empty($searchBar) ? '' : $searchBar[0][1],
                                            'placeholder' => 'Search for publications, provenience, collection no.',
                                            'aria-label' => 'Search',
                                            'required'
                                            ]
                                        ); ?>

                                        <?= $this->Form->label('1', null, ['hidden']); ?>
                                        <?=$this->Form->select(
                                            '', [
                                                // $category =. $placeholder
                                                'keyword' => 'Free Search',
                                                'publication' => 'Publications',
                                                'collection' => 'Collections',
                                                'provenience' => 'Proveniences',
                                                'period' => 'Periods',
                                                'inscription' => 'Inscriptions',
                                                'id' => 'Identification numbers'
                                            ], [
                                                'value' => empty($searchBar) ? 'publication' : $searchBar[0][0],
                                                'class' => 'form-group mb-0',
                                                'id' => '1'
                                            ]
                                        );?>
                                    </div>
                                </div>
                            </div>
                            <?php $extraSearchBox = (sizeof($searchBar) - 1)/2?>
                            <?php for ($i = $extraSearchBox; $i <= 2; $i++):?>
                                <div id="row<?=$i?>">
                                    <div class="container rectangle-23 p-3">
                                        <div class="align-items-baseline">
                                            <?=$this->Form->select(
                                                'operator'.$i, [
                                                    'AND' => 'AND',
                                                    'OR' => 'OR'
                                                ], [
                                                    'value' => sizeof($searchBar) < ($i*2) ? 'AND' : $searchBar[$i*2 - 1][1],
                                                    'class' => 'mb-3 mt-0 cdli-btn-light btn-and mr-3 float-left',
                                                    'id' => $i
                                                ]
                                            );?>

                                            <div class="search-page-grid w-100-sm">
                                                <?= $this->Form->label('input'.($i + 1), null, ['hidden']); ?>
                                                <?= $this->Form->text('', [
                                                        'name' => 'publication'.$i,
                                                        'id' => 'input'.($i + 1),
                                                        'value' => sizeof($searchBar) < ($i*2) ? '' : $searchBar[$i*2][1],
                                                        'placeholder' => 'Search for publications, provenience, collection no.',
                                                        'aria-label' => 'Search'
                                                    ]
                                                ); ?>

                                                <?= $this->Form->label(($i + 1), null, ['hidden']); ?>
                                                <?=$this->Form->select(
                                                    '', [
                                                        // $category =. $placeholder
                                                        'keyword'.$i => 'Free Search',
                                                        'publication'.$i => 'Publications',
                                                        'collection'.$i => 'Collections',
                                                        'provenience'.$i => 'Proveniences',
                                                        'period'.$i => 'Periods',
                                                        'inscription'.$i => 'Inscriptions',
                                                        'id'.$i => 'Identification numbers'
                                                    ], [
                                                        'value' => sizeof($searchBar) < ($i*2) ? 'publication'.$i : $searchBar[$i*2][0].$i,
                                                        'class' => 'form-group mb-0',
                                                        'id' => ($i + 1)
                                                    ]
                                                );?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endfor; ?>
                    </noscript>

                    <div class="rectangle-2 container p-3 default-search-block">
                        <div class="search-page-grid" id="1">

                            <?= $this->Form->label('input1', null, ['hidden']); ?>
                            <?= $this->Form->text('', [
                                    'name' => empty($searchBar) ? "publication" : $searchBar[0][0],
                                    'id' => 'input1',
                                    'value' => empty($searchBar) ? '' : $searchBar[0][1],
                                    'placeholder' => 'Search for publications, provenience, collection no.',
                                    'class' => 'grid-1',
                                    'aria-label' => 'Search',
                                    'required'
                                ]
                            ); ?>

                            <?= $this->Form->label(1, null, ['hidden']); ?>
                            <?=$this->Form->select(
                                '', [
                                    // $category =. $placeholder
                                    'keyword' => 'Free Search',
                                    'publication' => 'Publications',
                                    'collection' => 'Collections',
                                    'provenience' => 'Proveniences',
                                    'period' => 'Periods',
                                    'inscription' => 'Inscriptions',
                                    'id' => 'Identification numbers'
                                ], [
                                    'value' => empty($searchBar) ? 'publication' : $searchBar[0][0],
                                    'class' => 'form-group mb-0 grid-2',
                                    'id' => '1'
                                ]
                            );?>
                        </div>
                    </div>
                </div>
                <?php if((sizeof($searchBar) > 1) && $JAVASCRIPT_STATUS) { ?>
                    <?php $extraSearchBox = (sizeof($searchBar)-1)/2;
                        $extraSearchBox = $extraSearchBox <= 2 ? $extraSearchBox : 2;
                    ?>
                    <?php for ($i = 1; $i <= $extraSearchBox; $i++):?>
                        <div id="row<?=$i?>">
                            <div class="container rectangle-23 p-3">
                                <div class="align-items-baseline">
                                    <?=$this->Form->select(
                                        'operator'.$i,
                                        [
                                            'AND' => 'AND',
                                            'OR' => 'OR'
                                        ],
                                        [
                                            'value' => $searchBar[$i*2 - 1][1],
                                            'class' => 'mb-3 mt-0 cdli-btn-light btn-and mr-3 float-left',
                                            'id' => $i
                                        ]
                                    );?>

                                    <?= $this->Form->button(
                                        '<b>&times;</b>',
                                        [
                                            'class' => 'btn cdli-btn-light ml-3 float-right btn_remove',
                                            'id' => $i,
                                            'escapeTitle' => false,
                                            'name' => 'remove'
                                        ]
                                    ); ?>

                                    <div class="search-page-grid w-100-sm">
                                        <?= $this->Form->label('input'.($i + 1), null, ['hidden']); ?>
                                        <?= $this->Form->text('', [
                                                'name' => 'publication'.$i,
                                                'id' => 'input'.($i + 1),
                                                'value' => $searchBar[$i*2][1],
                                                'placeholder' => 'Search for publications, provenience, collection no.',
                                                'aria-label' => 'Search'
                                            ]
                                        ); ?>

                                        <?= $this->Form->label(($i + 1), null, ['hidden']); ?>
                                        <?=$this->Form->select(
                                            '',
                                            [
                                                // $category =. $placeholder
                                                'keyword'.$i => 'Free Search',
                                                'publication'.$i => 'Publications',
                                                'collection'.$i => 'Collections',
                                                'provenience'.$i => 'Proveniences',
                                                'period'.$i => 'Periods',
                                                'inscription'.$i => 'Inscriptions',
                                                'id'.$i => 'Identification numbers'
                                            ],
                                            [
                                                'value' => $searchBar[$i*2][0].$i,
                                                'class' => 'form-group mb-0',
                                                'id' => ($i + 1)
                                            ]
                                        );?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                <?php } ?>
            </div>
        </div>

        <div class="container d-flex cdli-btn-group px-0 search-add-field-group">
            <?= $this->Form->button('Search', [
                'type' => 'submit',
                'class' => 'btn cdli-btn-blue'
            ]); ?>
            <button type="button" name="add" id="add" class="btn cdli-btn-light default-add-search-btn">
                <span class="fa fa-plus-circle plus-icon"></span> Add search field
            </button>
            <a class="d-none d-lg-block search-links mr-5" href="/SearchSettings">Search Settings</a>
            <?php if($isAdvancedSearch === 1) : ?>
                <?= $this->Html->link('Modify Advanced Search', [
                    'controller' => 'Advancedsearch',
                    'action' => 'index',
                    '?' => modifyAdvancedSearchLike($searchableFields)
                ], [
                    'class' => 'd-none d-lg-block search-links mr-5'
                ]
                ) ?>
            <?php else: ?>
                <a class="d-none d-lg-block search-links mr-5" href="/advancedsearch">Advanced Search</a>
            <?php endif;?>
        </div>
    <?= $this->Form->end()?>

    <hr class="line" />

    <?php
    // Add a link to the commodity visualization if there are results that can be visualized:
    if (count($commodityVizIds) > 0) {
        $commodityURL = $this->URLEncoding->getEncodedCommodityURL( $commodityVizIds );
        if ( strlen($commodityURL) <= 2048 ) {
          echo "<p><a href='" . $commodityURL . "'>Visualize commodity information from these texts</a></p>";
        } else {
        // Link is too long. Could show a message to let user know they can visualize the results if they prune a little.
        }
    }
    ?>

    <?php if (!empty($result)):?>
        <!-- Filter trigger button and dropdown and pagination controls -->
            <div class="d-none d-md-flex align-items-center justify-content-between mt-5">
                <div>
                    <button
                        class="show-card-btn
                        <?php if ($compactFilterStatus || $fullFilterStatus) {
                            echo "d-none";
                        } ?>
                        btn btn-outline-dark rounded-0 px-4"
                        data-toggle="modal"
                        data-target="#filter-modal">
                        Filter categories
                    </button>
                </div>

                <div class="d-flex align-items-center">
                    <?php if ($LayoutType == 1): ?>
                        <?= $this->Form->create(null, ['type'=>'get','id'=>'sort-by-dd'])?>
                            <label for="filterSelect">Sort by:</label>
                            <?= $this->Form->select('sort-order:', $filterOptions, [
                                'id' => 'filterSelect',
                                'class' => 'bg-white pl-1 ml-1 py-2 sort-by-dd'
                            ])?>
                        <?= $this->Form->end() ?>
                    <?php endif;?>

                    <div class="d-none d-md-flex align-items-center ">
                        <?php if ($LayoutType == 1): ?>
                            <?= $this->Form->button(
                                '<span
                                    class="fa fa-th-large fa-2x pt-2 pb-1 px-1"
                                    aria-hidden="true">
                                </span>',
                                [
                                    'class' => 'btn bg-dark text-white border-dark ml-4 view-btn',
                                    'aria-label' =>'Toggle view to card layout',
                                    'escapeTitle' => false,
                                    'title' => 'Card layout'
                                ]
                            ); ?>

                            <?= $this->Form->create(null,[
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['LayoutType' => 2])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    '<span
                                        class="fa fa-bars fa-2x pt-2 pb-1 px-1"
                                        aria-hidden="true">
                                    </span>',
                                    [
                                        'class' => 'btn bg-white border-dark view-btn',
                                        'aria-label' =>'Toggle view to table layout',
                                        'escapeTitle' => false,
                                        'title' => 'Table layout'
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        <?php elseif ($LayoutType == 2): ?>
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['LayoutType' => 1])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    '<span
                                        class="fa fa-th-large fa-2x pt-2 pb-1 px-1"
                                        aria-hidden="true">
                                    </span>',
                                    [
                                        'class' => 'btn bg-white border-dark ml-4 view-btn',
                                        'aria-label' =>'Toggle view to card layout',
                                        'escapeTitle' => false,
                                        'title' => 'Card layout'
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                            <?= $this->Form->button(
                                '<span
                                    class="fa fa-bars fa-2x pt-2 pb-1 px-1"
                                    aria-hidden="true">
                                </span>',
                                [
                                    'class' => 'btn bg-dark text-white border-dark view-btn',
                                    'aria-label' =>'Toggle view to table layout',
                                    'escapeTitle' => false,
                                    'title' => 'Table layout'
                                ]
                            ); ?>
                        <?php else:?>
                            <!-- <button class="btn bg-white border-dark ml-4 view-btn">
                                <span class="fa fa-th-large fa-2x pt-2 pb-1 px-1" aria-hidden="true"></span>
                            </button>
                            <button class="btn bg-white border-dark border-left-0 view-btn">
                                <span class="fa fa-bars fa-2x pt-2 pb-1 px-1" aria-hidden="true"></span>
                            </button>
                            <button class="d-none btn bg-dark text-white border-dark view-btn">
                                <span class="fa fa-pie-chart fa-2x pt-2 pb-1 px-1" aria-hidden="true"></span>
                            </button> -->
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <!-- Display query on successful search -->
            <div class="query-success">Search results for <span class = "queryString"> "<?php echo $query ?>"</span></div>

            <div class="d-md-none mt-5">
                <?php if ($LayoutType !=3): ?>
                    <div>
                        <?php if ($LayoutType == 1): ?>
                            <?= $this->Form->create(null, [
                                'type'=>'get',
                                'class'=>'sort-by-search-sm text-left'
                            ])?>
                                <label for="filterSelect">Sort by:</label>
                                <?= $this->Form->select('sort-order:', $filterOptions, [
                                    'id' => 'filterSelect',
                                    'class' => 'bg-white py-2 '
                                ])?>
                            <?= $this->Form->end() ?>
                        <?php endif;?>

                        <div class="d-flex align-items-center justify-content-between mt-4">
                            <button
                                class="show-card-btn btn btn-outline-dark rounded-0 px-4"
                                data-toggle="modal"
                                data-target="#filter-modal">
                                Filter
                            </button>

                            <span class="text-muted">Showing <?= count($result);?> entries</span>
                        </div>
                    </div>
                <?php endif;?>

                <div class="d-flex align-items-center justify-content-between mt-4">
                        <button class="btn">
                            <?= $this->Html->link('Download', array(
                                'action' => 'view', $param, 'ext' => 'pdf'), ['class' => 'text-dark'])
                            ?>
                        </button>

                        <nav>
                            <ul class="pagination pagination-dark my-1 d-flex justify-content-md-end justify-content-center" id='page-bar'>
                            <?php
                                $previous = $Page == 1 ? '-' : $Page - 1;
                                $current =  $Page;
                                $currentplus = $Page + 2;
                                $currentminus = $Page - 2;
                                $next = ($Page + 1) > $lastPage ? '-' : $Page + 1;
                            ?>
                            <li class="page-item" id="page-first">
                                <?= $this->Form->create(null, [
                                    'url' => [
                                        $searchId,
                                        '?' => $Page == 1 ? $_GET :array_merge($_GET, ['Page' => 1 ])
                                    ]
                                ]); ?>
                                    <?= $this->Form->button(
                                        $Page == 1 ? '<img class="arrow-svg rotate-svg" src="/images/arrow1.svg" alt="Last Page"' : '<img class="arrow-svg rotate-svg" src="/images/arrow2.svg" alt="Last Page">', [
                                            $Page == 1 ? 'disabled' : '',
                                            'class' => 'page-link text-dark',
                                            'aria-label' => 'Previous button',
                                            'escapeTitle' => false,
                                            'title' => 'First page'
                                        ]
                                    ); ?>
                                <?= $this->Form->end()?>
                            </li>
                            <?php
                            if($Page > 2){
                                echo '<li class="page-item page-link text-dark page-custom border">...</li>';
                            }
                            ?>
                            <?php if($next == '-' && $currentminus > 0): ?>
                            <li class="page-item" id="page-3">
                                <?= $this->Form->create(null, [
                                    'url' => [
                                        $searchId,
                                        '?' => array_merge($_GET, ['Page' => $currentminus ])
                                    ]
                                ]); ?>
                                    <?= $this->Form->button(
                                        $currentminus,
                                        [
                                            'class' => 'page-link text-dark page-custom border',
                                            'escapeTitle' => false,
                                            'title' => $currentminus
                                        ]
                                    ); ?>
                                <?= $this->Form->end()?>
                            </li>
                            <?php endif; ?>
                            <?php if(!($previous == '-')): ?>
                            <li class="page-item" id="page-1">
                                <?= $this->Form->create(null, [
                                    'url' => [
                                        $searchId,
                                        '?' => array_merge($_GET, ['Page' => $previous ])
                                    ]
                                ]); ?>
                                    <?= $this->Form->button(
                                        $previous,
                                        [
                                            'class' => 'page-link text-dark page-custom border',
                                            'escapeTitle' => false,
                                            'title' => $previous
                                        ]
                                    ); ?>
                                <?= $this->Form->end()?>
                            </li>
                            <?php endif;?>
                            <li class="page-item" id="page-2">
                                <?= $this->Form->button(
                                    $current,
                                    [
                                        'disabled',
                                        'class' => 'page-link text-light bg-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $current
                                    ]
                                ); ?>
                            </li>
                            <?php if(!($next == '-')): ?>
                            <li class="page-item" id="page-3">
                                <?= $this->Form->create(null, [
                                    'url' => [
                                        $searchId,
                                        '?' => array_merge($_GET, ['Page' => $next ])
                                    ]
                                ]); ?>
                                    <?= $this->Form->button(
                                        $next,
                                        [
                                            'class' => 'page-link text-dark page-custom border',
                                            'escapeTitle' => false,
                                            'title' => $next
                                        ]
                                    ); ?>
                                <?= $this->Form->end()?>
                            </li>
                            <?php endif;?>
                            <?php if($previous == '-' && $lastPage > $currentplus): ?>
                            <li class="page-item" id="page-3">
                                <?= $this->Form->create(null, [
                                    'url' => [
                                        $searchId,
                                        '?' => array_merge($_GET, ['Page' => $currentplus ])
                                    ]
                                ]); ?>
                                    <?= $this->Form->button(
                                        $currentplus,
                                        [
                                            'class' => 'page-link text-dark page-custom border',
                                            'escapeTitle' => false,
                                            'title' => $currentplus
                                        ]
                                    ); ?>
                                <?= $this->Form->end()?>
                            </li>
                            <?php endif; ?>
                            <?php
                            if($Page < ($lastPage - 1)){
                                echo '<li class="page-item page-link text-dark page-custom border">...</li>';
                            }
                            ?>
                            <li class="page-item" id="page-last">
                                <?= $this->Form->create(null, [
                                    'url' => [
                                        $searchId,
                                        '?' => $lastPage == $Page ? $_GET :array_merge($_GET, ['Page' => $lastPage ])
                                    ]
                                ]); ?>
                                    <?= $this->Form->button(
                                        $lastPage == $Page ? '<img class="arrow-svg" src="/images/arrow1.svg" alt="Last Page">' : '<img class="arrow-svg" src="/images/arrow2.svg" alt="Last Page">' , [
                                            $lastPage == $Page ? 'disabled' : '',
                                            'class' => 'page-link text-dark',
                                            'aria-label' => 'Next button',
                                            'escapeTitle' => false,
                                            'title' => 'Last page'
                                        ]
                                    ); ?>
                                <?= $this->Form->end()?>
                            </li>
                            </ul>
                        </nav>
                </div>
            </div>

        <!-- End Filter trigger button and  dropdown and pagination controls -->


        <!-- Download dropdown and pagination controls -->
        <?php if ($LayoutType !=3): ?>
            <div class="d-none d-md-flex align-items-center justify-content-between mt-5">
                <div>
                    <span class="text-muted">Showing <?= count($result);?> entries</span>
                    <button class="ml-2 btn p-0">
                        <?= $this->Html->link('Download', array(
                            'action' => 'view', $param, 'ext' => 'pdf'), ['class' => 'text-dark btn'])
                        ?>
                    </button>
                </div>

                <nav>
                    <ul class="pagination pagination-dark my-1 d-flex justify-content-md-end justify-content-center" id='page-bar'>
                    <?php
                            $previous = $Page == 1 ? '-' : $Page - 1;
                            $current =  $Page;
                            $currentplus = $Page + 2;
                            $currentminus = $Page - 2;
                            $next = ($Page + 1) > $lastPage ? '-' : $Page + 1;
                        ?>
                        <li class="page-item" id="page-first">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => $Page == 1 ? $_GET :array_merge($_GET, ['Page' => 1 ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $Page == 1 ? '<img class="arrow-svg rotate-svg" src="/images/arrow1.svg" alt="Last Page"' : '<img class="arrow-svg rotate-svg" src="/images/arrow2.svg" alt="Last Page">', [
                                        $Page == 1 ? 'disabled' : '',
                                        'class' => 'page-link text-dark',
                                        'aria-label' => 'Previous button',
                                        'escapeTitle' => false,
                                        'title' => 'First page'
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php
                        if($Page > 2){
                            echo '<li class="page-item page-link text-dark page-custom border">...</li>';
                        }
                        ?>
                        <?php if($next == '-' && $currentminus > 0): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $currentminus ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $currentminus,
                                    [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $currentminus
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif; ?>
                        <?php if(!($previous == '-')): ?>
                        <li class="page-item" id="page-1">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $previous ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $previous,
                                    [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $previous
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif;?>
                        <li class="page-item" id="page-2">
                            <?= $this->Form->button(
                                $current,
                                [
                                    'disabled',
                                    'class' => 'page-link text-light bg-dark page-custom border',
                                    'escapeTitle' => false,
                                    'title' => $current
                                ]
                            ); ?>
                        </li>
                        <?php if(!($next == '-')): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $next ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $next,
                                    [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $next
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif;?>
                        <?php if($previous == '-' && $lastPage > $currentplus): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $currentplus ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $currentplus,
                                    [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $currentplus
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif; ?>
                        <?php
                        if($Page < ($lastPage - 1)){
                            echo '<li class="page-item page-link text-dark page-custom border">...</li>';
                        }
                        ?>
                        <li class="page-item" id="page-last">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => $lastPage == $Page ? $_GET :array_merge($_GET, ['Page' => $lastPage ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $lastPage == $Page ? '<img class="arrow-svg" src="/images/arrow1.svg" alt="Last Page">' : '<img class="arrow-svg" src="/images/arrow2.svg" alt="Last Page">' , [
                                        $lastPage == $Page ? 'disabled' : '',
                                        'class' => 'page-link text-dark',
                                        'aria-label' => 'Next button',
                                        'escapeTitle' => false,
                                        'title' => 'Last page'
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                    </ul>
                </nav>
            </div>
        <?php endif;?>
        <!-- End Download dropdown and pagination controls -->

        <?php if ($LayoutType == 2):?>
            <div class="mt-5 rectangle-2 p-3 extended-search-actions" id="sort-options-bar">
                <span class="font-weight-bold">SORT OPTIONS:</span>

                <?= $this->Form->create(null, ['type'=>'get','class'=>'sort-options'])?>
                    <div>
                        <label for="sort-options-1">1.</label>
                        <?= $this->Form->select('sort-order:', ['option1'], ['id' => 'sort-options-1', 'class' => 'bg-white pl-1 ml-1 py-2 sort-by-dd'])?>
                    </div>

                    <div>
                        <label for="sort-options-2">2.</label>
                        <?= $this->Form->select('sort-order:', ['option2'], ['id' => 'sort-options-2', 'class' => 'bg-white pl-1 ml-1 py-2 sort-by-dd'])?>
                    </div>

                    <div>
                        <label for="sort-options-3">3.</label>
                        <?= $this->Form->select('sort-order:', ['option3'], ['id' => 'sort-options-3', 'class' => 'bg-white pl-1 ml-1 py-2 sort-by-dd'])?>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        <?php endif;?>

        <?php if (count($appliedFilters) > 0 && $LayoutType!=3): ?>
            <div class="mt-4 rectangle-2 p-3 extended-search-actions">
                <span class="font-weight-bold">APPLIED FILTERS:</span>
                <div>
                    <?php foreach ($appliedFilters as $filter):?>
                        <div class="btn bg-white p-2 m-1">
                            <?=$filter?>
                            <button class="btn bg-white p-0" aria-label="Remove filter button" title="Remove this filter from current results">
                                <span class="fa fa-times-circle" aria-hidden="true"></span>
                            </button>
                        </div>
                    <?php endforeach;?>
                </div>
                <?= $this->Form->create(null, [
                    'type'=>'POST', 'url' => [
                        'filter-reset-'.$searchId,
                        '?' => $_GET
                    ]
                ]) ?>
                <?= $this->Form->button(
                    'Clear all',
                    [
                        'class' => 'btn bg-dark text-white',
                        'aria-label' => 'Clear all',
                        'title' => 'Clear all applied filters'
                    ]
                ); ?>
                <?= $this->Form->end() ?>
            </div>
        <?php endif; ?>


        <div class="row">
            <?php if ($compactFilterStatus || $fullFilterStatus): ?>
                <div class="sideFilterBlock d-none d-md-block col-md-3 mt-5">

                    <p class="text-left font-weight-bold sidebarFilter-p">FILTER OPTIONS</p>
                    <div class="d-flex mb-4">
                            <button class="btn bg-white cdli-blue p-0" id="filter-div-expand" onclick="expandAll()">Expand all</button>
                            <button class="btn bg-white cdli-blue text-dark p-0 ml-2" id="filter-div-collapse" onclick="collapseAll()" disabled>Collapse all</button>
                    </div>

                    <!--Filters  -->
                    <?= $this->Form->create(null, [
                        'type'=>'POST', 'url' => [
                            'filter-'.$searchId,
                            '?' => $_GET
                        ],
                        'class'=>'accordion text-left search-accordion'
                    ]) ?>
                                <div class="filter-group--header">Show entries with</div>
                                <div class="border p-3">
                                    <div class="row">
                                        <div class="col-sm-12 filter-checkbox">
                                            <?php if (!empty($filterArray['object']['image_type'])): ?>
                                                <?php
                                                    $arrayKeys = array_keys($filterArray['object']['image_type']);
                                                    $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                    $selectedValues = $filteredArray['object']['image_type'];
                                                ?>
                                                <?=$this->Form->select(
                                                    'image_type',
                                                    $listOfFilters, [
                                                        'multiple' => 'checkbox',
                                                        'value' => $selectedValues
                                                    ]
                                                );?>
                                            <?php endif; ?>   
                                            <?php if (!empty($filterArray['inscription'])):?>
                                                <?php foreach ($filterArray['inscription'] as $subFilter => $values):?>
                                                        <?php
                                                            $arrayKeys = array_keys($values);
                                                            $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                            $selectedValues = $filteredArray['inscription'][$subFilter];
                                                        ?>
                                                        <?=$this->Form->select(
                                                            $subFilter,
                                                            $listOfFilters, [
                                                                'multiple' => 'checkbox',
                                                                'value' => $selectedValues
                                                            ]
                                                        );?>
                                                <?php endforeach;?>
                                            <?php endif; ?>         
                                        </div>
                                    </div>
                                </div>

                                <?php if (!empty($filterArray['object'])): ?>
                                    <div class="filter-group--header mt-3">Artifact</div>

                                    <?php foreach ($filterArray['object'] as $subFilter => $values):?>
                                        <?php if(in_array($subFilter,$searchSettings['object']) && $subFilter !== 'image_type'): ?>
                                            <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['object'][$subFilter]) ?>
                                                <div class="border filter-modal-block p-3">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <?php
                                                                $arrayKeys = array_keys($values);
                                                                $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                                $selectedValues = $filteredArray['object'][$subFilter];
                                                            ?>
                                                            <?=$this->Form->select(
                                                                $subFilter,
                                                                $listOfFilters, [
                                                                    'multiple' => 'checkbox',
                                                                    'value' => $selectedValues
                                                                ]
                                                            );?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?= $this->Accordion->partClose() ?>
                                        <?php endif; ?>
                                    <?php endforeach;?>
                                <?php endif; ?>

                                <?php if (!empty($filterArray['textual'])): ?>
                                    <div class="filter-group--header mt-3">Textual data</div>

                                    <?php foreach ($filterArray['textual'] as $subFilter => $values):?>
                                        <?php if(in_array($subFilter,$searchSettings['textual'])): ?>
                                            <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['textual'][$subFilter]) ?>
                                                <div class="border filter-modal-block p-3">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <?php
                                                                $arrayKeys = array_keys($values);
                                                                $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                                $selectedValues = $filteredArray['textual'][$subFilter];
                                                            ?>
                                                            <?=$this->Form->select(
                                                                $subFilter,
                                                                $listOfFilters, [
                                                                    'multiple' => 'checkbox',
                                                                    'value' => $selectedValues
                                                                ]
                                                            );?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?= $this->Accordion->partClose() ?>
                                        <?php endif; ?>
                                    <?php endforeach;?>
                                <?php endif; ?>

                                <?php if (!empty($filterArray['publication'])): ?>
                                    <div class="filter-group--header mt-3">Publication</div>

                                    <?php foreach ($filterArray['publication'] as $subFilter => $values):?>
                                        <?php if(in_array($subFilter,$searchSettings['publication'])): ?>
                                            <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['publication'][$subFilter]) ?>
                                                <div class="border filter-modal-block p-3">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <?php
                                                                $arrayKeys = array_keys($values);
                                                                $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                                $selectedValues = $filteredArray['publication'][$subFilter];
                                                            ?>
                                                            <?=$this->Form->select(
                                                                $subFilter,
                                                                $listOfFilters, [
                                                                    'multiple' => 'checkbox',
                                                                    'value' => $selectedValues
                                                                ]
                                                            );?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?= $this->Accordion->partClose() ?>
                                        <?php endif; ?>
                                    <?php endforeach;?>
                                <?php endif; ?>

                                <div class="d-flex justify-content-center mt-5">
                                    <?=$this->form->submit("Apply", [
                                        'id' => 'filter-button-apply',
                                        'class' => 'btn rounded-0 px-5 py-2'
                                    ])?>
                                </div>

                            <?= $this->Form->end()?>
                        <!-- end Filters -->
                </div>
            <?php endif; ?>
            <?php echo ($compactFilterStatus || $fullFilterStatus) ?

            "<div class='col-sm-12 col-md-9'>":"<div class='overflow-wrapper w-100'>"; ?>

                <?php if ($LayoutType == 1): ?>
                    <!-- show card layout -->
                    <div class="search-grid-layout text-left container mt-5" id="card-view">
                            <?php foreach ($result as $artifactId=>$row):?>
                                <div class="row mb-5">
                                    <div class="col-lg-3 col-md-4 bg-dark d-none d-md-block">
                                        <div class="search-media">
                                            <div>
                                                <a href="/dl/photo/P<?= str_pad($artifactId, 6, '0', STR_PAD_LEFT) ?>.jpg">
                                                    <figure>
                                                        <img src="/dl/tn_photo/P<?= str_pad($artifactId, 6, '0', STR_PAD_LEFT) ?>.jpg" alt="Artifact photograph">
                                                        <figcaption>View full image <span class="fa fa-share-square-o" aria-hidden="true"></span></figcaption>
                                                    </figure>
                                                </a>
                                            </div>
                                        <div>

                                        <p class="text-center">
                                            <a href="/dl/line_art/P<?= $artifactId ?>.jpg" class="text-white"><u>View line art</u></a>
                                        </p>

                                        <div class="mx-3 mt-3 text-white d-md-none d-lg-flex d-xl-flex justify-content-between">
                                            <p>View RTI:</p>
                                            <a href="#" class="text-white"><u>Observe</u></a>
                                            <a href="#" class="text-white"><u>Reverse</u></a>
                                        </div>

                                        <div class="mx-3 mt-3 text-white d-md-flex d-lg-none d-xl-none justify-content-between">
                                            <p>View RTI:</p>

                                            <span class="w-50">
                                                <a href="#" class="text-white"><u>Observe</u></a>
                                                <a href="#" class="text-white"><u>Reverse</u></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                    <div class="search-result-card border px-4 py-5 col-lg-9 col-md-8">
                                        <?= $this->Html->link(
                                            empty($row["artifact"]) ? '' : $row["artifact"]["designation"],
                                                '/artifacts/'.$artifactId, [
                                                    'class' => 'title active link',
                                                    'target' => '_blank'
                                                ]
                                            );?>
                                        <?php if(in_array('authors',$searchSettings['full:publication'])) :?>
                                            <?php $authors = empty($row['author']) ? '' : $row['author']['authors'];
                                            echo "<p class='mt-3'>
                                                <b>
                                                    Author:
                                                </b> ".
                                                $authors
                                            ."</p>";?>
                                        <?php endif;?>
                                        <?php if(in_array('year',$searchSettings['full:publication'])) :?>
                                            <?php $year = empty($row['publication']) ? 'NA' : $row['publication']['year'];
                                                echo " <p>
                                                <b>
                                                    Publications Date:
                                                </b> ".
                                                $year
                                            ."</p>"; ?>
                                        <?php endif;?>
                                        <?php echo " <p>
                                            <b>
                                                CDLI No.:
                                            </b> ".
                                            $artifactId
                                        ."</p>"; ?>
                                        <?php if(in_array('collection',$searchSettings['full:object'])) :?>
                                            <?php $collection = empty($row['collection']) ? 'NA' : $row['collection']['collection'];
                                                echo " <p>
                                                <b>
                                                    Collection:
                                                </b> ".
                                                $collection
                                            ."</p>"; ?>
                                        <?php endif;?>
                                        <?php if(in_array('provenience',$searchSettings['full:object'])) :?>
                                            <?php $provenience = empty($row['provenience']) ? '' : $row['provenience']['provenience'];
                                                echo " <p>
                                                <b>
                                                    Provenience:
                                                </b> ".
                                                $provenience
                                            ."</p>"; ?>
                                        <?php endif;?>
                                        <?php if(in_array('period',$searchSettings['full:object'])) :?>
                                           <?php $period = empty($row['period']) ? '' : $row['period']['period'];
                                            echo " <p>
                                                <b>
                                                    Period:
                                                </b> ".
                                                $period
                                            ."</p>"; ?>
                                        <?php endif;?>
                                        <?php if(in_array('atype',$searchSettings['full:object'])) :?>    
                                            <?php $ObjectType = empty($row['artifact_type']) ? '' : $row['artifact_type']['artifact_type'];
                                            echo " <p>
                                                <b>
                                                    Object Type:
                                                </b> ".
                                                $ObjectType
                                            ."</p>"; ?>
                                        <?php endif;?>
                                        <?php if(in_array('materials',$searchSettings['full:object'])) :?>
                                            <?php $materials = empty($row['material']) ? '' : $row['material']['material'];
                                            echo " <p>
                                                <b>
                                                    Material:
                                                </b> ".
                                                $materials
                                            ."</p>"; ?>
                                        <?php endif;?>
                                        
                                        <?php if(in_array('transliteration',$searchSettings['full:inscription'])):?>
                                            <p class='mt-3'><b>Inscription:</b> <br/></p>
                                            <?php if(!empty($row['inscription'])) :?>
                                                <?php   
                                                    foreach (explode("\n", $row['inscription']['atf']) as $line) {
                                                        if ($line == "" || substr($line, 0, 2) == ">>") {
                                                            continue;
                                                        }
                                                        $processedLine = processInscriptions($line, $inscriptionsQuery);
                                                        if (!empty($processedLine)) {
                                                            echo $processedLine;
                                                        }
                                                    }
                                                ?>
                                            <?php endif;?>
                                        <?php endif;?>    
                                        <button class="btn mt-3">
                                            <?php
                                                echo $this->Html->link(
                                                    "Show more",
                                                    '/artifacts/'.$artifactId, [
                                                        'class' => ' text-dark',
                                                        'target' => '_blank'
                                                    ]
                                                );
                                            ?>
                                        </button>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    <!-- end show card layout -->
                <?php elseif ($LayoutType == 2):?>
                    <!-- show table layout -->
                    <div class="overflow-wrapper mt-5" id="table-view">
                            <div class="search-table-layout text-left d-flex border-bottom">
                                <table class="fixed-table">
                                    <tr>
                                        <th class="border-right">Primary Publication</th>
                                    </tr>
                                    <?php foreach ($result as $artifactId=>$row):?>
                                        <?php
                                            $designation = empty($row["artifact"]) ? '' : $row["artifact"]["designation"];
                                            $publicationText = strlen($designation) > 60
                                            ? substr($designation, 0, 60). "..." : $designation;
                                        ?>
                                        <tr class="border-top">
                                            <td class="border-right">
                                                <?php if (strlen($publicationText)):?>
                                                    <?= $this->Html->link(
                                                        $publicationText,
                                                        '/artifacts/'.$artifactId, [
                                                            'class' => 'active',
                                                            'target' => '_blank'
                                                        ]
                                                    )?>
                                                <?php else:?>
                                                    <?= "NA" ?>
                                                <?php endif;?>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </table>
                                <table class="scrollable-table">
                                    <tr>
                                        <th>CDLI NO.</th>
                                        <?php if(in_array('period',$searchSettings['compact:object'])) :?>
                                            <th>Period</th>
                                        <?php endif;?>
                                        <?php if(in_array('year',$searchSettings['compact:publication'])) :?>
                                            <th>Publication Date</th>
                                        <?php endif;?>
                                        <?php if(in_array('provenience',$searchSettings['compact:object'])) :?>
                                            <th>Provenience</th>
                                        <?php endif;?>
                                        <?php if(in_array('authors',$searchSettings['compact:publication'])) :?>
                                            <th>Author</th>
                                        <?php endif;?> 
                                        <?php if(in_array('atype',$searchSettings['compact:object'])) :?>
                                            <th>Object Type</th>
                                        <?php endif;?>      
                                        <?php if(in_array('collection',$searchSettings['compact:object'])) :?>
                                            <th>Collection</th>
                                        <?php endif;?>
                                        <?php if(in_array('materials',$searchSettings['compact:object'])) :?>
                                            <th>Material</th>
                                        <?php endif;?>
                                        <?php if(!empty($inscriptionsQuery) && in_array('transliteration',$searchSettings['compact:inscription'])):?>
                                            <th>Transliteration/Translation</th>
                                        <?php endif;?>
                                    </tr>
                                    <?php foreach ($result as $artifactId=>$row):?>
                                        <tr class="border-top">
                                            <td>
                                                <?php echo $artifactId;?>
                                            </td>

                                            <?php if(in_array('period',$searchSettings['compact:object'])) :?>
                                                <td>
                                                    <?=
                                                        $period = empty($row['period']) ? '' : $row['period']['period'];
                                                    ?>
                                                </td>
                                            <?php endif;?>
                                            <?php if(in_array('year',$searchSettings['compact:publication'])) :?>
                                                <td>
                                                    <?=
                                                        $year = empty($row['publication']) ? 'NA' : $row['publication']['year'];
                                                    ?>
                                                </td>
                                            <?php endif;?>
                                            <?php if(in_array('provenience',$searchSettings['compact:object'])) :?>
                                                <td>
                                                    <?=
                                                        $provenience = empty($row['provenience']) ? '' : $row['provenience']['provenience'];
                                                    ?>
                                                </td>
                                            <?php endif;?>
                                            <?php if(in_array('authors',$searchSettings['compact:publication'])) :?>
                                                <td>
                                                    <?=
                                                        $authors = empty($row['author']) ? '' : $row['author']['authors'];
                                                    ?>
                                                </td>
                                            <?php endif;?>
                                            <?php if(in_array('atype',$searchSettings['compact:object'])) :?>
                                                <td>
                                                    <?=
                                                        $ObjectType = empty($row['artifact_type']) ? '' : $row['artifact_type']['artifact_type'];
                                                    ?>
                                                </td>
                                            <?php endif;?>
                                            <?php if(in_array('collection',$searchSettings['compact:object'])) :?>
                                                <?php $collection = empty($row['collection']) ? 'NA' : $row['collection']['collection']; ?>
                                                <td>
                                                    <?php
                                                        echo strlen($collection) > 35 ? substr($collection, 0, 35)."..." :  $collection;
                                                    ?>
                                                </td>
                                            <?php endif;?>
                                            <?php if(in_array('materials',$searchSettings['compact:object'])) :?>
                                                <td>
                                                    <?=
                                                        $materials = empty($row['material']) ? '' : $row['material']['material'];
                                                    ?>
                                                </td>
                                            <?php endif;?>
                                            <td>
                                                <!-- Display inscriptions only when searched in inscriptions field for compact search -->
                                                <?php if (!empty($inscriptionsQuery) && in_array('transliteration',$searchSettings['compact:inscription'])) :?>
                                                    <?php
                                                        foreach (explode("\n", $row['inscription']['atf']) as $line) {
                                                            
                                                            if ($line == "" || substr($line, 0, 2) == ">>") {
                                                                continue;
                                                            }
                                                            
                                                            $processedLine = processInscriptions($line, $inscriptionsQuery);
                                                            //display only highlighted texts
                                                            if (preg_match("/#FFFF00;/", $processedLine,)) {
                                                                echo $processedLine;
                                                            }
                                                        }
                                                    ?>
                                                <?php endif;?>    
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </table>
                            </div>
                        </div>
                    <!-- end show table layout -->

                <?php else:?>
                    <div class="my-5">
                        <p class="my-5">
                            Stats will come here
                        </p>
                    </div>
                <?php endif;?>
                </div>

            </div>
        </div>

        <!-- pagination bottom -->
        <?php if ($LayoutType !=3):?>
            <div class="mt-5 d-md-flex flex-row-reverse justify-content-between align-items-baseline search-bottom-controls">
                <nav class="mb-4">
                    <ul class="pagination pagination-dark my-1 d-flex justify-content-md-end justify-content-center" id='page-bar'>
                        <?php
                            $previous = $Page == 1 ? '-' : $Page - 1;
                            $current =  $Page;
                            $currentplus = $Page + 2;
                            $currentminus = $Page - 2;
                            $next = ($Page + 1) > $lastPage ? '-' : $Page + 1;
                        ?>
                        <li class="page-item" id="page-first">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => $Page == 1 ? $_GET :array_merge($_GET, ['Page' => 1 ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $Page == 1 ? '<img class="arrow-svg rotate-svg" src="/images/arrow1.svg" alt="Last Page"' : '<img class="arrow-svg rotate-svg" src="/images/arrow2.svg" alt="Last Page">', [
                                        $Page == 1 ? 'disabled' : '',
                                        'class' => 'page-link text-dark',
                                        'aria-label' => 'Previous button',
                                        'escapeTitle' => false,
                                        'title' => 'First page'
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php
                        if($Page > 2){
                            echo '<li class="page-item page-link text-dark page-custom border">...</li>';
                        }
                        ?>
                        <?php if($next == '-' && $currentminus > 0): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $currentminus ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $currentminus,
                                    [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $currentminus
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif; ?>
                        <?php if(!($previous == '-')): ?>
                        <li class="page-item" id="page-1">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $previous ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $previous,
                                    [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $previous
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif;?>
                        <li class="page-item" id="page-2">
                            <?= $this->Form->button(
                                $current,
                                [
                                    'disabled',
                                    'class' => 'page-link text-light bg-dark page-custom border',
                                    'escapeTitle' => false,
                                    'title' => $current
                                ]
                            ); ?>
                        </li>
                        <?php if(!($next == '-')): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $next ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $next,
                                    [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $next
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif;?>
                        <?php if($previous == '-' && $lastPage > $currentplus): ?>
                        <li class="page-item" id="page-3">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => array_merge($_GET, ['Page' => $currentplus ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $currentplus,
                                    [
                                        'class' => 'page-link text-dark page-custom border',
                                        'escapeTitle' => false,
                                        'title' => $currentplus
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                        <?php endif; ?>
                        <?php
                        if($Page < ($lastPage - 1)){
                            echo '<li class="page-item page-link text-dark page-custom border">...</li>';
                        }
                        ?>
                        <li class="page-item" id="page-last">
                            <?= $this->Form->create(null, [
                                'url' => [
                                    $searchId,
                                    '?' => $lastPage == $Page ? $_GET :array_merge($_GET, ['Page' => $lastPage ])
                                ]
                            ]); ?>
                                <?= $this->Form->button(
                                    $lastPage == $Page ? '<img class="arrow-svg" src="/images/arrow1.svg" alt="Last Page">' : '<img class="arrow-svg" src="/images/arrow2.svg" alt="Last Page">' , [
                                        $lastPage == $Page ? 'disabled' : '',
                                        'class' => 'page-link text-dark',
                                        'aria-label' => 'Next button',
                                        'escapeTitle' => false,
                                        'title' => 'Last page'
                                    ]
                                ); ?>
                            <?= $this->Form->end()?>
                        </li>
                    </ul>
                </nav>
                <div class="d-flex justify-content-between align-items-baseline">
                    <p class="text-muted mr-2">Results per page: </p>
                        <?php foreach ([10, 25, 100, 500, 1000] as $pageSizeElement) {?>
                        <?= $this->Form->create(null, [
                            'url' => [
                                $searchId,
                                '?' => modifyPageNumberIfPerSizeChanges($pageSizeElement)
                            ]
                        ]); ?>
                            <?= $this->Form->button(
                                ' <span>'.$pageSizeElement.'</span> ', [
                                    'class' => ($PageSize == $pageSizeElement) ? 'btn bg-white border-dark view-btn mx-1' : 'btn bg-white view-btn mx-1',
                                    'escapeTitle' => false,
                                    'title' => $pageSizeElement
                                ]
                            ); ?>
                        <?= $this->Form->end()?>
                    <?php } ?>
                </div>
            </div>

        <?php echo $this->element('smoothscroll'); ?>

        <?php endif;?>
        <!-- end pagination bottom -->

        <!-- filter modal -->
            <?php if (!($compactFilterStatus || $fullFilterStatus)):?>
            <div class="modal fade bd-example-modal-lg" id="filter-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" id="search-modal-dialog">
                    <div class="modal-content filter-modal" id="search-modal-content">
                        <div class="d-flex flex-row justify-content-between">
                            <h5 class="filter-heading">Filter options</h5>
                            <button type="button" class="filter-close" data-dismiss="modal">
                                <span class="fa fa-times my-auto" aria-hidden="true"></span>
                            </button>
                        </div>

                        <div class="d-flex justify-content-end mt-2">
                            <button class="btn bg-white text-primary" id="side-div-expand" onclick="expandAll()">Expand All</button>
                            <button class="btn bg-white text-primary text-dark ml-2" id="side-div-collapse" onclick="collapseAll()" disabled>Collapse all</button>
                        </div>

                        <!--Filters  -->
                            <?= $this->Form->create(null, [
                                    'type'=>'POST',
                                    'url' => [
                                        'filter-'.$searchId,
                                        '?' => $_GET
                                    ],
                                    'class'=>'mt-2 accordion text-left'
                            ]) ?>
                                <div class="filter-group--header mt-3">Show entries with</div>
                                <div class="border p-3">
                                    <div class="row">
                                        <?php foreach ($filterEntriesType as $entry):?>
                                            <div class="col-sm-12 col-md-4 col-lg-3 filter-checkbox">
                                                <?=$this->Form->control($entry, [
                                                    'type' => 'checkbox',
                                                    'value' => $entry
                                                ]);?>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>

                                <?php if (!empty($filterArray['object'])): ?>
                                    <div class="filter-group--header mt-3">Object</div>

                                    <?php foreach ($filterArray['object'] as $subFilter => $values):?>
                                        <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['object'][$subFilter]) ?>
                                            <div class="border filter-modal-block p-3">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4 col-lg-3">
                                                        <?php
                                                            $arrayKeys = array_keys($values);
                                                            $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                            $selectedValues = $filteredArray['object'][$subFilter];
                                                        ?>
                                                        <?=$this->Form->select(
                                                            $subFilter,
                                                            $listOfFilters, [
                                                                'multiple' => 'checkbox',
                                                                'value' => $selectedValues
                                                            ]
                                                        );?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?= $this->Accordion->partClose() ?>
                                    <?php endforeach;?>
                                <?php endif; ?>

                                <?php if (!empty($filterArray['textual'])): ?>
                                    <div class="filter-group--header mt-3">Textual data</div>

                                    <?php foreach ($filterArray['textual'] as $subFilter => $values):?>
                                        <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['textual'][$subFilter]) ?>
                                            <div class="border filter-modal-block p-3">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4 col-lg-3">
                                                        <?php
                                                            $arrayKeys = array_keys($values);
                                                            $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                            $selectedValues = $filteredArray['textual'][$subFilter];
                                                        ?>
                                                        <?=$this->Form->select(
                                                            $subFilter,
                                                            $listOfFilters, [
                                                                'multiple' => 'checkbox',
                                                                'value' => $selectedValues
                                                            ]
                                                        );?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?= $this->Accordion->partClose() ?>
                                    <?php endforeach;?>
                                <?php endif; ?>

                                <?php if (!empty($filterArray['publication'])): ?>
                                    <div class="filter-group--header mt-3">Publication</div>

                                    <?php foreach ($filterArray['publication'] as $subFilter => $values):?>
                                        <?= $this->Accordion->partOpen($subFilter, $mappingSearchSettings['publication'][$subFilter]) ?>
                                            <div class="border filter-modal-block p-3">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4 col-lg-3">
                                                        <?php
                                                            $arrayKeys = array_keys($values);
                                                            $listOfFilters = array_combine($arrayKeys, $arrayKeys);
                                                            $selectedValues = $filteredArray['publication'][$subFilter];
                                                        ?>
                                                        <?=$this->Form->select(
                                                            $subFilter,
                                                            $listOfFilters, [
                                                                'multiple' => 'checkbox',
                                                                'value' => $selectedValues
                                                            ]
                                                        );?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?= $this->Accordion->partClose() ?>
                                    <?php endforeach;?>
                                <?php endif; ?>

                                <div class="d-flex justify-content-center mt-4">
                                    <?=$this->form->submit("Apply", [
                                        'id' => 'filter-button-apply' ,
                                        'class' => 'btn cdli-btn-blue rounded-0 px-5 py-2'
                                    ])?>
                                </div>
                            <?= $this->Form->end()?>
                        <!-- end Filters -->
                    </div>
                </div>
            </div>
        <!-- end filter modal -->
        <?php endif;?>
    <?php else :
        if (isset($result)) {
            echo '<div>No results found for '.$query.'</div>';
        } ?>
    <?php endif;?>
</div>
