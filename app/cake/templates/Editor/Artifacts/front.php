<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact[]|\Cake\Collection\CollectionInterface $artifacts
 */
?>
<h2>Search</h2>

Search form

<h2>Search Results</h2>
<div class="artifacts index large-9 medium-8 columns content">
    <h3><?= __('Artifacts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ark_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('credit_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('primary_publication_comments') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cdli_collation') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cdli_comments') ?></th>
                <th scope="col"><?= $this->Paginator->sort('composite_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('condition_description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_entered') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_comments') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_updated') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dates_referenced') ?></th>
                <th scope="col"><?= $this->Paginator->sort('designation') ?></th>
                <th scope="col"><?= $this->Paginator->sort('electronic_publication') ?></th>
                <th scope="col"><?= $this->Paginator->sort('elevation') ?></th>
                <th scope="col"><?= $this->Paginator->sort('excavation_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('findspot_comments') ?></th>
                <th scope="col"><?= $this->Paginator->sort('findspot_square') ?></th>
                <th scope="col"><?= $this->Paginator->sort('genre_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('height') ?></th>
                <th scope="col"><?= $this->Paginator->sort('join_information') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lineart_up') ?></th>
                <th scope="col"><?= $this->Paginator->sort('museum_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('id_text') ?></th>
                <th scope="col"><?= $this->Paginator->sort('artifact_preservation') ?></th>
                <th scope="col"><?= $this->Paginator->sort('photo_up') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_public') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_atf_public') ?></th>
                <th scope="col"><?= $this->Paginator->sort('are_images_public') ?></th>
                <th scope="col"><?= $this->Paginator->sort('seal_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('seal_information') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stratigraphic_level') ?></th>
                <th scope="col"><?= $this->Paginator->sort('surface_preservation') ?></th>
                <th scope="col"><?= $this->Paginator->sort('general_comments') ?></th>
                <th scope="col"><?= $this->Paginator->sort('thickness') ?></th>
                <th scope="col"><?= $this->Paginator->sort('width') ?></th>
                <th scope="col"><?= $this->Paginator->sort('surface_comments') ?></th>
                <th scope="col"><?= $this->Paginator->sort('provenience_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('period_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_provenience_certain') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_period_certain') ?></th>
                <th scope="col"><?= $this->Paginator->sort('artifact_type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('collection_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('accession_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('accounting_period') ?></th>
                <th scope="col"><?= $this->Paginator->sort('alternative_years') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dumb') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dumb2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('month_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('month_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('day_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('custom_designation') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($artifacts as $artifact): ?>
            <tr>
                <td><?= $this->Number->format($artifact->id) ?></td>
                <td><?= h($artifact->ark_no) ?></td>
                <td><?= $this->Number->format($artifact->credit_id) ?></td>
                <td><?= h($artifact->primary_publication_comments) ?></td>
                <td><?= h($artifact->cdli_collation) ?></td>
                <td><?= h($artifact->cdli_comments) ?></td>
                <td><?= h($artifact->composite_no) ?></td>
                <td><?= h($artifact->condition_description) ?></td>
                <td><?= h($artifact->date_entered) ?></td>
                <td><?= h($artifact->date_comments) ?></td>
                <td><?= h($artifact->date_updated) ?></td>
                <td><?= h($artifact->dates_referenced) ?></td>
                <td><?= h($artifact->designation) ?></td>
                <td><?= h($artifact->electronic_publication) ?></td>
                <td><?= h($artifact->elevation) ?></td>
                <td><?= h($artifact->excavation_no) ?></td>
                <td><?= h($artifact->findspot_comments) ?></td>
                <td><?= h($artifact->findspot_square) ?></td>
                <td><?= $this->Number->format($artifact->genre_id) ?></td>
                <td><?= $this->Number->format($artifact->height) ?></td>
                <td><?= h($artifact->join_information) ?></td>
                <td><?= h($artifact->lineart_up) ?></td>
                <td><?= h($artifact->museum_no) ?></td>
                <td><?= $this->Number->format($artifact->id_text) ?></td>
                <td><?= h($artifact->artifact_preservation) ?></td>
                <td><?= h($artifact->photo_up) ?></td>
                <td><?= h($artifact->is_public) ?></td>
                <td><?= h($artifact->is_atf_public) ?></td>
                <td><?= h($artifact->are_images_public) ?></td>
                <td><?= h($artifact->seal_no) ?></td>
                <td><?= h($artifact->seal_information) ?></td>
                <td><?= h($artifact->stratigraphic_level) ?></td>
                <td><?= h($artifact->surface_preservation) ?></td>
                <td><?= h($artifact->general_comments) ?></td>
                <td><?= $this->Number->format($artifact->thickness) ?></td>
                <td><?= $this->Number->format($artifact->width) ?></td>
                <td><?= h($artifact->surface_comments) ?></td>
                <td><?= $this->Number->format($artifact->provenience_id) ?></td>
                <td><?= $this->Number->format($artifact->period_id) ?></td>
                <td><?= h($artifact->is_provenience_certain) ?></td>
                <td><?= h($artifact->is_period_certain) ?></td>
                <td><?= $this->Number->format($artifact->artifact_type_id) ?></td>
                <td><?= $this->Number->format($artifact->collection_id) ?></td>
                <td><?= h($artifact->accession_no) ?></td>
                <td><?= $this->Number->format($artifact->accounting_period) ?></td>
                <td><?= h($artifact->alternative_years) ?></td>
                <td><?= h($artifact->dumb) ?></td>
                <td><?= h($artifact->dumb2) ?></td>
                <td><?= $this->Number->format($artifact->month_id) ?></td>
                <td><?= h($artifact->month_no) ?></td>
                <td><?= h($artifact->day_no) ?></td>
                <td><?= h($artifact->custom_designation) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $artifact->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $artifact->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $artifact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifact->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->element('Paginator'); ?>
</div>
