<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading $signReading
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <div class="signReadings view content">
            <h3></h3>
            <table class="table-bootstrap">
              <tr>
                <th>Sign Name</th>
                <td><?=h($signReading->sign_name) ?></td>
              </tr>
              <tr>
                <th>Preferred Reading</th>
                <td><?=h($signReading->sign_reading) ?></td>
              </tr>
              <tr>
                <th>Meaning</th>
                <td><?=h($signReading->meaning)?></td>
              </tr>
              <tr>
                <th>Comment</th>
                <td></td>
              </tr>
            </table>
        </div>
    </div>
</div>
