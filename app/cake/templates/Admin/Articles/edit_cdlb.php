<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */
?>

<div class="row justify-content-md-center">
    <div id="articleEditDiv" class="col-lg-7 boxed">
        <h4>Add Cuneiform Digital Library Bulletin
        <span class="ojs-btn">
            <a href="http://localhost:8081/index.php/articles/workflow/index/<?= $article['submission_id'] ?>/5"><button class="btn cdli-btn-blue">Ojs Link</button></a>
        </span>
        </h4>
        <hr>
        <?= $this->Form->create(null,array('id' => 'article-form')) ?>
            <div class="form-group">
                <label>Article Title</label>
                <input type="text" name="cdlb_title" class="article_title_input_lg form-control" placeholder="" value="<?=  $article->title; ?>">
                <small class="form-text text-muted">This will be displayed as page headers.</small>
            </div>
            <div class="form-group" style="display: inline-block" >
                <label>Year</label>
                <input type="text" name="year" class="article_title_input_sm form-control" placeholder="" value="<?=  $article->year; ?>">
                <small class="form-text text-muted" >This will be displayed as link parameter.</small>
            </div>
            <div class="form-group" style="display: inline-block; margin-left: 5%" >
                <label>Article No.</label>
                <input type="text" name="article_no" class="article_title_input_sm form-control" placeholder="" value="<?=  $article->article_no; ?>">
                <small class="form-text text-muted" >This will be displayed as link parameter.</small>
            </div>
            <div class="form-group">
                <label for="">Author(s)</label>
                <input id="article_author_input" name="cdlb_authors" type="text" class="article_title_input_lg form-control">
                <div id="input-foot-tags-parent" class="input-foot-tags-parent">
                </div>
            </div>
             <div class="form-group">
             <label for="">Upload CDLB latex</label>
                <input type='hidden' name="artilce_latex_dir" id="artilce_latex_dir"  value=''>
                <textarea style="display:none" id="artilce_html_content" name="artilce_html_content"></textarea>
                <textarea style="display:none" id="article_latex_content" name="article_latex_content"></textarea>

                <div id="uploadCDLBLatex">Upload</div>
                <a id="showConvertHTMLButton" style="display:block;" onclick="toggle_latex_preview('show')" class="">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-code-slash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.854 4.146a.5.5 0 0 1 0 .708L1.707 8l3.147 3.146a.5.5 0 0 1-.708.708l-3.5-3.5a.5.5 0 0 1 0-.708l3.5-3.5a.5.5 0 0 1 .708 0zm6.292 0a.5.5 0 0 0 0 .708L14.293 8l-3.147 3.146a.5.5 0 0 0 .708.708l3.5-3.5a.5.5 0 0 0 0-.708l-3.5-3.5a.5.5 0 0 0-.708 0zm-.999-3.124a.5.5 0 0 1 .33.625l-4 13a.5.5 0 0 1-.955-.294l4-13a.5.5 0 0 1 .625-.33z"/>
                    </svg>
                    Manage converted HTML
                </a>
            </div>

            <div class="form-group">
                <label for="">Upload CDLB pdf</label>
                <input type='hidden' name="cdlb_pdf_link" id="cdlb_pdf_link"  value=''>
                 <input type='hidden' name="artilce_html_content" id="artilce_html_content"  value='<?=  htmlspecialchars($article->content_html, ENT_QUOTES, 'UTF-8'); ?>'>
                  <input type='hidden' name="article_latex_content" id="article_latex_content"  value='<?=  htmlspecialchars($article->content_latex, ENT_QUOTES, 'UTF-8'); ?>'>
                <div id="uploadCDLBPdf">Upload</div>
                 <div class="ajax-file-upload-container" style="display:none">
                    <div class="ajax-file-upload-statusbar" style="width: 400px;">
                        <img class="ajax-file-upload-preview" style="width: 100%; height: auto; display: none;">
                        <div id="edit_cdlb_filename" class="ajax-file-upload-filename"></div>
                        <div class="ajax-file-upload-progress" style="">
                            <div class="ajax-file-upload-bar" style="width: 100%;"></div>
                        </div>
                        <div class="ajax-file-upload-red ajax-file-upload-1591637473937 ajax-file-upload-abort" onclick="remove_cdlb_pdf()" style="">delete</div>
                        <div class="ajax-file-upload-green" style="display: none;">Done</div>
                        <div class="ajax-file-upload-green" style="display: none;">Download</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="">Status</label>
                <select class="form-control" id="" name="cdlb_status">
                    <option value="0">Created</option>
                    <option value="1">Under Review</option>
                    <option value="2">Accepted</option>
                    <option value="3">Published</option>
                    <option value="4">Unpublish</option>
                </select>
                <small class="form-text text-muted">This article will be assigned to the logged in user.</small>
            </div>
            <div id="cdlb_error" style="display:none;" class="alert alert-danger" role="alert">
                Please fill all the required details.
            </div>

            <div id="delete_article_warning" style="display:none;" class="alert alert-warning" role="alert">
                Are you sure you want to delete this article?.
                <button type="button" onclick="delete_article_close()" class="btn btn-sm btn-primary">No</button>
                <button type="button" onclick="delete_article_confirm('<?php echo $article->id;?>')" class="btn btn-sm btn-danger">Yes</button>
            </div>
            <button id="delete_article_button" type="button" onclick="delete_article_show()" class="btn btn-danger float-right" style="">Delete</button>
             <button id="showPreviewButton" type="button" onclick="toggle_article_preview('show')" class="btn btn-primary">Preview</button>
            <button type="button" onclick="submit_cdlb()" class="btn btn-success">Save</button>
        <?= $this->Form->end() ?>
        <button id="image_manager" style="margin-bottom:5%;display:none;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#manager_modal">
        Image Manager <i class="fa fa-cloud-upload" aria-hidden="true"></i>
        </button>
        <button id="reConvertButton" style="margin-bottom:5%;display:none;" type="button" onclick="re_convert_article()" class="btn btn-primary">
            Refresh Converter <i class="fa fa-refresh" aria-hidden="true"></i>
        </button>
         <button id="closeHTMLPreviewButton" style="display:none" type="button" onclick="toggle_latex_preview('hide')" class="btn btn-primary">
            Close Converter <i class="fa fa-times" aria-hidden="true"></i>
        </button>
         <button id="closePreviewButton" style="display:none" type="button" onclick="toggle_article_preview('hide')" class="btn btn-primary">
            Close Preview <i class="fa fa-times" aria-hidden="true"></i>
        </button>
    </div>
    <div class="col-lg boxed"  id="articleConvertDiv" style="display:none">
        <div class="capital-heading"><?= __('Conversion of CDLB Latex file to HTML') ?></div>
        <hr>
        <ul class="nav nav-tabs" style="float:right">
            <li class="nav-item">
                <a id="AEditHTML" class="nav-link active" href="#" onclick="toggle_converter_tab('EditHTML')">Edit HTML</a>
            </li>
            <li class="nav-item">
                <a id="AEditLatex" class="nav-link" href="#" onclick="toggle_converter_tab('EditLatex')">Edit Latex</a>
            </li>
            <li class="nav-item">
                <a id="APreviewLogs" class="nav-link disabled" href="#" onclick="toggle_converter_tab('PreviewLogs')">Logs</a>
            </li>
        </ul>
        <div style="clear:both;"></div>
        <div id="myProgress" style="background-color:white">
            <div id="myBar"></div>
        </div>
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
            Tip: <span id="converter_tip_msg">All the details are auto-saved.</span>
        </div>

        <div id="EditHTML" class="preview" style="height:auto;overflow:auto;background-color:white;">
            <div id="article_editor"></div>
        </div>
        <div id="EditLatex" class="preview" style="height:auto;overflow:auto;background-color:white;">
            <textarea id="latex_file_editor" style="height:100vh;width:100%;"></textarea>
        </div>
        <div id="PreviewLogs" class="preview" style="height:auto;overflow:auto;background-color:white;">
            <div id="conversion_success_notify" class="alert alert-warning alert-dismissible fade show" role="alert" style="display:none">
            <span id="conversion_success_msg"></span>
            </div>
            <div id="preview_success_notify" class="alert alert-warning alert-dismissible fade show" role="alert" style="display:none">
            <span id="preview_success_msg"></span>
            </div>
        </div>
    </div>
    <div class="col-lg boxed"  id="articlePreviewDiv" style="display:none">
               <div class="capital-heading"><?= __('Preview') ?></div>
        <div id="myProgress">
            <div id="myBar"></div>
        </div>
        <hr>
        <div class="preview" style="height:80vh;overflow-x:scroll;overflow-y:scroll;background-color:white;padding:5%">
                        <table width="750" border="0" cellpadding="0" cellspacing="5">
                <tr>
                    <td valign="top" width="400">
                    <p style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif; font-size:9pt">
                        Cuneiform Digital Library Journal <br>
                        <b> 2020:xx </b>
                        <br>
                        <font size="1"> ISSN 1540-8779 <br>
                        &#169; <i> Cuneiform Digital Library Initiative </i> </font> &nbsp;
                    </p>
                    </td>
                    <td width="200" height="200" rowspan="2" align="right" nowrap bgcolor="#1461ab">
                    <p style="line-height:15.0pt; font-size:9pt;color:white;">
                        <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif"> <a href="/"> CDLI Home </a> <br>
                        <a href=""> CDLI Publications </a> <br>
                        <a href="" target="link"> Editorial Notes </a> <br>
                        <a href="" target="link"> Abbreviations </a> <br>
                        <a href="" target="link"> Bibliography </a> <br>
                        <br>
                        <a href="" target="blank">PDF Version of this Article </a>
                        <br>
                        <a href="" target="link"> Get Acrobat Reader </a> <br>
                        <a href="" target="link"> <font color="#800517"><b>Download Cuneiform Font</b></font> </a> </font>
                    </p>
                    </td>
                    <td width="1" rowspan="2" align="right" nowrap bgcolor="#99CCCC"> &nbsp; </td>
                </tr>
                <tr>
                    <td>
                    <p>
                        <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif">
                        <h2 style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif">
                        <br>
                        <p id="pArticleName"></p>
                        </h2>
                        <b> <p id="pArticleAuthors"> Name </p</b>
                        <br>
                        <i>University</i>
                        <br>
                        <br>
                        <b> Keywords </b> <br>
                        </font>
                    </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                    <hr align="center" width="600" size="2">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="left">
                    <br>
                    <p>
                    <i><b>Abstract</b><br><br></i>
                    </p>
                    <div id="pArticleContent"></div>

                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="manager_modal" tabindex="-1" role="dialog" aria-labelledby="manager_modal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Image Manager</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="ImageManagerUploadAll"></div>
        <table id="modal-table">

        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" onclick="convert_imgmgr_results()" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- page script for admin/journals/add_cdlb -->
<script type="text/javascript" async
  src="/assets/js/mathjax/MathJax.js?config=TeX-AMS_HTML">
</script>
<script src="/assets/js/ckeditor/ckeditor.js"></script>
<script src="/assets/js/jquery.uploadfile.min.js"></script>
<script type="text/javascript">
var article_type = "cdlb";
var addPage = false;
var article_id = "<?php echo $article->id ?>";
    $(window).bind("load", function () {

        var ck = "<?php echo base64_encode($article->content_html) ?>";
        var ck_latex = "<?php echo base64_encode($article->content_latex) ?>";

        var article_id = "<?php echo $article->id ?>";

        CKEDITOR.replace('article_editor', {
            extraPlugins: 'mathjax',
            mathJaxLib: '/assets/js/mathjax/MathJax.js?config=TeX-AMS_HTML',
            height:530,
            allowedContent: true,
            removeFormatAttributes: '',
        });
        CKEDITOR.instances.article_editor.setData(atob(ck));

        $('#latex_file_editor').val((atob(ck_latex)));

        populate_authors("<?php echo $authorNames; ?>")
        success_article_upload_pdf("<?php echo $article->pdf_link; ?>",'CDLB')
    });
</script>
<script src="/assets/js/journals_dashboard.js"></script>
