<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <h4>Edit Cuneiform Digital Library PrePrint</h4>
        <h6>ID <?=  $article->id; ?>, <?=  $article->title; ?>.</h6>
        <hr>
        
        <?= $this->Form->create(null,array('id' => 'form-cdlp')) ?>
            <div class="form-group">
                <label>Article Title </label>
                <input type="text" name="cdlp_title" class="article_title_input_lg form-control" placeholder="" value="<?=  $article->title; ?>">
                <small class="form-text text-muted">This will be displayed as page headers.</small>
            </div>
            <div class="form-group" style="display: inline-block" >
                <label>Year</label>
                <input type="text" name="year" class="article_title_input_sm form-control" placeholder="" value="<?=  $article->year; ?>">
                <small class="form-text text-muted" >This will be displayed as link parameter.</small>
            </div>
            <div class="form-group" style="display: inline-block; margin-left: 5%" >
                <label>Article No.</label>
                <input type="text" name="article_no" class="article_title_input_sm form-control" placeholder="" value="<?=  $article->article_no; ?>">
                <small class="form-text text-muted" >This will be displayed as link parameter.</small>
            </div>
            <div class="form-group">
                <label for="">Author(s) </label>
                <input id="article_author_input" name="cdlp_authors" type="text" class="article_title_input_lg form-control">
                <div id="input-foot-tags-parent" class="input-foot-tags-parent">
                </div>
            </div>
            <div class="form-group">
                <label for="">Upload CDLP </label>
                <input type='hidden' name="cdlp_pdf_link" id="cdlp_pdf_link"  value=''>
                <div id="uploadCDLPPdf">Upload</div>
                <div class="ajax-file-upload-container" style="display:none">
                    <div class="ajax-file-upload-statusbar" style="width: 400px;">
                        <img class="ajax-file-upload-preview" style="width: 100%; height: auto; display: none;">
                        <div id="edit_cdlp_filename" class="ajax-file-upload-filename"></div>
                        <div class="ajax-file-upload-progress" style="">
                            <div class="ajax-file-upload-bar" style="width: 100%;"></div>
                        </div>
                        <div class="ajax-file-upload-red ajax-file-upload-1591637473937 ajax-file-upload-abort" onclick="remove_cdlp_pdf()" style="">delete</div>
                        <div class="ajax-file-upload-green" style="display: none;">Done</div>
                        <div class="ajax-file-upload-green" style="display: none;">Download</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="">Status</label>
                <select class="form-control" id="" name="cdlp_status" value="kkk">
                    <option value="0" <?php if($article->article_status == 0) echo 'selected="selected"'; ?>>Created</option>
                    <option value="1" <?php if($article->article_status == 1) echo 'selected="selected"'; ?>>Under Review</option>
                    <option value="2" <?php if($article->article_status == 2) echo 'selected="selected"'; ?>>Accepted</option>
                    <option value="3" <?php if($article->article_status == 3) echo 'selected="selected"'; ?>>Published</option>
                    <option value="4" <?php if($article->article_status == 4) echo 'selected="selected"'; ?>>Unpublish</option>
                </select>
                <small class="form-text text-muted">This article will be assigned to the logged in user.</small>
            </div>
             <div id="submit_error_show" style="display:none;" class="alert alert-danger" role="alert">
                Please fill all the required details.
            </div>
            <div id="delete_article_warning" style="display:none;" class="alert alert-warning" role="alert">
                Are you sure you want to delete this article?.
                <button type="button" onclick="delete_article_close()" class="btn btn-sm btn-primary">No</button>
                <button type="button" onclick="delete_article_confirm('<?php echo $article->id;?>')" class="btn btn-sm btn-danger">Yes</button>
            </div>
            <button id="cdlp_delete_button" type="button" onclick="delete_article_show()" class="btn btn-danger float-right">Delete</button>
            <button type="button" onclick="submit_cdlp()" class="btn btn-success">Save</button>
        <?= $this->Form->end() ?>
    </div>

</div>
<!-- page script for admin/journals/add_cdlp -->
<script src="/assets/js/jquery.uploadfile.min.js"></script>
<script src="/assets/js/journals_dashboard.js"></script>
<script type="text/javascript">
  $(window).bind("load", function () {
    populate_authors("<?php echo $authorNames; ?>")
    success_article_upload_pdf("<?php echo $article->pdf_link; ?>",'CDLP')
  });
</script>
