<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <h4>Add Cuneiform Digital Library PrePrint</h4>
        <hr>
        <?= $this->Form->create(null,array('id' => 'form-cdlp')) ?>
            <div class="form-group">
                <label>Article Title</label>
                <input type="text" name="cdlp_title" class="article_title_input_lg form-control" placeholder="">
                <small class="form-text text-muted">This will be displayed as page headers.</small>
            </div>
            <div class="form-group" style="display: inline-block" >
                <label>Year</label>
                <input type="text" name="year" value="2021" class="article_title_input_sm form-control" placeholder="">
                <small class="form-text text-muted" >This will be displayed as link parameter.</small>
            </div>
            <div class="form-group" style="display: inline-block; margin-left: 5%" >
                <label>Article No.</label>
                <input type="text" name="article_no" value="" class="article_title_input_sm form-control" placeholder="">
                <small class="form-text text-muted" >This will be displayed as link parameter.</small>
            </div>
            <div class="form-group">
                <label for="">Author(s)</label>
                <input id="article_author_input" name="cdlp_authors" type="text" class="article_title_input_lg form-control">
                <div id="input-foot-tags-parent" class="input-foot-tags-parent">
                </div>
            </div>
            <div class="form-group">
                <label for="">Upload CDLP</label>
                <input type='hidden' name="cdlp_pdf_link" id="cdlp_pdf_link"  value=''>
                <div id="uploadCDLPPdf">Upload</div>
            </div>
            <div class="form-group">
                <label for="">Status</label>
                <select class="form-control" id="" name="cdlp_status">
                    <option value="0">Created</option>
                    <option value="1">Under Review</option>
                    <option value="2">Accepted</option>
                    <option value="3">Published</option>
                    <option value="4">Unpublish</option>
                </select>
                <small class="form-text text-muted">This article will be assigned to the logged in user.</small>
            </div>
            <div id="cdlp_error" style="display:none;" class="alert alert-danger" role="alert">
                Please fill all the required details.
            </div>
            <button type="button" onclick="submit_cdlp()" class="btn btn-success">Save</button>
        <?= $this->Form->end() ?>
    </div>
</div>
<!-- page script for admin/journals/add_cdlp -->
<script src="/assets/js/jquery.uploadfile.min.js"></script>
<script src="/assets/js/journals_dashboard.js"></script>
