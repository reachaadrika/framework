<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CdliTags[]|\Cake\Collection\CollectionInterface $cdli_tags
 */
?>

<h3 class="display-4 pt-3"><?= __('CDLI Tags') ?></h3>


    
<div class="row justify-content-md-center">

<div class="boxed">
    <div class="capital-heading"><?= __('View Tags') ?></div>

    <table class="table-bootstrap">
        <thead>
            <tr>
                <th><?= __('CDLI Tags') ?></th>
                <th><?= __('') ?></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($cdli_tags as $tag) : ?>
            <tr>
                <td><?= $tag->cdli_tag ?></td>
                <td><?= $this->Html->link('delete', ['action' => 'delete/', $tag->id],['class'=>'btn btn-danger btn-sm']) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    
<br><br><br><div class="capital-heading"><?= __('Add CDLI Tag') ?></div>
<?php
    echo $this->Form->create($cdli_tag, ['action' => 'add']);
    echo $this->Form->text('cdli_tag', ['maxLength' => 100, 'onkeypress' => "this.style.width = ((this.value.length + 1) * 8) + 20 + 'px';"]);
    echo $this->Form->button(__('Save Tag'),['class'=>'btn btn-success btn-sm']);
    echo $this->Form->end();
?>
<br>

<div  align="center">
<?= $this->Html->link('Go Back', ['controller' => 'AgadeMails', 'action' => 'index'],['class' => 'btn btn-primary']) ?>
</div>

</div>


