<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExternalResource $externalResource
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($externalResource) ?>
            <legend class="capital-heading"><?= __('Add External Resource') ?></legend>
            <?php
                echo $this->Form->control('external_resource');
                echo $this->Form->control('base_url');
                echo $this->Form->control('project_url');
                echo $this->Form->control('abbrev');
                echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>
</div>
