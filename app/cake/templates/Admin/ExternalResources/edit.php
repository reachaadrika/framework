<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExternalResource $externalResource
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($externalResource) ?>
            <legend class="capital-heading"><?= __('Edit External Resource') ?></legend>
            <?php
                echo $this->Form->control('external_resource');
                echo $this->Form->control('base_url');
                echo $this->Form->control('project_url');
                echo $this->Form->control('abbrev');
                // echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $externalResource->id],
                ['class' => 'btn btn-danger float-right'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $externalResource->id)]
            )
        ?>
        </div>

    </div>

</div>
