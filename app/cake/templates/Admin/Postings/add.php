<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Posting $posting
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($posting) ?>
            <legend class="form-heading mb-3"><?= __('Add Posting') ?></legend>
            <hr class="form-hr mb-4"/>
                <?php echo $this->Form->control('posting_type_id', ['options' => $postingTypes, 'empty' => false, 'class' => 'form-control w-100 mb-3 custom-form-select', 'id' => 'posting_type', 'default' => '2', 'onchange' => 'checkValue(this);']); 
                echo $this->Form->control('title', ['class' => 'form-control w-100 mb-3', 'type' => 'text']); ?>
                <div id="artifact_no" style="display:none">
                    <?php 
                    echo $this->Form->control('artifact_id', ['class' => 'form-control w-100', 'label' => 'Artifact ID', 'type' => 'text', 'error' => false, 'id' => 'artifact_regex']);
                    echo "<div id='regex_output1' class='d-none error-message mb-3'>The Artifact ID can only contain 'P' and number upto 6 digits</div>";
                    echo "<div id='regex_output2' class='d-none error-message mb-3'>Artifact ID can not be equal to 0</div>";
                    ?>
                    <div class="mb-3">
                        <?php
                        if ($this->Form->isFieldError('artifact_id')) {
                            echo $this->Form->error('artifact_id', 'This Artifact ID doesn\'t match with the saved entries'); } 
                        if (!$this->Form->isFieldError('artifact_id')) {
                            echo '<small id="artifactField_info" class="form-text text-muted">It provides the image thumbnail.</small>'; }
                        ?>
                    </div>
                </div>
                <?php
                echo $this->Form->control('body', ['class' => 'form-control w-100 mb-3', 'label' => 'Body Text']);
                echo '<div class="mb-3"></div>';
                echo $this->Form->control('publish_start', ['empty' => true, 'label' => 'Publication Date', 'class' => 'form-control w-100 mb-3']);
                echo $this->Form->control('lang', ['options' => $lang, 'empty' => false, 'default' => 'eng', 'class' => 'form-control w-100 mb-3 custom-form-select', 'label' => 'Language']);
                echo $this->Form->control('slug', ['class' => 'form-control w-100', 'error' => false]);
                ?>
                <div class="mb-3">
                    <?php
                    if ($this->Form->isFieldError('slug')) {
                        echo $this->Form->error('slug', 'Slugs must be unique and this one is already in use',); } 
                    ?>
                </div>
                <?php
                echo $this->Form->control('published', ['label' => 'Publish (Visible to visitors)', 'class' => 'mb-4 mr-1']);
                ?>
        <div>
        <?= $this->Form->button(__('Save Changes'),['class' => 'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
        <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<!-- Add CKEDITOR to Body Field -->
<script src="/assets/js/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'body', {
	extraPlugins: 'autogrow',
	autoGrow_maxHeight: 800,
	removePlugins: 'resize'
});
</script>


<!-- Artifact ID Regex Validation -->
<script>
    function ValidatePassword() {
        //Just grab the password once
        var password = document.getElementById("artifact_no").querySelector(".form-control").value;
        $("#regex_output1").addClass("d-none");
        $("#regex_output2").addClass("d-none");

            if (!(new RegExp("(P\\d{0,6}$)|^(\\d{1,6}$)").test(password)) && (document.getElementById("regex_output2").classList.contains("d-none"))) {
                $("#regex_output1").removeClass("d-none");
                document.getElementById("submit").setAttribute('disabled','disabled');
                $("#submit").addClass("cursor_not-allowed");
                $("#artifactField_info").addClass("d-none");
            }
            else if ((!(new RegExp("((?!^0{1,6}$)^)").test(password)) || !(new RegExp("((?!^(P)0{1,6}$)^)").test(password))) && (document.getElementById("regex_output1").classList.contains("d-none"))) {
                $("#regex_output2").removeClass("d-none");
                document.getElementById("submit").setAttribute('disabled','disabled');
                $("#submit").addClass("cursor_not-allowed");
                $("#artifactField_info").addClass("d-none");
            }
            else {  
                $("#regex_output1").addClass("d-none");
                $("#regex_output2").addClass("d-none");
                document.getElementById("submit").removeAttribute("disabled");
                $("#submit").removeClass("cursor_not-allowed");
                $("#artifactField_info").removeClass("d-none");
            }
            if(document.getElementById("artifact_no").querySelector(".form-control").value == "") {
            $("#regex_output1").addClass("d-none");
            $("#regex_output2").addClass("d-none");
            document.getElementById("submit").removeAttribute("disabled");
            $("#submit").removeClass("cursor_not-allowed");
            $("#artifactField_info").removeClass("d-none");
            }
    }

    /*Bind our event to key up for the field.*/
    $(document).ready(function () {
        $("#artifact_regex").on('keyup', ValidatePassword)
    });
</script>


<!-- Function to toggle Artifact_Id field -->
<script type="text/javascript">
    function checkValue(that){
    if((that.value == 2)){
        document.getElementById("artifact_no").style.display = "none";
        document.getElementById("submit").removeAttribute("disabled");
        $("#submit").removeClass("cursor_not-allowed");
    }
    else{
        document.getElementById("artifact_no").style.display = "block";
        ValidatePassword();
    }
    }
</script>


<!-- Show Alert box on reload -->
<script type="text/javascript">
    var submit = document.getElementById("submit");
    var count = false;
    submit.onclick = () => {
        count = true;
        // When user select Posting Type as page
        if(document.getElementById("posting_type").value == '2'){
            document.getElementById("artifact_no").querySelector(".form-control").value = "0";
        }
    }

    window.addEventListener('beforeunload', function (e) {
    if(count) {
        return false;
    }
    else {
    e.preventDefault(); 
    e.returnValue = '';
    }
    });
</script>
