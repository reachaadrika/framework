<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Posting[]|\Cake\Collection\CollectionInterface $postings
 */
?>

<h3 class="display-4 pt-3"><?= __('Postings') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('posting_type_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('slug') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
            <th scope="col"><?= $this->Paginator->sort('published') ?></th>
            <th scope="col"><?= $this->Paginator->sort('lang') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created_by') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified_by') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publish_start') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publish_end') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($postings as $posting): ?>
        <tr>
            <!-- <td><?= $this->Number->format($posting->id) ?></td> -->
            <td><?= $posting->has('posting_type') ? $posting->posting_type->posting_type : '' ?></td>
            <td><?= $this->Html->link(__($posting->slug), ['action' => 'view',  $posting->id],['escape' => false]) ?></td>
            <td><?= h($posting->created) ?></td>
            <td><?= h($posting->modified) ?></td>
            <td><?= h($posting->published) ?></td>
            <td><?= h($posting->lang) ?></td>
            <td><?= $this->Number->format($posting->created_by) ?></td>
            <td><?= h($posting->modified_by) ?></td>
            <td><?= h($posting->publish_start) ?></td>
            <td><?= h($posting->publish_end) ?></td>
            <td class="btn">
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $posting->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $posting->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $posting->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

