<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading[]|\Cake\Collection\CollectionInterface $signReadings
 */
?>
<div class="signReadings index content">

    
    <h3 class="display-4 pt-3"><?= __('Sign Readings') ?></h3>
    <div class="table-responsive">
        <table  cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('sign_reading') ?></th>
                    <th><?= $this->Paginator->sort('sign_name') ?></th>
                    <th><?= $this->Paginator->sort('meaning') ?></th>
                    <th><?= $this->Paginator->sort('Preferred_reading') ?></th>
                    <th><?= $this->Paginator->sort('period_id') ?></th>
                    <th><?= $this->Paginator->sort('provenience_id') ?></th>
                    <th><?= $this->Paginator->sort('language_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($signReadings as $signReading): ?>
                <tr>
                    <td><?= h($signReading->sign_reading) ?></td>
                    <td><?= h($signReading->sign_name) ?></td>
                    <td><?= h($signReading->meaning) ?></td>
                    <td><?= h($signReading->preferred_reading) ?></td>
                    <td><?php if($signReading->has('period')): ?>
                      <a href="/periods/<?=h($signReading->period->period)?>"><?= h($signReading->period->id) ?></a>
                    <?php endif; ?> </td> 
                    <td><?php if($signReading->has('provenience')): ?>
                      <a href="/proveniences/<?=h($signReading->provenience->id)?>"><?= h($signReading->provenience->provenience) ?></a>
                    <?php endif; ?> </td>
                     <td><?php if($signReading->has('language')): ?>
                      <a href="/languages/<?=h($signReading->language->language)?>"><?= h($signReading->language->language) ?></a>
                    <?php endif; ?> </td> 
                    <td class="actions">
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $signReading->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $signReading->id], ['confirm' => __('Are you sure you want to delete # {0}?', $signReading->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginator'); ?>
</div>
