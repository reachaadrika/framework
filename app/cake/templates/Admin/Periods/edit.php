<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Period $period
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($period) ?>
            <legend class="capital-heading"><?= __('Edit Period') ?></legend>
            <?php
                echo $this->Form->control('sequence');
                echo $this->Form->control('period');
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $period->id],
                ['class' => 'btn btn-danger float-right'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $period->id)]
            )
        ?>
        </div>

    </div>

</div>
