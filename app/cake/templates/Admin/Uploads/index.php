<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Upload[]|\Cake\Collection\CollectionInterface $uploads
 */
?>
<?= $this->Html->css('upload') ?>
<div id="drop-area" class="container mt-3">
        
        <div class="row pt-2">
                <div class="col card-columns" id="cards">
                </div>                         
        </div>
        <div class="row px-2">
            <div class="col">
                <strong id="tooltip">
                    Click choose files or drop them
                </strong>
            </div>
        </div>
        <div class="row p-0 x-0">
            <div class="col">
                <input type="file" id="fileElem" multiple accept="image/*" onchange="handleFiles(this.files)">
            </div>
        </div>
        <div class="row p-2">
            <div class="col-md-auto">
                <label type="button" class="btn btn-primary" for="fileElem">Choose Files</label> 
            </div>
            <div class="col text-left">
                <button type="button" class="btn btn-success" id="submit" onclick="submitFiles()">Upload</button> 
            </div>
            <div class="col text-right">
                <a class="btn btn-primary" href="/admin/uploads/dashboard" role="button">Dashboard</a>
            </div>
        </div>
    </div>
<script>
  $(document).ready(function (e) {
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')) ?>
    }
  })
})
</script>
<?= $this->Html->script('upload') ?>
