<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactType[]|\Cake\Collection\CollectionInterface $artifactTypes
 */
?>

<h3 class="display-4 pt-3"><?= __('Artifact Types') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('artifact_type') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactTypes as $artifactType): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactType->id) ?></td> -->
            <td><a href="/artifact-types/<?=h($artifactType->id)?>"><?= h($artifactType->artifact_type) ?></a></td>
            <td><?= $artifactType->has('parent_artifact_type') ? $this->Html->link($artifactType->parent_artifact_type->id, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifactType->parent_artifact_type->artifact_type]) : '' ?></td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $artifactType->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $artifactType->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $artifactType->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
               
                
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

