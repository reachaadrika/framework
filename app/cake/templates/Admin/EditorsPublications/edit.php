<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($authorsPublication, ['action' => 'edit/'.$authorsPublication->id.'/'.$flag.'/'.$parent_id]) ?>
            <legend class="capital-heading"><?= __('Edit Editor Publication Link') ?></legend>
            <table cellpadding="10" cellspacing="10">
                <tr>
                    <td> Publication ID: </td>
                    <td><?php echo $this->Form->control('publication_id', ['label' => '', 'type' => 'number', 'disabled' => 'disabled']);
                                echo $this->Form->control('publication_id', ['type' => 'hidden']); ?></td>
                </tr>
                <tr>
                    <td> Editor Name: </td>
                    <td><?php echo $this->Form->control('editor_id', ['label' => '', 'type' => 'text', 'disabled' => 'disabled', 'value' => $authorsPublication->author->author]);
                                echo $this->Form->control('editor_id', ['type' => 'hidden']); ?></td>
                </tr>
                <tr>
                    <td> Sequence: </td>
                    <td><?php $options = [
                            1 => 1,
                            2 => 2,
                            3 => 3,
                            4 => 4,
                            5 => 5,
                            6 => 6,
                            7 => 7,
                            8 => 8,
                            9 => 9,
                            10 => 10,
                        ];
                        echo $this->Form->control('sequence', ['label' => '', 'type' => 'select', 'options' => $options]); ?></td>
                </tr>
                <tr>
                    <td> Publication Reference: </td>
                    <td> To be added </td>
                </tr>
            </table>

            <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>

<h3 class="display-4 pt-3"><?= __('Linked Editors') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"> Editor</th>
            <th scope="col"> Publiction ID </th>
            <th scope="col">Sequence</th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($authorsPublications as $row): ?>
        <tr>
            <!-- <td><?= $this->Number->format($row->id) ?></td> -->
            <td><?= $row->has('author') ? $this->Html->link($row->author->author, ['controller' => 'Authors', 'action' => 'view', $row->author->id]) : '' ?></td>
            <td><?= $row->has('publication') ? $this->Html->link($row->publication->id, ['controller' => 'Publications', 'action' => 'view', $row->publication->id]) : '' ?></td>
            <td><?= h($row->sequence) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $row->id, 0],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $row->id, 0],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $row->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

