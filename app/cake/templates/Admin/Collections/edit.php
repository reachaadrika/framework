<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection $collection
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($collection) ?>
            <legend class="capital-heading"><?= __('Edit Collection') ?></legend>
            <?php
                echo $this->Form->control('collection');
                echo $this->Form->control('geo_coordinates');
                echo $this->Form->control('slug');
                echo $this->Form->control('is_private');
                // echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $collection->id],
                ['class' => 'btn btn-danger float-right'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $collection->id)]
            )
        ?>
        </div>

    </div>

</div>
