<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Staff $staff
 */
use Cake\Routing\Router;
?>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($staff, ['type'=>'file']) ?>
            <legend class="form-heading mb-3"><?= __('Add Staff') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php 
                echo $this->Form->control('author_id', ['empty' => true, 'options' => $authors, 'id' => 'select2', 'class' => 'form-control w-100 mb-3 custom-form-select', 'required' => 'required']);
                echo "<div class='mb-3'></div>";
            ?>
            <label for="image-file">Image File</label>
            <div class="position-relative">
                <input type='file' id="img_file" name="image_file" class="form-control w-100 mb-3" onchange="pressed()"><label id="fileLabel">No file chosen</label>
            </div>
            <?php
                echo $this->Form->control('staff_type_id', ['options' => $staffTypes, 'class' => 'form-control w-100 mb-3 custom-form-select']);
                echo $this->Form->control('cdli_title', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                echo $this->Form->control('contribution', ['class' => 'form-control w-100 mb-3', 'required' => 'required']);
                echo '<div class="mb-3"></div>';
                echo $this->Form->control('sequence', ['class' => 'form-control w-100 mb-4']);
            ?>

        <!-- Bootstrap Model -->
        <?php echo $this->element('bootstrap-modal'); ?>
        <!-- Bootstrap Model -->

        <div class="pt-2">
        <?= $this->Form->button(__('Save Changes'),['class'=>'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
        <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<!-- Add CKEDITOR to Body Field -->
<script src="/assets/js/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'contribution', {
	extraPlugins: 'autogrow',
	autoGrow_maxHeight: 800,
	removePlugins: 'resize'
});
</script>


<!-- Jquery Select2 Box -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
var Add_Author = '<?php echo Router::url('/', true).'admin/authors/add'; ?>';
var select2 = $('#select2').select2({
    placeholder: 'Choose an Author',
    allowClear: true,
    language: {
    noResults: function () {
        return `<button style="width: 100%" id="add_button" type="button"
        class="btn btn-primary" data-toggle="modal" data-target="#basicModal"
        onclick='addNewAuthor()'>+ Add New Author</button>`;
    }},
    escapeMarkup: function (markup) {
        return markup;
    }});
    function addNewAuthor() {
        window.open(Add_Author);
        select2.select2("close");
        count = true;
    }

document.getElementsByClassName('select2-selection--single')[0].classList.add('custom-form-select');
</script>


<!-- Custom Image Upload  -->
<script>
    var fileLabel = document.getElementById('fileLabel');
    var img_file = document.getElementById('img_file');
    fileLabel.style.display = 'none';
    img_file.style.color = 'black';
    window.pressed = function(){
    if(img_file.value == "")
    {
        fileLabel.style.display = 'none';
        img_file.style.color = 'black';
    }
    else
    {
        var theSplit = img_file.value.split('\\');
        img_file_value = theSplit[theSplit.length-1];
        fileLabel.textContent = img_file_value;
        fileLabel.style.display = 'block';
        img_file.style.color = 'transparent';
        img_file.style.setProperty("--custon-img", "none");
        fileLabel.classList.add('fileLabel');
        img_file.setAttribute('disabled','disabled');
    }};
    fileLabel.addEventListener('click', function (e) {
    if (e.offsetX > (fileLabel.offsetWidth - 17)) {
        img_file.value = "";
        fileLabel.style.display = 'none';
        img_file.style.color = 'black';
        img_file.style.setProperty("--custon-img", "inline");
        img_file.removeAttribute('disabled');
    }});
</script>


<!-- Show Alert box on reload -->
<script type="text/javascript">
    var submit = document.getElementById("submit");
    var count = false;
    submit.onclick = () => {
        count = true;
        img_file.removeAttribute('disabled');
    }

    window.addEventListener('beforeunload', function (e) {
    if(count) {
        return false;
    }
    else {
    e.preventDefault(); 
    e.returnValue = '';
    }
    });
</script>


<!-- Bootstrap Modal -->
<script>
    var modal_label = document.getElementById('modal-label');
    var modal_body = document.getElementById('modal-body');
    var modal_btn = document.getElementById('modal-btn');
    modal_label.textContent = 'Reload';
    modal_body.textContent = 'If you have added a new author then please press Reload button to reload the page';
    modal_btn.textContent = 'Reload';
    modal_btn.setAttribute('onclick', 'reloadpage()');
    function reloadpage() {
        window.location.reload();
    }
</script>
