<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Language $language
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($language) ?>
            <legend class="capital-heading"><?= __('Add Language') ?></legend>
            <?php
                echo $this->Form->control('sequence');
                echo $this->Form->control('parent_id', ['options' => $parentLanguages, 'empty' => true]);
                echo $this->Form->control('language');
                echo $this->Form->control('protocol_code');
                echo $this->Form->control('inline_code');
                echo $this->Form->control('notes');
                echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
