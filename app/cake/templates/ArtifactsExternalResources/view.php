<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsExternalResource $artifactsExternalResource
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Artifacts External Resource') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsExternalResource->has('artifact') ? $this->Html->link($artifactsExternalResource->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsExternalResource->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('External Resource') ?></th>
                    <td><?= $artifactsExternalResource->has('external_resource') ? $this->Html->link($artifactsExternalResource->external_resource->id, ['controller' => 'ExternalResources', 'action' => 'view', $artifactsExternalResource->external_resource->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('External Resource Key') ?></th>
                    <td><?= h($artifactsExternalResource->external_resource_key) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsExternalResource->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

</div>

