<?php
/**
* CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
* @link          https://cakephp.org CakePHP(tm) Project
* @since         0.10.0
* @license       https://opensource.org/licenses/mit-license.php MIT License
*/

// Get  Roles of Authenticated User
$roles = $this->getRequest()->getSession()->read('Auth.User.roles');
$name = $this->getRequest()->getSession()->read('Auth.User.username');

// Check if role_id 1 or 2 is present
$ifRoleExists = !is_null($roles) ?  array_intersect($roles, [1, 2]) : [];

$ifRoleExists = !empty($ifRoleExists) ? 1 : 0;
?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
	<?= $this->Html->charset() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?= h($this->fetch('title')) ?> - Cuneiform Digital Library Initiative
	</title>

	<?php if ($this->fetch('image')): ?>
		<meta name="twitter:card" content="summary_large_image">
		<meta property="og:image" content="<?= h($this->fetch('image')) ?>" />
	<?php else: ?>
		<meta name="twitter:card" content="summary">
	<?php endif; ?>

	<meta property="og:title" content="<?= h($this->fetch('title')) ?>" />
	<meta property="og:url" content="https://cdli.ucla.edu<?= $this->getRequest()->getRequestTarget() ?>" />
	<meta property="og:site_name" content="CDLI">
	<meta name="twitter:site" content="@cdli_news">

	<?php if ($this->fetch('description')): ?>
		<meta name="description" content="<?= h($this->fetch('description')) ?>">
		<meta property="og:description" content="<?= h($this->fetch('description')) ?>" />
	<?php endif; ?>

	<?= $this->Html->meta('icon') ?>
	<?= $this->Html->css('main.css')?>
	<?= $this->Html->css('font-awesome/css/font-awesome.min.css')?>
	<?php
	// For using Bootstrap dropdowns, set variable $includePopper in your view
	if (isset($includePopper) && $includePopper) {
		echo $this->Html->script('popper.min.js');
	}
	?>
    <?= $this->Html->script('popper.min.js');?>
	<?= $this->Html->script('jquery.min.js')?>
	<?= $this->Html->script('bootstrap.min.js')?>
	<?= $this->Html->script('js.js')?>
	<?= $this->Html->script('drawer.js', ['defer' => true]) ?>
    <?= $this->element('google-analytics'); ?>
	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('script') ?>
</head>
<body>
	<?php
	use Cake\Core\Configure;
	$gtm_code = Configure::read('gtm_code');
	// Google Tag Manager (noscript)
	$script_inject = <<<EOD
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=$gtm_code&nojscript=true"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	EOD;
	echo $script_inject;
	// End Google Tag Manager (noscript)
	?>
	<!-- White translucent film  -->
		<div class="translucent-film d-none" id="faded-bg"></div>
	<!-- End White translucent film  -->
	<header>
		<div class="bg-foggy-grey navbar navbar-expand d-none d-lg-block">
			<nav class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ml-auto">
				<?php if ($ifRoleExists) { ?>
                    <li class="nav-item mx-3">
                        <?= $this->Html->link(in_array(1, $this->getRequest()->getSession()->read('Auth.User.roles')) ? "Admin Dashboard" : "Dashboard", '/admin/dashboard', ['class' => 'nav-link']) ?>
                    </li>
                <?php } ?>

					<li class="publication-nav nav-item mx-3 dropdown no-js-dd">
					<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Publications
					</a>
					<nav class="dropdown-menu no-js-dd-content mt-0">
						<a class="dropdown-item" href="/articles/cdlj"> Cuneiform Digital Library Journal</a>
						<a class="dropdown-item" href="/articles/cdlb"> Cuneiform Digital Library Bulletin</a>
                        <a class="dropdown-item" href="/articles/cdln">Cuneiform Digital Library Notes</a>
                        <a class="dropdown-item" href="/articles/cdlp">Cuneiform Digital Library Preprint</a>
					</nav>
				</li>
					<li class="nav-item mx-3">
						<a class="nav-link" href="/resources">Resources</a>
					</li>

				<?php if (!$this->getRequest()->getSession()->read('Auth.User')) { ?>
						<li class="nav-item mx-3">
							<?= $this->Html->link("Login", '/login', ['class' => 'nav-link']) ?>
						</li>
						<li class="nav-item mx-3 pr-5">
							<?= $this->Html->link("Register", '/register', ['class' => 'nav-link']) ?>
						</li>
					<?php } else { ?>
						<li class="publication-nav nav-item mx-3 dropdown no-js-dd">
					<a class="nav-link dropdown-toggle" href="#" aria-label="My Profile" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?php echo $name ?>
					</a>
					<nav class="dropdown-menu dropdown-menu-right no-js-dd-content">
                       <?= $this->Html->link("My Profile", [
                        'controller' => 'Users',
                        'action' => 'profile',
                        'prefix' => false
                        ], [
                        'class' => 'dropdown-item'
                        ]) ?>
						   <?php if ($ifRoleExists) { ?>
						<a class="dropdown-item" href="/admin/artifacts"> Manage Artifacts</a>
                        <a class="dropdown-item" href="/admin/articles/cdlb">Manage Authors</a>
                        <a class="dropdown-item" href="/admin/publications">Manage Publications</a>
						<a class="dropdown-item" href="/admin/articles/cdlj">Manage CDL Journals</a>
						<?php } ?>
                        <hr>
                        <?= $this->Html-> link('Logout', [
                            'controller' => 'Logout',
                            'action' => 'index',
                            'prefix' => false
                        ], [
                            'class' => 'dropdown-item',
							'aria-label' => 'Log Out'
                            ]
                        ); ?>
					</nav>
				</li>
					<?php } ?>
				</ul>
			</nav>
		</div>

		<div class="header navbar navbar-expand-lg bg-transparent py-0" id="navbar-main">
			<div class="container-fluid d-flex align-items-center justify-content-between my-5">
				<a tabindex="0" class="navbar-brand logo" href="/">
					<div class="navbar-logo">
					<img src="/images/cdlilogo.svg" class="d-none d-md-block cdlilogo" alt="CDLI-logo" />
						<img src="/images/logo-no-text.svg" class="d-md-none" alt="CDLI-logo" />
					</div>
				</a>

				<nav class="collapse navbar-collapse" id="navbarText">
					<ul class="navbar-nav ml-auto">

						<li class="nav-item mx-3 ">
							<?= $this->Html->link("Browse", '/browse', ['class' => 'nav-link']) ?>
						</li>

						<li class="nav-item mx-3">
							<a class="nav-link" href="/contribute">Contribute</a>
						</li>

						<li class="nav-item mx-3">
							<a class="nav-link" href="#">About</a>
						</li>

						<li class="nav-item mx-3 mr-5 dropdown no-js-dd">
							<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Search </a>
							<nav class="dropdown-menu border-0 shadow no-js-dd-content">
								<a class="dropdown-item" href="/">Search</a>
								<a class="dropdown-item" href="/advancedsearch">Advanced search</a>
								<a class="dropdown-item" href="/SearchSettings">Search settings</a>
								<a class="dropdown-item" href="/cqp4rdf">Corpus search</a>
							</nav>
						</li>
					</ul>
				</nav>
				<nav aria-label="menubar">
				<a tabindex="0" class="d-sm-block d-lg-none bg-transparent" id="menubar" onclick="openSlideMenu()">
					<span class="fa fa-bars fa-2x"></span>
				</a>
				</nav>
<!-- Start of Side Menu bar for small devices -->
				<div role="menubar" id="side-menu" class="side-nav shadow d-none">
					<button aria-label="Close" id="close-button" class="sidebar-btn-close px-3 border-0" onclick="closeSlideMenu()">
						×
					</button>
					<a href="/browse" class="mt-5">Browse</a>
					<a href="/contribute">Contribute</a>
					<a href="#">About</a>
					<a href="/resources">Resources</a>
					<?php if ($ifRoleExists) { ?>
						<?= $this->Html->link(in_array(1, $this->getRequest()->getSession()->read('Auth.User.roles')) ? "Admin Dashboard" : "Dashboard", '/admin/dashboard', ['class' => 'nav-link']) ?>
					<?php } ?>
					<a  data-toggle="collapse" href="#collapseExampleSearch" role="button" aria-expanded="false" aria-haspopup="true" aria-controls="collapseExampleSearch" class="border-0 mb-1">
						Search <span id="drawer-arrowSearch" class="fa fa-angle-down float-right" aria-hidden="true"></span>
					</a>
					<div class="collapse embedded-links" id="collapseExampleSearch">
						<a class="ml-5" href="/">Search</a>
						<a class="ml-5" href="/advancedsearch">Advanced search</a>
						<a class="ml-5" href="/SearchSettings">Search settings</a>
						<a class="ml-5" href="/cqp4rdf">Corpus search</a>
					</div>

					<a  data-toggle="collapse" href="#collapseExamplePublication" role="button" aria-expanded="false" aria-haspopup="true" aria-controls="collapseExamplePublication" class="border-0 mb-1">
						Publications<span id="drawer-arrowPublication" class="fa fa-angle-down float-right" aria-hidden="true"></span>
					</a>
					<div class="collapse embedded-links" id="collapseExamplePublication">
							<a class="dropdown-item" href="/articles/cdlj" aria-label="Cuneiform Digital Library Journal">CDLJ</a>
							<a class="dropdown-item" href="/articles/cdlb" aria-label="Cuneiform Digital Library Bulletin">CDLB</a>
							<a class="dropdown-item" href="/articles/cdln" aria-label="Cuneiform Digital Library Notes">CDLN</a>
							<a class="dropdown-item" href="/articles/cdlp" aria-label="Cuneiform Digital Library Preprint">CDLP</a>
					</div>

					<?php if (!$this->getRequest()->getSession()->read('Auth.User')) { ?>
						<a class="border-0 mb-1" href="/login">Login</a>
						<a class="border-0 mb-1" href="/register">Register</a>
					<?php } else { ?>
					<a  data-toggle="collapse" href="#collapseExampleProfile" role="button" aria-label="My Profile" aria-expanded="false" aria-haspopup="true" aria-controls="collapseExampleProfile" class="border-0 mb-1">
						<?php echo $name ?><span id="drawer-arrowProfile" class="fa fa-angle-down float-right" aria-hidden="true"></span>
					</a>
					<div class="collapse embedded-links" id="collapseExampleProfile">
						<?= $this->Html->link("My Profile", [
							'controller' => 'users',
							'action' => 'view',
							$this->getRequest()->getSession()->read('Auth.User.username')
						]) ?>

						<?php if ($ifRoleExists) { ?>
							<a href="/admin/artifacts"> Manage Artifacts</a>
							<a href="/admin/articles/cdlb">Manage Authors</a>
							<a href="/admin/publications">Manage Publications</a>
							<a href="/admin/articles/cdlj">Manage CDL Journals</a>

						<?php } ?>
						<?= $this->Html->link(
							'Logout',
							[
								'controller' => 'Logout',
								'action' => 'index',
								'prefix' => false
							],
							[
								'class' => 'dropdown-item',
								'aria-label' => 'Log Out'
							]
						); ?>
					</div>
					<?php }?>
				</div>
<!-- End of Side Menu bar for small devices -->
			</div>
		</div>
	</header>

	<div class="container-fluid text-center contentWrapper">
		<?= $this->Flash->render() ?>
		<?= $this->fetch('content') ?>
	</div>
	<noscript>
		<p role="alert" class="alert alert-danger alert-dismissible fade show textcenter">Your browser does not support javascript please enable it for better experince!</p>
	</noscript>
	<footer>
		<div class="container">
			<div>
				<div class="row footer-1 py-5">
					<section aria-label="Navigate" class="col-lg-3 d-none d-lg-block">
						<h2 class="heading">Navigate</h2>
						<p><a href="/browse">Browse collection</a></p>
						<p><a href="/contribute">Contribute</a></p>
						<p><a href="#">About CDLI</a></p>
						<p><a href="#">Search collection</a></p>
					</section>
					<section aria-label="Acknowledgement" class="col-md-6 col-lg-4">
						<h2 class="heading">Acknowledgement</h2>
						<p class="backers">
                            Support for this initiative has been generously provided by the
							<a href="https://mellon.org/" target="_blank">Mellon Foundation</a>,
                            the <a href="https://www.nsf.gov/" target="_blank" aria-label="National Science Foundation">NSF</a>,
							the <a href="https://www.neh.gov/" target="_blank" aria-label="National Endowment for the Humanities">NEH</a>,
							the <a href="https://www.imls.gov/" target="_blank" aria-label="Institute of Museum and Library Services">IMLS</a>,
							the <a href="https://www.mpg.de/en" target="_blank" aria-label="Max-Planck-Gesellschaft">MPS</a>,
							<a href="http://www.ox.ac.uk/" target="_blank">Oxford University</a>
							and <a href="http://www.ucla.edu/" target="_blank" aria-label="University of California">UCLA</a>,
							with additional support
                            from <a href="https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx" target="_blank" aria-label="Social Sciences and Humanities Research Council">SSHRC</a> and
							the <a href="https://www.dfg.de/" target="_blank" lang="de" aria-label="Deutsche Forschungsgemeinschaft">DFG</a>.
							Computational resources and network services are provided by
							<a href="https://humtech.ucla.edu/" target="_blank" aria-label="University of California HumTech">UCLA’s HumTech</a>,
                            <a href="https://www.mpiwg-berlin.mpg.de/" target="_blank" aria-label="Max Planck Institute for the History of Science ">MPIWG</a>,
							and <a href="https://www.computecanada.ca/" target="_blank">Compute Canada</a>.
                        </p>
					</section>
                    <div class="col-lg-1 d-none d-lg-flex"></div>
					<section aria-label="Contact Us" class="col-md-6 col-lg-4 contact">
						<h2 class="heading">Contact Us</h2>
						    <ul class="pl-0">
						        <li style="list-style-type : none">Cuneiform Digital Library Initiative</li>
						        <li style="list-style-type : none">Linton Rd, Oxford OX2 6UD</li>
						        <li style="list-style-type : none">United Kingdom</li>
					        </ul>
						<div class="d-flex">
							<div class="twitter">
								<a href="https://twitter.com/cdli_news" target="_blank" aria-label="CDLI-twitter-page">
									<span class="fa fa-twitter" aria-hidden="true"></span>
								</a>
							</div>
							<div class="mail">
								<a href="mailto:cdli@ucla.edu" target="_blank" aria-label="mail-to-CDLI">
									<span class="fa fa-envelope fa-3x" aria-hidden="true"></span>
								</a>
							</div>
							<div> <a href="https://opencollective.com/cdli/donate" class="btn donate" role="button" target="_blank">Donate</a></div>
						</div>
					</a>
					</section>
			</div>
		</div>
	</div>
</div>
<div class="footer-end p-4">
	<div class="text-center text-white">
		<p class="mb-0">© 2019-2020 Cuneiform Digital Library Initiative.</p>
	</div>
</div>
</footer>
</body>
<script>
$( document ).ready(function() {
	$('header .no-js-dd').removeClass('no-js-dd');
});
</script>
</html>
