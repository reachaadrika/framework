<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Abbreviation[]|\Cake\Collection\CollectionInterface $abbreviations
 */
?>
<h1 class="display-3 header-txt text-left">Abbreviations Index</h1>
<?php foreach (range('A', 'Z') as $char): ?>
    <?= $this->Html->link($char, ['action' => 'view', '?' => ['letter' => $char]], ['class' => 'btn btn-action']) ?>
<?php endforeach ?>

<div class="abbreviations index content">
     <div class="table-responsive table-hover">
        <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead align="left">
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('Abbreviation') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Meaning') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Publication_id') ?></th>
                    <?php if ($access_granted): ?>
                    <th scope="col"><?= __('Action') ?></th>
                    <?php endif ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($abbreviations as $abbreviation): ?>
                    <tr>
                        <td align="left"><?= h($abbreviation->abbreviation) ?></td>
                        <td align="left"><?= h($abbreviation->fullform) ?></td>
                        <td align="left"><?= h($abbreviation->type) ?></td>
                        <?php if(!empty($abbreviation->publications)):?>
                            <td align="left"><a href="publications/<?=h($abbreviation->publication_id)?>"><?= h($abbreviation->publications[0]->designation)?></a></td> 
                        <?php else:?>
                            <td></td>
                        <?php endif;?>
                        <td class="btn">
                        <?php if ($access_granted): ?>
                            <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $abbreviation->id],
                                    ['escape' => false , 'class' => "btn btn-warning btn-sm", 'title' => 'Edit']) ?>
                            <?= $this->Form->postLink(__('Delete'), ['prefix'=>'Admin','action' => 'delete',  $abbreviation->id], 
                                    ['confirm' => __('Are you sure you want to delete # {0}?',  $abbreviation->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
                        <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginator'); ?>
</div>
