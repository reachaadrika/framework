<?php
   /**
    * @var \App\View\AppView $this
    * @var \App\Model\Entity\Artifact $artifact
    */
    $roles = $this->getRequest()->getSession()->read('Auth.User.roles');
    $name = $this->getRequest()->getSession()->read('Auth.User.username');
    $ifRoleExists = !is_null($roles) ?  array_intersect($roles, [1, 2]) : [];
    $ifRoleExists = !empty($ifRoleExists) ? 1 : 0;

   $CDLI_NO = $artifact->getCdliNumber();
   $this->assign('title', $artifact->designation . ' (' . $CDLI_NO . ')');
   $this->assign('image', '/dl/photo/' . $CDLI_NO . '.jpg');

   $flatCatalogues = ['CSV' => 'csv','XLSX' => 'xlsx','TSV' => 'tsv','PDF'=>'pdf'];
   $expandedCatalogues = ['JSON' => 'json'];
   $linkedCatalogues = ['TTL' => 'ttl','JSON-RDF' => 'json','XML-RDF'=>'xml'];
   $textData = ['ATF' => 'atf' ,'JTF'=>'jtf', 'PDF'=>'pdf'];
   $linkedAnnotations = ['TTL' => 'ttl', 'JSON-RDF'=>'json-rdf' ,'XML-RDF' => 'xml'];
   $relatedData = ['Sign list', 'Vocabulary list'];
   $imageTypes = ['High resolution', 'Low resolution'];
   $docId = explode("/", $_SERVER['REQUEST_URI'])[2];
   function processInscriptions($line) {
       //Translitertaions
       $processedLine = "";
       if (preg_match("/^[0-9]+/", $line)) {
           $processedLine = $line."<br>";
       }
       //Translations
       elseif (preg_match("/^(#tr)/", $line)) {
           $line = substr($line, 5);
           $processedLine =  "<i> &emsp;".$line."</i> <br>";
       }
       //Comments
       elseif (preg_match("/^(\$ )/", $line) || preg_match("/^(# )/", $line) ) {
           $line = substr($line, 2);
           $processedLine =  "<i> &emsp;".$line."</i><br>";
       }
       //structure
       elseif (preg_match("/^@/", $line))  {
           $line = substr($line, 1);
           $processedLine =  "<b>".$line."</b><br>";
       }
       return $processedLine;
   }

   ?>
   
<main id="artifact">
   <a id="back" href="#" onclick="history.back()"><i class="fa fa-chevron-left"></i> Back to Search Results</a>
   <h1 class="display-3 header-txt"><?= h($artifact->designation) ?> (<?= h($CDLI_NO) ?>)</h1>
   <h2 class="my-4 artifact-desc">
      <?php if (!empty($artifact->genres)): ?>
      <?= ucfirst(h($artifact->genres[0]->genre)) ?>,
      <?php endif; ?>
      <?php if (!empty($artifact->artifact_type)): ?>
      <?= ucfirst(h($artifact->artifact_type->artifact_type)) ?>,
      <?php endif; ?>
      <?php if (!empty($artifact->provenience)): ?>
      excavated from
      <?= ucfirst(h($artifact->provenience->provenience)) ?>
      <?php endif; ?>
      <?php if (!empty($artifact->period)): ?>
      in <?= ucfirst(h($artifact->period->period)) ?>
      <?php endif; ?>
      <?php if (!empty($artifact->collections)): ?>
      and now kept in <?= ucfirst(h($artifact->collections[0]->collection)) ?>
      <?php endif; ?>
   </h2>
   <div class="export-grid">
      <?=$this->Dropdown->open("Export artifact <i class='fa fa-chevron-down' aria-hidden='true'></i>")?>
      <div class="row">
         <div class="col-lg-4 col-md-6 col-sm-12">
            <h3>Metadata / catalogue</h3>
            <h4 class="ml-4">Flat catalogue</h4>
            <?php foreach ($flatCatalogues as $format => $link): ?>
            <?= $this->Html->link("Get {$format}", "/artifacts/{$docId}/{$link}", [
               'controller' => 'Artifacts',
               'action' => 'view',
               $artifact->id,
               'class' => 'ml-5'
               // '_ext' => $format,
               ])?>
            <?php endforeach;?>
            <h4 class="ml-4">Expanded catalogue</h4>
            <?php foreach ($expandedCatalogues as $format => $link): ?>
            <?= $this->Html->link("Get {$format}", "/artifacts/{$docId}/{$link}", [
               'controller' => 'Artifacts',
               'action' => 'view',
               $artifact->id,
               'class' => 'ml-5'
               // '_ext' => $format,
               ])?>
            <?php endforeach;?>
            <h4 class="ml-4">Linked catalogue</h4>
            <?php foreach ($linkedCatalogues as $format => $link): ?>
            <?= $this->Html->link("Get {$format}", "/artifacts/{$docId}/{$link}", [
               'controller' => 'Artifacts',
               'action' => 'view',
               $artifact->id,
               'class' => 'ml-5'
               // '_ext' => $format,
               ])?>
            <?php endforeach;?>
         </div>
         <div class="col-lg-4 col-md-6 col-sm-12">
            <h3>Text / annotations</h3>
            <h4 class="ml-4">Text data</h4>
            <?php foreach ($textData as $format => $link): ?>
            <?= $this->Html->link("As {$format}", "/artifacts/{$docId}/{$link}", [
               'controller' => 'Artifacts',
               'action' => 'view',
               $artifact->id,
               'class' => 'ml-5'
               // '_ext' => $format,
               ])?>
            <?php endforeach;?>
            <h4 class="ml-4">Annotation data</h4>
            <?= $this->Html->link('CDLI-CoNLL', [
               'controller' => 'Artifacts',
               'action' => 'view',
               $artifact->id,
               // '_ext' => $format,
               ], ['class' => 'ml-5'])?>
            <?= $this->Html->link('CoNLL-U', [
               'controller' => 'Artifacts',
               'action' => 'view',
               $artifact->id,
               // '_ext' => $format,
               ], ['class' => 'ml-5'])?>
            <?= $this->Html->link('Brat', [
               'controller' => 'Artifacts',
               'action' => 'view',
               $artifact->id,
               // '_ext' => $format,
               ], ['class' => 'ml-5'])?>
            <h4 class="ml-4">Linked annotations</h4>
            <?php foreach ($linkedAnnotations as $format => $link): ?>
            <?= $this->Html->link("As {$format}", "/artifacts/{$docId}/{$link}", [
               'controller' => 'Artifacts',
               'action' => 'view',
               $artifact->id,
               'class' => 'ml-5'
               // '_ext' => $format,
               ])?>
            <?php endforeach;?>
            <h4 class="ml-4">Related data</h4>
            <?php foreach ($relatedData as $format): ?>
            <?= $this->Html->link('As '.$format, [
               'controller' => 'Artifacts',
               'action' => 'view',
               $artifact->id,
               // '_ext' => $format,
               ], ['class' => 'ml-5'])?>
            <?php endforeach;?>
         </div>
         <div class="col-lg-4 col-md-6 col-sm-12">
            <h3>Images</h3>
            <?php foreach ($imageTypes as $format): ?>
            <?= $this->Html->link('As '.$format, [
               'controller' => 'Artifacts',
               'action' => 'view',
               $artifact->id,
               '_ext' => $format,
               ], [
               'class' => 'ml-5'
               ])?>
            <?php endforeach;?>
         </div>
      </div>
      <?=$this->Dropdown->close()?>
   </div>
   <div id="parent-div">
      <div class="outer-div1">
         <?php $CDLI_NO = "P" . str_pad($artifact->id, 6, '0', STR_PAD_LEFT) ?>
         <div id="artifact-media" class="ml-3">
            <div class="btn-group p-3 d-flex">
               <button id="single-artifact-btn2" class="btn btn-secondary btn-sm" type="button">
               Image Type
               </button>
               <button type="button" class="btn btn-sm btn-ar dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <span id="ar-txt">Photograph</span><span class="sr-only">Toggle Dropdown</span>
               </button>
               <div id="single-artifact-dd" class="dropdown-menu pb-2 pt-2">
                  <a class="dropdown-item" href="#">Photograph</a>
                  <a class="dropdown-item" href="/dl/line_art/<?= $CDLI_NO ?>_ld.jpg">Line art</a>
                  <a class="dropdown-item" href="#">Line art detail</a>
                  <a class="dropdown-item" href="#">Photo detail</a>
                  <a class="dropdown-item" href="#">SVG</a>
               </div>
            </div>
            <div>
               <a href="/dl/photo/<?= $CDLI_NO ?>.jpg">
                  <figure>
                     <img src="/dl/tn_photo/<?= $CDLI_NO ?>.jpg" alt="<?= h($CDLI_NO) ?>"/>
                     <figcaption>View full image <i class="fa fa-external-link"></i></figcaption>
                  </figure>
               </a>
            </div>
            <footer class="p-4 outer-div">
               <div class="dropdown p-2">
                  <button id="single-artifact-btn" class="single-artifact-btn1 btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <a style="text-decoration: none;"><i class="fa fa-external-link"></i> 3D Model</a>
                  </button>
                  <div id="single-artifact-dd" class="dropdown-menu pb-2 pt-2" aria-labelledby="dropdownMenuButton">
                     <a class="dropdown-item" href="#">Artifact</a>
                     <a class="dropdown-item" href="#">Envelope</a>
                  </div>
               </div>
               <div class="dropdown p-2">
                  <button id="single-artifact-btn" class="single-artifact-btn1 btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <a style="text-decoration: none;"><i class="fa fa-external-link"></i> RTI Surfaces</a>
                  </button>
                  <div id="single-artifact-dd" class="dropdown-menu pb-2 pt-2" aria-labelledby="dropdownMenuButton">
                     <a class="dropdown-item" href="#">Observe</a>
                     <a class="dropdown-item" href="#">Reverse</a>
                  </div>
               </div>
               <div class="last p-1">
                  <u class="line-art pr-3 pl-3 pt-2 pb-1"><i class="fa fa-external-link mt-3"></i> PDF</u>
               </div>
               &nbsp;
               <?php
                  $model = WWW_ROOT . 'dl' . DS . 'vcmodels' . DS . $CDLI_NO . '.ply';
                  $texture = WWW_ROOT . 'dl' . DS . 'vcmodels' . DS . $CDLI_NO . '.jpg';
                  if (file_exists($model))
                   {
                     if (file_exists($texture))
                     {
                       echo "<a href=/3dviewer/$CDLI_NO>View in 3D</a> &nbsp";
                     }
                   }
                  ?>
            </footer>
         </div>
         <div class="artifact-summary  font-weight-light mb-0 ml-4 mt-0 mr-4">
            <h2 class="font-weight-light">Summary</h2>
            <div class="mt-4">
               <p>Musuem Collection(s)</p>
               <?php if (empty($artifact->collections)): ?>
               -
               <?php endif; ?>
               <?php foreach ($artifact->collections as $collection): ?>
               <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
               <?php endforeach; ?>
               <p>Period</p>
               <?php if (empty($artifact->period)): ?>
               -
               <?php endif; ?>
               <?php if (!empty($artifact->period)): ?>
               <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
               <?php endif; ?>
               <p>Provenience</p>
               <?php if (empty($artifact->provenience)): ?>
               -
               <?php endif; ?>
               <?php if (!empty($artifact->provenience)): ?>
               <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
               <?php endif; ?>
               <p>Artifact Type</p>
               <?php if (empty($artifact->artifact_type)): ?>
               -
               <?php endif; ?>
               <?php if (!empty($artifact->artifact_type)): ?>
               <?= $this->Html->link($artifact->artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) ?>
               <?php endif; ?>
               <p>Material(s)</p>
               <?php if (empty($artifact->materials)): ?>
               -
               <?php endif; ?>
               <?php foreach ($artifact->materials as $material): ?>
               <?= $this->Html->link($material->material, ['controller' => 'Materials', 'action' => 'view', $material->id]) ?>
               <?php endforeach; ?>
               <p>Genre / Subgenre(s) </p>
               <?php if (empty($artifact->genres)): ?>
               -
               <?php endif; ?>
               <?php foreach ($artifact->genres as $genre): ?>
               <?= $this->Html->link($genre->genre, ['controller' => 'Genres', 'action' => 'view', $genre->id]) ?>
               <?php endforeach; ?>
               <p>Language(s)</p>
               <?php if (empty($artifact->languages)): ?>
               -
               <?php endif; ?>
               <?php foreach ($artifact->languages as $language): ?>
               <?= $this->Html->link($language->language, ['controller' => 'Languages', 'action' => 'view', $language->id]) ?>
               <?php endforeach; ?>
            </div>
         </div>
      </div>
   </div>
   <h2 class="single-art-heading font-weight-light display-4">Artifact Information</h2>
   <hr class="ar-hr">
   <div class="artifact-acc">
      <div class="artifact-detail accordion pb-4">
         <?php if (!empty($artifact->inscription)): ?>
         <?php
            //split seperate lines into array
            $lines = preg_split('/\r\n|\r|\n/', $artifact->inscription->atf);
            $processedATF = '';
            foreach($lines as $line) {
                if ($line == "" || substr($line, 0, 2) == ">>") {
                    continue;
                }
                $processedLine = processInscriptions($line);
                if (!empty($processedLine)) {
                    $processedATF .= $processedLine;
                }
            }
            ?>
         <?= $this->Accordion->partOpen('inscriptions', 'Inscription and annotations', 'h2') ?>
         <div>
            <p>No. <?= h($artifact->inscription->id) ?>:</p>
            <?= $processedATF ?>
            <br/>
         </div>
         <?= $this->Html->link(
            __('Consult previous versions and their differences.'),
            ['controller' => 'Artifacts', 'id' => $artifact->id, 'action' => 'history', 'type' => 'atf']
            ) ?>
         <?= $this->Accordion->partClose() ?>
         <?php endif; ?>
         <?= $this->Accordion->partOpen('physical_information', 'Physical Information', 'h2') ?>
         <div class="related">
            <table cellpadding="0" cellspacing="0">
               <tbody>
                  <tr valign="top">
                     <td>
                        <li>
                           <?= __('Collection(s)') ?>:
                           <?php if (empty($artifact->collections)): ?>
                           -
                           <?php endif; ?>
                           <?php foreach ($artifact->collections as $collection): ?>
                           <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id])
                              ?>
                           <br>
                           <?php endforeach; ?>
                        </li>
                        <li>
                           <?= __('Is artifact type Uncertain') ?>:
                           <?= $artifact->is_artifact_type_uncertain ? __('[identification uncertain]') : __('No'); ?>
                        </li>
                        <li>
                           <?= __('Material(s)') ?>:
                           <?php if (empty($artifact->materials)): ?>
                           -
                           <?php endif; ?>
                           <?php foreach ($artifact->materials as $material): ?>
                           <?= $this->Html->link($material->material, ['controller' => 'Materials', 'action' => 'view', $material->id]) ?>
                           <?php endforeach; ?>
                        </li>
                        <li>
                           <?= __('Material Aspect(s)') ?>:
                           <?php if (empty($artifact->materials->material_aspect)): ?>
                           -
                           <?php endif; ?>
                           <?php foreach ($artifact->materials as $material): ?>
                           <?= $this->Html->link($material->material_aspect, ['controller' => 'Materials', 'action' => 'view', $material->id]) ?>
                           <?php endforeach; ?>
                        </li>
                        <li>
                           <?= __('Material Color(s)') ?>:
                           <?php if (empty($artifact->materials->material_color)): ?>
                           -
                           <?php endif; ?>
                           <?php foreach ($artifact->materials as $material): ?>
                           <?= $this->Html->link($material->material_color, ['controller' => 'Materials', 'action' => 'view', $material->id]) ?>
                           <?php endforeach; ?>
                        </li>
                        <li>
                           <?= __('Artifact Preservation') ?>:
                           <?php if (empty($artifact->artifact_preservation)): ?>
                           -
                           <?php endif; ?>
                           <?= h($artifact->artifact_preservation) ?>
                        </li>
                        <li>
                           <?= __('Surface Preservation') ?>:
                           <?php if (empty($artifact->surface_preservation)): ?>
                           -
                           <?php endif; ?>
                           <?= h($artifact->surface_preservation) ?>
                        </li>
                        <li>
                           <?= __('Condition Description') ?>:
                           <?php if (empty($artifact->condition_description)): ?>
                           -
                           <?php endif; ?>
                           <?= h($artifact->condition_description) ?>
                        </li>
                        <li>
                           <?= __('Join Information') ?>:
                           <?php if (empty($artifact->join_information)): ?>
                           -
                           <?php endif; ?>
                           <?= h($artifact->join_information) ?>
                        </li>
                        <li>
                           <?= __('Seal no.') ?>:
                           <?php if (empty($artifact->seal_no)): ?>
                           -
                           <?php endif; ?>
                           <?= h($artifact->seal_no) ?>
                        </li>
                        <li>
                           <?= __('Seal information') ?>:
                           <?php if (empty($artifact->seal_information)): ?>
                           -
                           <?php endif; ?>
                           <?= $this->Text->autoParagraph(h($artifact->seal_information)) ?>
                        </li>
                        <?php if ($artifact->is_artifact_fake): ?>
                        <li>
                           <p>This artifact was made in modern times.</p>
                        </li>
                        <?php endif; ?>
                        <li>
                           <?= __('Artifact Note') ?>:
                           <?php if (empty($artifact->cdli_comments)): ?>
                           -
                           <?php endif; ?>
                           <?php if ($artifact->cdli_comments): ?>
                           <?= h($artifact->cdli_comments) ?>
                           <?php endif; ?>
                        </li>
                     </td>
                  </tr>
                  <tr valign="top">
                     <td>
                        <li>
                           <?= __('Measurements (mm)') ?>:
                           <?= h($artifact->height) ?> x
                           <?= h($artifact->width) ?> x
                           <?= h($artifact->thickness) ?>
                        </li>
                        <li>
                           <?= __('Weight (grams)') ?>:
                           <?php if (empty($artifact->weight)): ?>
                           -
                           <?php endif; ?>
                           <?= h($artifact->weight) ?>
                        </li>
                        <li>
                           <?= __('Has Fragments') ?>:
                           <?php if (empty($artifact->has_fragments)): ?>
                           -
                           <?php endif; ?>
                           <?php if ($artifact->has_fragments): ?>
                           <p>This artifact is composed of fragments.</p>
                           <?php endif; ?>
                        </li>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
         <?= $this->Accordion->partClose() ?>
         <?= $this->Accordion->partOpen('text information', 'Text Information', 'h2') ?>
         <ul>
            <li>
               <?= __('Genre / Subgenre(s)') ?>:
               <?php if (empty($artifact->genres)): ?>
               -
               <?php endif; ?>
               <?php foreach ($artifact->genres as $genre): ?>
               <?= $this->Html->link($genre->genre, ['controller' => 'Genres', 'action' => 'view', $genre->id]) ?>
               <?php endforeach; ?>
            </li>
            <li>
               <?= __('Is Genre Uncertain') ?>:
               <?= $artifact->is_genre_uncertain ? __('[identification uncertain]') : __('No'); ?>
            </li>
            <li>
               <?= __('Is School Text') ?>:
               <?= $artifact->is_school_text ? __('This text is a school text.') : __('No'); ?>
            </li>
            <li>
               <?= __('Language(s)') ?>:
               <?php if (empty($artifact->languages)): ?>
               -
               <?php endif; ?>
               <?php foreach ($artifact->languages as $language): ?>
               <?= $this->Html->link($language->language, ['controller' => 'Languages', 'action' => 'view', $language->id]) ?>
               <?php endforeach; ?>
            </li>
         </ul>
         <?= $this->Accordion->partClose() ?>
         <?= $this->Accordion->partOpen('publications', 'Publication Data', 'h2') ?>
         <ul>
            <?php foreach ($artifact->publications as $publication): ?>
            <li>
               <?= $this->Html->link($publication->_joinData->publication_type, ['controller' => 'Publications', 'action' => 'view', $publication->id]) ?>
               <?php if (!empty($publication->designation)): ?>
               : <?= h($publication->designation) ?>
               <?php endif; ?>
               <?php foreach ($publication->authors as $author): ?>
               (<?= $this->Html->link($author->author, ['controller' => 'Authors', 'action' => 'view', $author->id]) ?>)
               <?php endforeach; ?>
            </li>
            <?php endforeach; ?>
            <?php foreach ($artifact->publications as $publication): ?>
            <li>
               <?php if (!empty($publications->bibtexkey)): ?>
               <?= __('Bibtex Key') ?>:
               <?php if (empty($artifact->bibtexkey)): ?>
               -
               <?php endif; ?>
               <?= h($publications->bibtexkey) ?>
               <?php endif; ?>
            </li>
            <?php endforeach; ?>
            <?php foreach ($artifact->publications as $publication): ?>
            <li>
               <?php if (!empty($publications->exact_reference)): ?>
               <?= __('Exact Reference') ?>:
               <?php if (empty($artifact->exact_reference)): ?>
               -
               <?php endif; ?>
               <?= h($publications->exact_reference) ?>
               <?php endif; ?>
            </li>
            <?php endforeach; ?>
            <li>
               <?= __('Format Reference') ?>:
               <?php foreach ($artifactsPublications as $artifactsPublication): ?>
               <?= $this->Scripts->formatReference($artifactsPublication->toArray(), 'bibliography', [
                  'template' => 'chicago-author-date',
                  'format' => 'html'
                  ]) ?>
               <?php endforeach ?>
            </li>
            <?php foreach ($artifact->publications as $publication): ?>
            <li>
               <?php if (!empty($publications->publication_comments)): ?>
               <?= __('Publication Notes') ?>:
               <?php if (empty($artifact->publication_comments)): ?>
               -
               <?php endif; ?>
               <?= h($publications->publication_comments) ?>
               <?php endif; ?>
            </li>
            <?php endforeach; ?>
         </ul>
         <?php if (strlen($artifact->primary_publication_comments)) :?>
         <h3 class="artifact-sub-heading">Author comments:</h3>
         <?php if (empty($artifact->primary_publication_comments)): ?>
         -
         <?php endif; ?>
         <?= $this->Text->autoParagraph(h($artifact->primary_publication_comments)) ?>
         <?php endif;?>
         <?= $this->Accordion->partClose() ?>
         <?= $this->Accordion->partOpen('identifiers', 'Identifiers', 'h2') ?>
         <ul>
            <li>
               <?= __('Designation') ?>:
               <?php if (empty($artifact->designation)): ?>
               -
               <?php endif; ?>
               <?= h($artifact->designation) ?>
            </li>
            <li>
               <?= __('CDLI No') ?>:
               <?php if (empty($artifact->CDLI_NO)): ?>
               -
               <?php endif; ?>
               <?= h($CDLI_NO) ?>
            </li>
            <li>
               <?= __('Composite No.') ?>:
               <?php if (empty($artifact->composite_no)): ?>
               -
               <?php endif; ?>
               <?= h($artifact->composite_no) ?>
            </li>
            <li>
               <?= __('Seal No.') ?>:
               <?php if (empty($artifact->seal_no)): ?>
               -
               <?php endif; ?>
               <?= h($artifact->seal_no) ?>
            </li>
            <li>
               <?= __('Museum No.') ?>:
               <?php if (empty($artifact->museum_no)): ?>
               -
               <?php endif; ?>
               <?= h($artifact->museum_no) ?>
            </li>
            <li>
               <?= __('Accession No.') ?>:
               <?php if (empty($artifact->accession_no)): ?>
               -
               <?php endif; ?>
               <?= h($artifact->accession_no) ?>
            </li>
            <li>
               <?= __('Ark No.') ?>:
               <?php if (empty($artifact->ark_no)): ?>
               -
               <?php endif; ?>
               <?= h($artifact->ark_no) ?>
            </li>
         </ul>
         <?= $this->Accordion->partClose() ?>
         <?= $this->Accordion->partOpen('provenience and excavation', 'Provenience & Excavation information', 'h2') ?>
         <div class="related">
            <table cellpadding="0" cellspacing="0">
               <tbody>
                  <tr valign="top">
                     <td>
                        <li>
                           <?= __('Provenience') ?>:
                           <?php if (empty($artifact->provenience)): ?>
                           -
                           <?php endif; ?>
                           <?php if (!empty($artifact->provenience)): ?>
                           <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
                           <?php endif; ?>
                        </li>
                        <li>
                           <?= __('Is Provenience Uncertain') ?>:
                           <?= $artifact->is_provenience_uncertain ? __('Yes') : __('No'); ?>
                           <?php if ($artifact->is_period_uncertain): ?>
                           <p>[identification uncertain]</p>
                           <?php endif; ?>
                        </li>
                        <li>
                           <?= __('Provenience Notes') ?>:
                           <?php if (empty($artifact->provenience_comments)): ?>
                           -
                           <?php endif; ?>
                           <?= $this->Text->autoParagraph(h($artifact->provenience_comments)); ?>
                        </li>
                     </td>
                  </tr>
                  <tr valign="top">
                     <td>
                        <li>
                           <?= __('Elevation') ?>:
                           <?php if (empty($artifact->elevation)): ?>
                           -
                           <?php endif; ?>
                           <?= h($artifact->elevation) ?>
                        </li>
                        <li>
                           <?= __('Stratigraphic Level') ?>:
                           <?php if (empty($artifact->stratigraphic_level)): ?>
                           -
                           <?php endif; ?>
                           <?= h($artifact->stratigraphic_level) ?>
                        </li>
                        <li>
                           <?= __('Findspot Square') ?>:
                           <?php if (empty($artifact->findspot_square)): ?>
                           -
                           <?php endif; ?>
                           <?= h($artifact->findspot_square) ?>
                        </li>
                        <li>
                           <?= __('Findspot Notes') ?>:
                           <?php if (empty($artifact->findspot_comments)): ?>
                           -
                           <?php endif; ?>
                           <?= $this->Text->autoParagraph(h($artifact->findspot_comments)) ?>
                        </li>
                        <li>
                           <?= __('Excavation No') ?>:
                           <?php if (empty($artifact->excavation_no)): ?>
                           -
                           <?php endif; ?>
                           <?= h($artifact->excavation_no) ?>
                        </li>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
         <?= $this->Accordion->partClose() ?>
         <?= $this->Accordion->partOpen('chronology', 'Chronology', 'h2') ?>
         <ul>
            <li>
               <?= __('Period') ?>:
               <?php if (empty($artifact->period)): ?>
               -
               <?php endif; ?>
               <?php if (!empty($artifact->period)): ?>
               <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
               <?php endif; ?>
               <?php if ($artifact->is_period_uncertain): ?>
               <p>[identification uncertain]</p>
               <?php endif; ?>
            </li>
            <li>
               <?= __('Period Notes') ?>:
               <?php if (empty($artifact->period_comments)): ?>
               -
               <?php endif; ?>
               <?= $this->Text->autoParagraph(h($artifact->period_comments)) ?>
            </li>
            <li>
               <?= __('Accounting Period') ?>:
               <?php if (empty($artifact->accounting_period)): ?>
               -
               <?php endif; ?>
               <?php if (!empty($artifact->accounting_period)): ?>
               <?= $this->Number->format($artifact->accounting_period) ?>
               <?php endif; ?>
            </li>
            <li>
               <div class="related">
                  <table cellpadding="0" cellspacing="0" class="ml-0">
                     <thead>
                        <tr>
                           <th scope="col"><?= __('Date Id') ?></th>
                           <th scope="col"><?= __('Date(s) Referenced') ?></th>
                           <th scope="col"><?= __('Date Notes') ?></th>
                           <th scope="col"><?= __('Alternative Years') ?></th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>
                              <?php if (empty($artifact->date_id)): ?>
                              -
                              <?php endif; ?>
                              <?= h($artifact->date_id) ?>
                           </td>
                           <td>
                              <?php if (empty($artifact->dates_referenced)): ?>
                              -
                              <?php endif; ?>
                              <?= h($artifact->dates_referenced) ?>
                           </td>
                           <td>
                              <?php if (empty($artifact->date_comments)): ?>
                              -
                              <?php endif; ?>
                              <?= h($artifact->date_comments) ?>
                           </td>
                           <td>
                              <?php if (empty($artifact->alternative_years)): ?>
                              -
                              <?php endif; ?>
                              <?= h($artifact->alternative_years) ?>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </li>
         </ul>
         <?= $this->Accordion->partClose() ?>
         <?= $this->Accordion->partOpen('credits', 'Credits', 'h2') ?>
         <ul>
            <?php if (empty($artifact->credits)): ?>
            -
            <?php endif; ?>
            <?php foreach ($artifact->credits as $credit): ?>
            <li>
               <?= $this->Html->link($credit->author->author, ['controller' => 'Authors', 'action' => 'view', $credit->author->id]) ?>
               (<?= h($credit->date) ?>)<!--
                  --><?php if (!empty($credit->comments)): ?>: <?= h($credit->comments) ?><?php endif; ?>
            </li>
            <?php endforeach; ?>
         </ul>
         <?= $this->Accordion->partClose() ?>
         <?= $this->Accordion->partOpen('external_resources', 'External Resources', 'h2') ?>
         <p>Here list links to external resources one per line:</p>
         <ul>
            <?php if (empty($artifact->external_resources)): ?>
            -
            <?php endif; ?>
            <?php foreach ($artifact->external_resources as $external_resource): ?>
            <li>
               <?= $this->Html->link(
                  $external_resource->external_resource,
                  $external_resource->base_url . $external_resource->_joinData->external_resource_key
                  ) ?>
               <?= h($artifact->designation) ?> (<?= h($CDLI_NO) ?>)
            </li>
            <?php endforeach; ?>
         </ul>
         <?= $this->Accordion->partClose() ?>
         <?php if (!empty($artifact->credits)): ?>
         <?= $this->Accordion->partOpen('related_credits', 'Related Credits', 'h2') ?>
         <div class="related">
            <table cellpadding="0" cellspacing="0">
               <thead>
                  <tr>
                     <th scope="col"><?= __('Id') ?></th>
                     <th scope="col"><?= __('User Id') ?></th>
                     <th scope="col"><?= __('Artifact Id') ?></th>
                     <th scope="col"><?= __('Date') ?></th>
                     <th scope="col"><?= __('Notes') ?></th>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach ($artifact->credits as $credits): ?>
                  <tr>
                     <td>
                        <?php if (empty($credits->id)): ?>
                        -
                        <?php endif; ?>
                        <a href="/credits/<?= $credits->id ?>" >
                        <?= h($credits->id) ?>
                        </a>
                     </td>
                     <td>
                        <?php if (empty($credits->author_id)): ?>
                        -
                        <?php endif; ?>
                        <?= h($credits->author_id) ?>
                     </td>
                     <td>
                        <?php if (empty($credits->artifact_id)): ?>
                        -
                        <?php endif; ?>
                        <?= h($credits->artifact_id) ?>
                     </td>
                     <td>
                        <?php if (empty($credits->date)): ?>
                        -
                        <?php endif; ?>
                        <?= h($credits->date) ?>
                     </td>
                     <td>
                        <?php if (empty($credits->comments)): ?>
                        -
                        <?php endif; ?>
                        <?= h($credits->comments) ?>
                     </td>
                  </tr>
                  <?php endforeach; ?>
               </tbody>
            </table>
         </div>
         <?= $this->Accordion->partClose() ?>
         <?php endif; ?>
         <?= $this->Accordion->partOpen('composites_and_witnesses', 'Composites and Witnesses', 'h2') ?>
         <?= $this->Accordion->partClose() ?>
         <?php if (!empty($artifact->artifacts_composites)): ?>
         <?= $this->Accordion->partOpen('related_artifacts_composites', 'Related Artifacts Composites', 'h2') ?>
         <div class="related">
            <table cellpadding="0" cellspacing="0">
               <thead>
                  <tr>
                     <th scope="col"><?= __('Id') ?></th>
                     <th scope="col"><?= __('Composite') ?></th>
                     <th scope="col"><?= __('Artifact Id') ?></th>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach ($artifact->artifacts_composites as $artifactsComposites): ?>
                  <tr>
                     <td>
                        <?php if (empty($artifactsComposites->id)): ?>
                        -
                        <?php endif; ?>
                        <a href="/artifacts-composites/<?= $artifactsComposites->id ?>" >
                        <?= h($artifactsComposites->id) ?>
                        </a>
                     </td>
                     <td>
                        <?php if (empty($artifactsComposites->composite_no)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsComposites->composite_no) ?>
                     </td>
                     <td>
                        <?php if (empty($artifactsComposites->artifact_id)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsComposites->artifact_id) ?>
                     </td>
                  </tr>
                  <?php endforeach; ?>
               </tbody>
            </table>
         </div>
         <?= $this->Accordion->partClose() ?>
         <?php endif; ?>
         <?php if (!empty($artifact->artifacts_date_referenced)): ?>
         <?= $this->Accordion->partOpen('related_artifacts_date_referenced', 'Related Artifacts Date Referenced', 'h2') ?>
         <div class="related">
            <table cellpadding="0" cellspacing="0">
               <thead>
                  <tr>
                     <th scope="col"><?= __('Id') ?></th>
                     <th scope="col"><?= __('Artifact Id') ?></th>
                     <th scope="col"><?= __('Ruler Id') ?></th>
                     <th scope="col"><?= __('Month Id') ?></th>
                     <th scope="col"><?= __('Month No') ?></th>
                     <th scope="col"><?= __('Year Id') ?></th>
                     <th scope="col"><?= __('Day No') ?></th>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach ($artifact->artifacts_date_referenced as $artifactsDateReferenced): ?>
                  <tr>
                     <td>
                        <?php if (empty($artifactsDateReferenced->id)): ?>
                        -
                        <?php endif; ?>
                        <a href="/artifacts-date-referenced/<?= $artifactsDateReferenced->id ?>" >
                        <?= h($artifactsDateReferenced->id) ?>
                        </a>
                     </td>
                     <td>
                        <?php if (empty($artifactsDateReferenced->artifact_id)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsDateReferenced->artifact_id) ?>
                     </td>
                     <td>
                        <?php if (empty($artifactsDateReferenced->ruler_id)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsDateReferenced->ruler_id) ?>
                     </td>
                     <td>
                        <?php if (empty($artifactsDateReferenced->month_id)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsDateReferenced->month_id) ?>
                     </td>
                     <td>
                        <?php if (empty($artifactsDateReferenced->month_no)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsDateReferenced->month_no) ?>
                     </td>
                     <td>
                        <?php if (empty($artifactsDateReferenced->year_id)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsDateReferenced->year_id) ?>
                     </td>
                     <td>
                        <?php if (empty($artifactsDateReferenced->day_no)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsDateReferenced->day_no) ?>
                     </td>
                  </tr>
                  <?php endforeach; ?>
               </tbody>
            </table>
         </div>
         <?= $this->Accordion->partClose() ?>
         <?php endif; ?>
         <?php if (!empty($artifact->artifacts_seals)): ?>
         <?= $this->Accordion->partOpen('related_artifacts_seals', 'Related Artifacts Seals', 'h2') ?>
         <div class="related">
            <table cellpadding="0" cellspacing="0">
               <thead>
                  <tr>
                     <th scope="col"><?= __('Id') ?></th>
                     <th scope="col"><?= __('Seal No') ?></th>
                     <th scope="col"><?= __('Artifact Id') ?></th>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach ($artifact->artifacts_seals as $artifactsSeals): ?>
                  <tr>
                     <td>
                        <?php if (empty($artifactsSeals->id)): ?>
                        -
                        <?php endif; ?>
                        <a href="/artifacts-seals/<?= $artifactsSeals->id ?>" >
                        <?= h($artifactsSeals->id) ?>
                        </a>
                     </td>
                     <td>
                        <?php if (empty($artifactsSeals->seal_no)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsSeals->seal_no) ?>
                     </td>
                     <td>
                        <?php if (empty($artifactsSeals->artifact_id)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsSeals->artifact_id) ?>
                     </td>
                  </tr>
                  <?php endforeach; ?>
               </tbody>
            </table>
         </div>
         <?= $this->Accordion->partClose() ?>
         <?php endif; ?>
         <?php if ($ifRoleExists) { ?>
         <?php if (!empty($artifact->artifacts_shadow)): ?>
         <?= $this->Accordion->partOpen('related_artifacts_shadow', 'Related Artifacts Shadow', 'h2') ?>
         <div class="related">
            <table cellpadding="0" cellspacing="0">
               <thead>
                  <tr>
                     <th scope="col"><?= __('Id') ?></th>
                     <th scope="col"><?= __('Artifact Id') ?></th>
                     <th scope="col"><?= __('Cdli Notes') ?></th>
                     <th scope="col"><?= __('Collection Location') ?></th>
                     <th scope="col"><?= __('Collection Notes') ?></th>
                     <th scope="col"><?= __('Acquisition History') ?></th>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach ($artifact->artifacts_shadow as $artifactsShadow): ?>
                  <tr>
                     <td>
                        <?php if (empty($artifactsShadow->id)): ?>
                        -
                        <?php endif; ?>
                        <a href="/artifacts-shadow/<?= $artifactsShadow->id ?>" >
                        <?= h($artifactsShadow->id) ?>
                        </a>
                     </td>
                     <td>
                        <?php if (empty($artifactsShadow->artifact_id)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsShadow->artifact_id) ?>
                     </td>
                     <td>
                        <?php if (empty($artifactsShadow->cdli_comments)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsShadow->cdli_comments) ?>
                     </td>
                     <td>
                        <?php if (empty($artifactsShadow->collection_location)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsShadow->collection_location) ?>
                     </td>
                     <td>
                        <?php if (empty($artifactsShadow->collection_comments)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsShadow->collection_comments) ?>
                     </td>
                     <td>
                        <?php if (empty($artifactsShadow->acquisition_history)): ?>
                        -
                        <?php endif; ?>
                        <?= h($artifactsShadow->acquisition_history) ?>
                     </td>
                  </tr>
                  <?php endforeach; ?>
               </tbody>
            </table>
         </div>
         <?= $this->Accordion->partClose() ?>
         <?php endif; ?>
         <?php } ?>
         <?php if (!empty($artifact->retired_artifacts)): ?>
         <?= $this->Accordion->partOpen('related_retired_artifacts', 'Related Retired Artifacts', 'h2') ?>
         <div class="related">
            <table cellpadding="0" cellspacing="0">
               <thead>
                  <tr>
                     <th scope="col"><?= __('Id') ?></th>
                     <th scope="col"><?= __('Artifact Id') ?></th>
                     <th scope="col"><?= __('New Artifact Id') ?></th>
                     <th scope="col"><?= __('Artifact Remarks') ?></th>
                     <th scope="col"><?= __('Is Public') ?></th>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach ($artifact->retired_artifacts as $retiredArtifacts): ?>
                  <tr>
                     <td>
                        <?php if (empty($retiredArtifacts->id)): ?>
                        -
                        <?php endif; ?>
                        <a href="/retired-artifacts/<?= $retiredArtifacts->id ?>" >
                        <?= h($retiredArtifacts->id) ?>
                        </a>
                     </td>
                     <td>
                        <?php if (empty($retiredArtifacts->artifact_id)): ?>
                        -
                        <?php endif; ?><?= h($retiredArtifacts->artifact_id) ?>
                     </td>
                     <td>
                        <?php if (empty($retiredArtifacts->new_artifact_id)): ?>
                        -
                        <?php endif; ?><?= h($retiredArtifacts->new_artifact_id) ?>
                     </td>
                     <td>
                        <?php if (empty($retiredArtifacts->artifact_remarks)): ?>
                        -
                        <?php endif; ?><?= h($retiredArtifacts->artifact_remarks) ?>
                     </td>
                     <td>
                        <?php if (empty($retiredArtifacts->is_public)): ?>
                        -
                        <?php endif; ?><?= h($retiredArtifacts->is_public) ?>
                     </td>
                  </tr>
                  <?php endforeach; ?>
               </tbody>
            </table>
         </div>
         <?= $this->Accordion->partClose() ?>
         <?php endif; ?>
         <?= $this->Accordion->partOpen('Notes', 'Notes', 'h2') ?>
         <div>
            <?= __('General Notes') ?>:
            <?php if (empty($artifact->general_comments)): ?>
            -
            <?php endif; ?>
            <?= $this->Text->autoParagraph(h($artifact->general_comments)); ?>
         </div>
         <div>
            <?= __('CDLI Notes') ?>:
            <?php if (empty($artifact->cdli_comments)): ?>
            -
            <?php endif; ?>
            <?= $this->Text->autoParagraph(h($artifact->cdli_comments)); ?>
         </div>
         <?= $this->Accordion->partClose() ?>
      </div>
   </div>
</main>
<?= $this->Html->script('focus-visible.js') ?>
<?php echo $this->element('smoothscroll'); ?>
