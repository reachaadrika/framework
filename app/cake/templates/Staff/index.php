<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Staff[]|\Cake\Collection\CollectionInterface $staff
 */
use Cake\Routing\Router;
?>

<div class="staff index large-9 medium-8 columns content">
    <h3><?= __('Staff') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('author_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                <th scope="col"><?= $this->Paginator->sort('staff_type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
                <?php if ($access_granted): ?>
                    <th scope="col"><?= __('Action') ?></th>
                <?php endif ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($staff as $staff): ?>
            <tr>
                <td><?= $this->Number->format($staff->id) ?></td>
                <td><?= $staff->has('author') ? $this->Html->link($staff->author->id, ['controller' => 'Authors', 'action' => 'view', $staff->author->id]) : '' ?></td>
                <td>
                <?php
                $url = Router::url('/',true).'webroot/staff-img/'.h($staff->id);
                echo "<img class='staff-img' src='$url' alt='Image' onerror=this.src='/images/default.png'>";
                ?>
                </td>
                <td><?= $staff->has('staff_type') ? $this->Html->link($staff->staff_type->id, ['controller' => 'StaffTypes', 'action' => 'view', $staff->staff_type->id]) : '' ?></td>
                <td><?= h($staff->sequence) ?></td>
                <td>
                <?php if ($access_granted): ?>
                    <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $staff->id],
                        ['escape' => false , 'class' => "btn btn-warning btn-sm", 'title' => 'Edit']) ?>
                <?php endif ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->element('Paginator'); ?>
</div>
