<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'default';

?>

<div class="text-left">
  <img src="/images/broken-tablet.svg">
    <h2 class="mt-4">Oops! we can not find this page</h2>
    <p>The requested address was not found on this server. Invalid url!. Try again or contact the admin.</p>
    <a href="/">Go to Home!</a>
    <?php
        if (Configure::read('debug')) :
            $this->layout = 'dev_error';

            $this->assign('title', $message);
            $this->assign('templateName', 'error500.ctp');

            $this->start('file');
    ?>
        <?php if (!empty($error->queryString)) : ?>
            <p class="notice">
                <strong>SQL Query: </strong>
                <?= h($error->queryString) ?>
            </p>
        <?php endif; ?>
        <?php if (!empty($error->params)) : ?>
            <strong>SQL Query Params: </strong>
            <?php Debugger::dump($error->params) ?>
        <?php endif; ?>
        <?php if ($error instanceof Error) : ?>
            <strong>Error in: </strong>
            <?= sprintf('%s, line %s', str_replace(ROOT, 'ROOT', $error->getFile()), $error->getLine()) ?>
        <?php endif; ?>
        <?= $this->element('auto_table_warning') ?>
        <?php
            if (extension_loaded('xdebug')) :
                xdebug_print_function_stack();
            endif;

            $this->end();
        endif;
    ?>
</div>
