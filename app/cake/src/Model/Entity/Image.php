<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Image Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property string|null $file_name
 * @property int|null $ppi
 * @property int|null $height
 * @property int|null $width
 * @property string|null $image_type
 * @property string|null $folder_name
 * @property float|null $size_mb
 * @property int|null $size_pixels
 * @property string|null $creation_date
 * @property string|null $modify_date
 * @property string|null $rgb
 * @property string|null $format
 * @property int|null $bit
 * @property bool|null $is_public
 * @property \App\Model\Entity\Artifact $artifact
 */
class Image extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'file_name' => true,
        'ppi' => true,
        'height' => true,
        'width' => true,
        'image_type' => true,
        'folder_name' => true,
        'size_mb' => true,
        'size_pixels' => true,
        'creation_date' => true,
        'modify_date' => true,
        'rgb' => true,
        'format' => true,
        'bit' => true,
        'is_public' => true,
        'artifact' => true,
    ];
}
