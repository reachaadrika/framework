<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SubmissionComment Entity
 *
 * @property int $comment_id
 * @property int|null $comment_type
 * @property int $role_id
 * @property int $submission_id
 * @property int $assoc_id
 * @property int $author_id
 * @property string|null $comment_title
 * @property string|null $comments
 * @property \Cake\I18n\FrozenTime|null $date_posted
 * @property \Cake\I18n\FrozenTime|null $date_modified
 * @property int|null $viewable
 *
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\Submission $submission
 * @property \App\Model\Entity\Assoc $assoc
 * @property \App\Model\Entity\Author $author
 */
class SubmissionComment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'comment_type' => true,
        'role_id' => true,
        'submission_id' => true,
        'assoc_id' => true,
        'author_id' => true,
        'comment_title' => true,
        'comments' => true,
        'date_posted' => true,
        'date_modified' => true,
        'viewable' => true,
        'role' => true,
        'submission' => true,
        'assoc' => true,
        'author' => true,
    ];
}
