<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Author Entity
 *
 * @property int $id
 * @property string $author
 * @property string|null $last
 * @property string|null $first
 * @property bool $east_asian_order
 * @property string|null $email
 * @property string|null $institution
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property string|null $orcid_id
 * @property bool $deceased
 *
 * @property \App\Model\Entity\SignReadingsComment[] $sign_readings_comments
 * @property \App\Model\Entity\Staff[] $staff
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Article[] $articles
 * @property \App\Model\Entity\Publication[] $publications
 * @property \App\Model\Entity\UpdateEvent[] $update_events
 */
class Author extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'author' => true,
        'last' => true,
        'first' => true,
        'east_asian_order' => true,
        'email' => true,
        'institution' => true,
        'modified' => true,
        'orcid_id' => true,
        'deceased' => true,
        'sign_readings_comments' => true,
        'staff' => true,
        'users' => true,
        'articles' => true,
        'publications' => true,
        'update_events' => true
    ];

    protected function _setEmail($value)
    {
        if ($value == '') {
            $value = null;
        }

        return $value;
    }
}
