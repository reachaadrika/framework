<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsUpdate Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property string|null $cdli_comments
 * @property string|null $designation
 * @property string|null $artifact_type
 * @property string|null $period
 * @property string|null $provenience
 * @property string|null $written_in
 * @property string|null $archive
 * @property string|null $composite_no
 * @property string|null $seal_no
 * @property string|null $composites
 * @property string|null $seals
 * @property string|null $museum_no
 * @property string|null $accession_no
 * @property string|null $condition_description
 * @property string|null $artifact_preservation
 * @property string|null $period_comments
 * @property string|null $provenience_comments
 * @property string|null $is_provenience_uncertain
 * @property string|null $is_period_uncertain
 * @property string|null $is_artifact_type_uncertain
 * @property string|null $is_school_text
 * @property string|null $height
 * @property string|null $thickness
 * @property string|null $width
 * @property string|null $weight
 * @property string|null $elevation
 * @property string|null $excavation_no
 * @property string|null $findspot_square
 * @property string|null $findspot_comments
 * @property string|null $stratigraphic_level
 * @property string|null $surface_preservation
 * @property string|null $artifact_comments
 * @property string|null $seal_information
 * @property string|null $accounting_period
 * @property string|null $is_public
 * @property string|null $is_atf_public
 * @property string|null $are_images_public
 * @property string|null $collections
 * @property string|null $dates
 * @property string|null $alternative_years
 * @property string|null $external_resources
 * @property string|null $external_resources_key
 * @property string|null $genres
 * @property string|null $genres_comment
 * @property string|null $genres_uncertain
 * @property string|null $languages
 * @property string|null $languages_uncertain
 * @property string|null $materials
 * @property string|null $materials_aspect
 * @property string|null $materials_color
 * @property string|null $materials_uncertain
 * @property string|null $shadow_cdli_comments
 * @property string|null $shadow_collection_location
 * @property string|null $shadow_collection_comments
 * @property string|null $shadow_acquisition_history
 * @property string|null $publications_key
 * @property string|null $publications_type
 * @property string|null $publications_exact_ref
 * @property string|null $publications_comment
 * @property string|null $retired
 * @property string|null $has_fragments
 * @property string|null $is_artifact_fake
 * @property int $update_events_id
 * @property string $publication_error
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\UpdateEvent $update_event
 */
class ArtifactsUpdate extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'cdli_comments' => true,
        'designation' => true,
        'artifact_type' => true,
        'period' => true,
        'provenience' => true,
        'written_in' => true,
        'archive' => true,
        'composite_no' => true,
        'seal_no' => true,
        'composites' => true,
        'seals' => true,
        'museum_no' => true,
        'accession_no' => true,
        'condition_description' => true,
        'artifact_preservation' => true,
        'period_comments' => true,
        'provenience_comments' => true,
        'is_provenience_uncertain' => true,
        'is_period_uncertain' => true,
        'is_artifact_type_uncertain' => true,
        'is_school_text' => true,
        'height' => true,
        'thickness' => true,
        'width' => true,
        'weight' => true,
        'elevation' => true,
        'excavation_no' => true,
        'findspot_square' => true,
        'findspot_comments' => true,
        'stratigraphic_level' => true,
        'surface_preservation' => true,
        'artifact_comments' => true,
        'seal_information' => true,
        'accounting_period' => true,
        'is_public' => true,
        'is_atf_public' => true,
        'are_images_public' => true,
        'collections' => true,
        'dates' => true,
        'alternative_years' => true,
        'external_resources' => true,
        'external_resources_key' => true,
        'genres' => true,
        'genres_comment' => true,
        'genres_uncertain' => true,
        'languages' => true,
        'languages_uncertain' => true,
        'materials' => true,
        'materials_aspect' => true,
        'materials_color' => true,
        'materials_uncertain' => true,
        'shadow_cdli_comments' => true,
        'shadow_collection_location' => true,
        'shadow_collection_comments' => true,
        'shadow_acquisition_history' => true,
        'publications_key' => true,
        'publications_type' => true,
        'publications_exact_ref' => true,
        'publications_comment' => true,
        'retired' => true,
        'has_fragments' => true,
        'is_artifact_fake' => true,
        'update_events_id' => true,
        'publication_error' => true,
        'artifact' => true,
        'update_event' => true,
    ];
}
