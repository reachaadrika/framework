<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Section Entity
 *
 * @property int $section_id
 * @property int $journal_id
 * @property int|null $review_form_id
 * @property float $seq
 * @property int $editor_restricted
 * @property int $meta_indexed
 * @property int $meta_reviewed
 * @property int $abstracts_not_required
 * @property int $hide_title
 * @property int $hide_author
 * @property int|null $abstract_word_count
 *
 * @property \App\Model\Entity\Journal $journal
 * @property \App\Model\Entity\ReviewForm $review_form
 */
class Section extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'journal_id' => true,
        'review_form_id' => true,
        'seq' => true,
        'editor_restricted' => true,
        'meta_indexed' => true,
        'meta_reviewed' => true,
        'abstracts_not_required' => true,
        'hide_title' => true,
        'hide_author' => true,
        'abstract_word_count' => true,
        'journal' => true,
        'review_form' => true,
    ];
}
