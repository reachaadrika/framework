<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use App\Model\Entity\TableExportTrait;

/**
 * Artifact Entity
 *
 * @property int $id
 * @property string|null $ark_no
 * @property string|null $cdli_comments
 * @property string|null $composite_no
 * @property string|null $condition_description
 * @property string|null $designation
 * @property string|null $elevation
 * @property string|null $excavation_no
 * @property string|null $findspot_comments
 * @property string|null $findspot_square
 * @property string|null $museum_no
 * @property string|null $artifact_preservation
 * @property bool|null $is_public
 * @property bool|null $is_atf_public
 * @property bool|null $are_images_public
 * @property string|null $seal_no
 * @property string|null $seal_information
 * @property string|null $stratigraphic_level
 * @property string|null $surface_preservation
 * @property string|null $thickness
 * @property string|null $height
 * @property string|null $width
 * @property string|null $weight
 * @property int|null $provenience_id
 * @property int|null $period_id
 * @property bool|null $is_provenience_uncertain
 * @property bool|null $is_period_uncertain
 * @property int|null $artifact_type_id
 * @property string|null $accession_no
 * @property string|null $alternative_years
 * @property string|null $period_comments
 * @property string|null $provenience_comments
 * @property bool|null $is_school_text
 * @property int|null $written_in
 * @property bool|null $is_artifact_type_uncertain
 * @property int|null $archive_id
 * @property string|null $dates_referenced
 * @property string|null $accounting_period
 * @property string|null $artifact_comments
 * @property int|null $created_by
 * @property bool $retired
 * @property bool $has_fragments
 * @property int $is_artifact_fake
 *
 * @property \App\Model\Entity\Provenience $provenience
 * @property \App\Model\Entity\Provenience $origin
 * @property \App\Model\Entity\Period $period
 * @property \App\Model\Entity\ArtifactType $artifact_type
 * @property \App\Model\Entity\Archive $archive
 * @property \App\Model\Entity\ArtifactsShadow[] $artifacts_shadow
 * @property \App\Model\Entity\ArtifactsUpdate[] $artifacts_updates
 * @property \App\Model\Entity\Inscription $inscription
 * @property \App\Model\Entity\Posting[] $postings
 * @property \App\Model\Entity\RetiredArtifact[] $retired_artifacts
 * @property \App\Model\Entity\Collection[] $collections
 * @property \App\Model\Entity\Date[] $dates
 * @property \App\Model\Entity\ExternalResource[] $external_resources
 * @property \App\Model\Entity\Genre[] $genres
 * @property \App\Model\Entity\Language[] $languages
 * @property \App\Model\Entity\Material[] $materials
 * @property \App\Model\Entity\MaterialAspect[] $material_aspects
 * @property \App\Model\Entity\MaterialColor[] $material_colors
 * @property \App\Model\Entity\Publication[] $publications
 * @property \App\Model\Entity\Artifact[] $witnesses
 * @property \App\Model\Entity\Artifact[] $composites
 * @property \App\Model\Entity\Artifact[] $impressions
 * @property \App\Model\Entity\Artifact[] $seals
 */
class Artifact extends Entity
{
    use TableExportTrait;
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ark_no' => true,
        'cdli_comments' => true,
        'composite_no' => true,
        'condition_description' => true,
        'designation' => true,
        'elevation' => true,
        'excavation_no' => true,
        'findspot_comments' => true,
        'findspot_square' => true,
        'museum_no' => true,
        'artifact_preservation' => true,
        'is_public' => true,
        'is_atf_public' => true,
        'are_images_public' => true,
        'seal_no' => true,
        'seal_information' => true,
        'stratigraphic_level' => true,
        'surface_preservation' => true,
        'thickness' => true,
        'height' => true,
        'width' => true,
        'weight' => true,
        'provenience_id' => true,
        'period_id' => true,
        'is_provenience_uncertain' => true,
        'is_period_uncertain' => true,
        'artifact_type_id' => true,
        'accession_no' => true,
        'alternative_years' => true,
        'period_comments' => true,
        'provenience_comments' => true,
        'is_school_text' => true,
        'written_in' => true,
        'is_artifact_type_uncertain' => true,
        'archive_id' => true,
        'dates_referenced' => true,
        'accounting_period' => true,
        'artifact_comments' => true,
        'created_by' => true,
        'retired' => true,
        'has_fragments' => true,
        'is_artifact_fake' => true,
        'provenience' => true,
        'origin' => true,
        'period' => true,
        'artifact_type' => true,
        'archive' => true,
        'artifacts_shadow' => true,
        'artifacts_updates' => true,
        'inscription' => true,
        'postings' => true,
        'retired_artifacts' => true,
        'collections' => true,
        'dates' => true,
        'external_resources' => true,
        'genres' => true,
        'languages' => true,
        'materials' => true,
        'material_aspects' => true,
        'material_colors' => true,
        'publications' => true,
        'witnesses' => true,
        'composites' => true,
        'impressions' => true,
        'seals' => true,
    ];

    public function getTableRow()
    {
        return [
            'artifact_id' => $this->id,
            'comments' => null,
            'designation' => $this->designation,
            'artifact_type' => $this->serializeDisplay($this->artifact_type, 'artifact_type'),
            'period' => $this->serializeDisplay($this->period, 'period'),
            'provenience' => $this->serializeDisplay($this->provenience, 'provenience'),
            'written_in' => $this->written_in,
            'archive' => $this->serializeDisplay($this->archive, 'archive'),
            'composite_no' => $this->composite_no,
            'seal_no' => $this->seal_no,
            'composites' => $this->serializeListAll(
                $this->artifacts_composites,
                function ($composite) {
                    return $composite->composite_no;
                }
            ),
            'seals' => $this->serializeListAll(
                $this->artifacts_seals,
                function ($seal) {
                    return $seal->seal_no;
                }
            ),
            'museum_no' => $this->museum_no,
            'accession_no' => $this->accession_no,
            'condition_description' => $this->condition_description,
            'artifact_preservation' => $this->artifact_preservation,
            'period_comments' => $this->period_comments,
            'provenience_comments' => $this->provenience_comments,
            'is_provenience_uncertain' => $this->is_provenience_uncertain,
            'is_period_uncertain' => $this->is_period_uncertain,
            'is_artifact_type_uncertain' => $this->is_artifact_type_uncertain,
            'is_school_text' => $this->is_school_text,
            'height' => $this->height,
            'thickness' => $this->thickness,
            'width' => $this->width,
            'weight' => $this->weight,
            'elevation' => $this->elevation,
            'excavation_no' => $this->excavation_no,
            'findspot_square' => $this->findspot_square,
            'findspot_comments' => $this->findspot_comments,
            'stratigraphic_level' => $this->stratigraphic_level,
            'surface_preservation' => $this->surface_preservation,
            'artifact_comments' => $this->artifact_comments,
            'seal_information' => $this->seal_information,
            'is_public' => $this->is_public,
            'is_atf_public' => $this->is_atf_public,
            'are_images_public' => $this->are_images_public,
            'artifacts_collections' => $this->serializeListFirst($this->collections, 'collection'),
            'artifacts_dates' => $this->dates_referenced,
            'alternative_years' => $this->alternative_years,
            'artifacts_genres' => $this->serializeListFirst($this->genres, 'genre'),
            'artifacts_languages' => $this->serializeListFirst($this->languages, 'language'),
            'artifacts_materials' => $this->serializeListFirst($this->materials, 'material'),
            'artifacts_shadow_cdli_comments' => $this->serializeListFirst($this->artifacts_shadow, 'cdli_comments'),
            'artifacts_shadow_collection_location' => $this->serializeListFirst($this->artifacts_shadow, 'collection_location'),
            'artifacts_shadow_collection_comments' => $this->serializeListFirst($this->artifacts_shadow, 'collection_comments'),
            'artifacts_shadow_acquisition_history' => $this->serializeListFirst($this->artifacts_shadow, 'acquisition_history'),
            'pubications_key' => $this->serializeListAll(
                $this->publications,
                function ($publication) {
                    return $publication->bibtexkey;
                }
            ),
            'publications_type' => $this->serializeListAll(
                $this->publications,
                function ($publication) {
                    return $publication->_joinData->publication_type;
                }
            ),
            'publications_exact_ref' => $this->serializeListAll(
                $this->publications,
                function ($publication) {
                    return $publication->_joinData->exact_reference;
                }
            ),
            'publications_comment' => $this->serializeListAll(
                $this->publications,
                function ($publication) {
                    return $publication->_joinData->publication_comments;
                }
            )
        ];
    }

    public function getCdliNumber()
    {
        return 'P' . str_pad($this->id, 6, '0', STR_PAD_LEFT);
    }

    private static function getSeparatedIdentifiers($ids, $kind)
    {
        return array_map(function ($id) use ($kind) {
            return self::getIdentifier($id, $kind);
        }, explode(" + ", $ids));
    }

    public function getCidocCrm()
    {
        $cdli = $this->getCdliNumber();
        return [
            '@id' => $cdli,
            '@type' => empty($this->witnesses)
                ? 'crm:E84_Information_Carrier'
                : 'crm:E33_Linguistic_Object',
            'crm:P48_has_preferred_identifier' => array_merge(
                [
                    self::getIdentifier($cdli, 'cdli'),
                    self::getIdentifier($this->designation, 'designation'),
                    self::getIdentifier($this->accession_no, 'accession'),
                    self::getIdentifier($this->ark_no, 'ark'),
                    self::getIdentifier($this->composite_no, 'composite'),
                    self::getIdentifier($this->seal_no, 'seal')
                ],
                self::getSeparatedIdentifiers($this->museum_no, 'museum'),
                self::getSeparatedIdentifiers($this->excavation_no, 'excavation')
            ),
            'crm:P1_is_identified_by' => empty($this->external_resources)
                ? null
                : array_map(function ($resource) {
                    return self::getEntity($resource->_joinData);
                }, $this->external_resources),
            'crm:P43_has_dimension' => [
                self::getDimension($this->width, 'width', 'mm'),
                self::getDimension($this->height, 'height', 'mm'),
                self::getDimension($this->thickness, 'thickness', 'mm'),
                self::getDimension($this->weight, 'weight', 'g')
            ],
            'crm:P128_carries' => self::getEntity($this->inscription),
            'crm:P108i_was_produced_by' => [
                '@type' => 'crm:E12_Production',
                'crm:P10_falls_within' => self::getEntity($this->period),
                'crm:P7_took_place_at' => self::getEntity($this->origin)
            ],
            'crm:P46i_forms_part_of' => self::getEntities($this->collections),
            'crm:P67i_is_referred_to_by' => self::getEntities($this->publications),
            'crm:P44_has_condition' => [
                '@type' => 'crm:E3_Condition_State',
                'crm:P5_consists_of' => [
                    '@type' => 'crm:E3_Condition_State',
                    'rdfs:value' => $this->surface_preservation
                ],
                'rdfs:value' => $this->artifact_preservation,
                'crm:P3_has_note' => $this->condition_description
            ],
            'crm:P45_consists_of' => self::getEntities($this->materials),
            'crm:P56_bears_feature' => array_merge(
                empty($this->material_aspects) ? [] : self::getEntities($this->material_aspects),
                empty($this->material_colors) ? [] : self::getEntities($this->material_colors)
            ),
            'crm:P53_has_former_or_current_location' => self::getEntity($this->archive),
            'crm:P2_has_type' => array_merge(
                self::getEntities($this->genres),
                [
                    self::getEntity($this->artifact_type)
                ]
            ),
            'crma:AP21i_is_contained_in' => [
                '@type' => 'crma:A2_Stratigraphic_Volume_Unit',
                'crm:P1_is_identified_by' => [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->stratigraphic_level
                ],
                'crma:AP5i_was_partially_or_totally_removed_by' => [
                    '@type' => 'crma:A1_Excavation_Process_Unit',
                    'crm:P7_took_place_at' => [
                        '@type' => 'crm:E53_Place',
                        'crm:P48_has_preferred_identifier' => self::getIdentifier($this->findspot_square, 'findspot'),
                        'crm:P3_has_note' => $this->findspot_comments,
                        'crm:P89_falls_within' => self::getEntity($this->provenience)
                    ]
                ]
            ],
            'crm:P148_has_component' => empty($this->witnesses) ? null : self::getEntities(
                array_map(function ($artifact) {
                    return $artifact->inscription;
                }, $this->witnesses)
            ),
            'crm:P31i_was_modified_by' => empty($this->seals) ? null : array_map(function ($artifact) {
                return [
                    '@type' => 'crm:E11_Modification',
                    'crm:P16_used_specific_object' => $artifact
                ];
            }, self::getEntities($this->seals)),
            'crm:P16i_was_used_for' => empty($this->impressions) ? null : array_map(function ($artifact) {
                return [
                    '@type' => 'crm:E11_Modification',
                    'crm:P31_has_modified' => $artifact
                ];
            }, self::getEntities($this->impressions))
            // TODO retired
        ];
    }
}
