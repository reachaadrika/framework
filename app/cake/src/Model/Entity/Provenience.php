<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Provenience Entity
 *
 * @property int $id
 * @property string|null $provenience
 * @property int|null $region_id
 * @property string|null $geo_coordinates
 * @property string $ancient_name
 * @property string $modern_name
 * @property int $pleiades_id
 *
 * @property \App\Model\Entity\Region $region
 * @property \App\Model\Entity\Archive[] $archives
 * @property \App\Model\Entity\Artifact[] $artifacts
 * @property \App\Model\Entity\Dynasty[] $dynasties
 * @property \App\Model\Entity\SignReading[] $sign_readings
 */
class Provenience extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'provenience' => true,
        'region_id' => true,
        'geo_coordinates' => true,
        'ancient_name' => true,
        'modern_name' => true,
        'pleiades_id' => true,
        'region' => true,
        'archives' => true,
        'artifacts' => true,
        'dynasties' => true,
        'sign_readings' => true
    ];

    public function getCidocCrm()
    {
        if ($this->provenience == 'Elbonia') {
            return null;
        }

        preg_match('/(.+) \\(mod. (.+?)\\)/', $this->provenience, $matches);

        [$lat, $long] = empty($this->geo_coordinates)
            ? [null, null]
            : explode(', ', $this->geo_coordinates);

        $provenience = [
            '@id' => $this->getUri(),
            '@type' => 'crm:E53_Place',
            'crm:P87_is_identified_by' => [
                empty($matches[1]) ? null : [
                    '@type' => 'crm:E48_Place_Name',
                    'rdfs:label' => $matches[1]
                ],
                empty($matches[2]) ? null : [
                    '@type' => 'crm:E48_Place_Name',
                    'rdfs:label' => $matches[2]
                ],
                empty($this->geo_coordinates) ? null : [
                    '@type' => 'crm:E47_Spatial_Coordinates',
                    'rdfs:label' => $this->geo_coordinates
                ]
            ],
            'geo:lat' => $lat,
            'geo:long' => $long,
            'crm:P7i_witnessed' => self::getEntities($this->dynasties),
            'crm:P89_falls_within' => self::getEntity($this->region),
            'crm:P89i_contains' => array_merge(
                self::getEntities($this->archives),
                !is_array($this->artifacts) ? [] : array_map(function ($artifact) {
                    return [
                        '@type' => 'crm:E53_Place',
                        'crm:P48_has_preferred_identifier' => self::getIdentifier($artifact->findspot_square, 'findspot'),
                        'crm:P3_has_note' => $artifact->findspot_comments,
                        'crm:P7i_witnessed' => [
                            '@type' => 'crma:A1_Excavation_Process_Unit',
                            'crma:AP5_removed_all_or_part_of' => [
                                '@type' => 'crma:A2_Stratigraphic_Volume_Unit',
                                'crm:P1_is_identified_by' => [
                                    '@type' => 'crm:E41_Appellation',
                                    'rdfs:label' => $artifact->stratigraphic_level
                                ],
                                'crma:AP21_contains' => self::getEntity($artifact)
                            ]
                        ]
                    ];
                }, $this->artifacts)
            )
            // TODO sign_readings
        ];

        return $provenience;
    }
}
