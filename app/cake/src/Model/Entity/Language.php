<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Language Entity
 *
 * @property int $id
 * @property int|null $sequence
 * @property int|null $parent_id
 * @property string $language
 * @property string|null $protocol_code
 * @property string|null $inline_code
 * @property string|null $notes
 *
 * @property \App\Model\Entity\Language $parent_language
 * @property \App\Model\Entity\Language[] $child_languages
 * @property \App\Model\Entity\SignReading[] $sign_readings
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Language extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sequence' => true,
        'parent_id' => true,
        'language' => true,
        'protocol_code' => true,
        'inline_code' => true,
        'notes' => true,
        'parent_language' => true,
        'child_languages' => true,
        'sign_readings' => true,
        'artifacts' => true
    ];

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'crm:E56_Language',
            'crm:P1_is_identified_by' => [
                empty($this->inline_code) ? null : [
                    'rdfs:label' => $this->inline_code,
                    '@type' => 'cdli:identifier_iso-639'
                ],
                empty($this->protocol_code) ? null : [
                    'rdfs:label' => $this->protocol_code,
                    '@type' => 'crm:E41_Appellation'
                ],
                empty($this->language) ? null : [
                    'rdfs:label' => $this->language,
                    '@type' => 'crm:E41_Appellation'
                ]
            ],
            'crm:P3_has_note' => $this->notes,
            'crm:P127_has_broader_term' => $this->getUri($this->parent_id),
            'crm:P127i_has_narrower_term' => self::getUris($this->child_languages),
            'crm:P72i_is_language_of' => !is_array($this->artifacts) ? [] : self::getEntities(
                array_map(function ($artifact) {
                    return $artifact->inscription;
                }, $this->artifacts)
            )
            // TODO sign readings
        ];
    }
}
