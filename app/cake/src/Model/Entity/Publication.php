<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use App\Model\Entity\TableExportTrait;
use Cake\Utility\Inflector;

/**
 * Publication Entity
 *
 * @property int $id
 * @property string|null $bibtexkey
 * @property string|null $year
 * @property int|null $entry_type_id
 * @property string|null $address
 * @property string|null $annote
 * @property string|null $book_title
 * @property string|null $chapter
 * @property string|null $crossref
 * @property string|null $edition
 * @property string|null $editor
 * @property string|null $how_published
 * @property string|null $institution
 * @property int|null $journal_id
 * @property string|null $month
 * @property string|null $note
 * @property string|null $number
 * @property string|null $organization
 * @property string|null $pages
 * @property string|null $publisher
 * @property string|null $school
 * @property string|null $title
 * @property string|null $volume
 * @property string|null $publication_history
 * @property string|null $series
 * @property int|null $oclc
 * @property string|null $designation
 * @property int $accepted_by
 * @property bool $accepted
 *
 * @property \App\Model\Entity\EntryType $entry_type
 * @property \App\Model\Entity\Journal $journal
 * @property \App\Model\Entity\Abbreviation[] $abbreviations
 * @property \App\Model\Entity\Article[] $articles
 * @property \App\Model\Entity\Artifact[] $artifacts
 * @property \App\Model\Entity\Author[] $authors
 * @property \App\Model\Entity\Editor[] $editors
 */
class Publication extends Entity
{
    use TableExportTrait;
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'bibtexkey' => true,
        'year' => true,
        'entry_type_id' => true,
        'address' => true,
        'annote' => true,
        'book_title' => true,
        'chapter' => true,
        'crossref' => true,
        'edition' => true,
        'editor' => true,
        'how_published' => true,
        'institution' => true,
        'journal_id' => true,
        'month' => true,
        'note' => true,
        'number' => true,
        'organization' => true,
        'pages' => true,
        'publisher' => true,
        'school' => true,
        'title' => true,
        'volume' => true,
        'publication_history' => true,
        'series' => true,
        'oclc' => true,
        'designation' => true,
        'accepted_by' => true,
        'accepted' => true,
        'entry_type' => true,
        'journal' => true,
        'abbreviations' => true,
        'articles' => true,
        'artifacts' => true,
        'authors' => true,
        'editors' => true
    ];

    public function getTableRow()
    {
        return [
            'bibtexkey' => $this->bibtexkey,
            'authors' => $this->serializeListAll(
                $this->authors,
                function ($author) {
                    return $author->author;
                }
            ),
            'year' => $this->year,
            'entry_type' => $this->serializeDisplay($this->entry_type, 'label'),
            'address' => $this->address,
            'annote' => $this->annote,
            'book_title' => $this->book_title,
            'chapter' => $this->chapter,
            'crossref' => $this->crossref,
            'edition' => $this->edition,
            'editors' => TableExportTrait::serializeListAll(
                $this->editors,
                function ($editor) {
                    return $editor->author;
                }
            ),
            'how_published' => $this->how_published,
            'institution' => $this->institution,
            'journal' => $this->serializeDisplay($this->journal, 'journal'),
            'month' => $this->month,
            'note' => $this->note,
            'number' => $this->number,
            'organization' => $this->organization,
            'pages' => $this->pages,
            'publisher' => $this->publisher,
            'school' => $this->school,
            'title' => $this->title,
            'volume' => $this->volume,
            'publication_history' => $this->publication_history,
            'series' => $this->series,
            'designation' => $this->designation
        ];
    }

    private function getEntryType()
    {
        if (!empty($this->entry_type)) {
            return 'bibtex:' . ucfirst($this->entry_type->label);
        } else {
            return 'bibtex:Entry';
        }
    }

    private static function getCreators($creators)
    {
        if (!is_array($creators)) {
            return null;
        }

        return implode(' and ', array_map(function ($creator) {
            return $creator->author;
        }, $creators));
    }

    public function getCidocCrm()
    {
        $publication = [
            '@id' => $this->getUri(),
            '@type' => [
                'crm:E89_Propositional_Object',
                $this->getEntryType()
            ],
            'bibtex:hasKey' => $this->bibtexkey,
            'bibtex:hasAddress' => $this->address,
            'bibtex:hasAnnotation' => $this->annote,
            'bibtex:hasAuthor' => self::getCreators($this->authors),
            'bibtex:hasBooktitle' => $this->book_title,
            'bibtex:hasChapter' => $this->chapter,
            'bibtex:hasCrossref' => $this->crossref,
            'bibtex:hasEdition' => $this->edition,
            'bibtex:hasEditor' => self::getCreators($this->editors),
            'bibtex:howPublished' => $this->how_published,
            'bibtex:hasInstitution' => $this->institution,
            'bibtex:hasJournal' => $this->serializeDisplay($this->journal, 'journal'),
            'bibtex:hasMonth' => $this->month,
            'bibtex:hasNote' => $this->note,
            'bibtex:hasNumber' => $this->number,
            'bibtex:hasOrganization' => $this->organization,
            'bibtex:hasPages' => $this->pages,
            'bibtex:hasPublisher' => $this->publisher,
            'bibtex:hasSchool' => $this->school,
            'bibtex:hasSeries' => $this->series,
            'bibtex:hasTitle' => $this->title,
            'bibtex:hasType' => $this->type,
            'bibtex:hasVolume' => $this->volume,
            'bibtex:hasYear' => $this->year
        ];

        foreach ($publication as $key => $value) {
            if (empty($value)) {
                unset($publication[$key]);
            }
        }

        return $publication;
    }
}
