<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SubmissionFile Entity
 *
 * @property int $file_id
 * @property int $revision
 * @property int|null $source_file_id
 * @property int|null $source_revision
 * @property int $submission_id
 * @property string $file_type
 * @property int|null $genre_id
 * @property int $file_size
 * @property string|null $original_file_name
 * @property int $file_stage
 * @property string|null $direct_sales_price
 * @property string|null $sales_type
 * @property int|null $viewable
 * @property \Cake\I18n\FrozenTime $date_uploaded
 * @property \Cake\I18n\FrozenTime $date_modified
 * @property int|null $uploader_user_id
 * @property int|null $assoc_type
 * @property int|null $assoc_id
 *
 * @property \App\Model\Entity\SourceFile $source_file
 * @property \App\Model\Entity\Submission $submission
 * @property \App\Model\Entity\Genre $genre
 * @property \App\Model\Entity\UploaderUser $uploader_user
 * @property \App\Model\Entity\Assoc $assoc
 */
class SubmissionFile extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'source_file_id' => true,
        'source_revision' => true,
        'submission_id' => true,
        'file_type' => true,
        'genre_id' => true,
        'file_size' => true,
        'original_file_name' => true,
        'file_stage' => true,
        'direct_sales_price' => true,
        'sales_type' => true,
        'viewable' => true,
        'date_uploaded' => true,
        'date_modified' => true,
        'uploader_user_id' => true,
        'assoc_type' => true,
        'assoc_id' => true,
        'source_file' => true,
        'submission' => true,
        'genre' => true,
        'uploader_user' => true,
        'assoc' => true,
    ];
}
