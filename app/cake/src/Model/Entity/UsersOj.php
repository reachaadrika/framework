<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UsersOj Entity
 *
 * @property int $user_id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string|null $url
 * @property string|null $phone
 * @property string|null $mailing_address
 * @property string|null $billing_address
 * @property string|null $country
 * @property string|null $locales
 * @property string|null $gossip
 * @property \Cake\I18n\FrozenTime|null $date_last_email
 * @property \Cake\I18n\FrozenTime $date_registered
 * @property \Cake\I18n\FrozenTime|null $date_validated
 * @property \Cake\I18n\FrozenTime $date_last_login
 * @property int|null $must_change_password
 * @property int|null $auth_id
 * @property string|null $auth_str
 * @property int $disabled
 * @property string|null $disabled_reason
 * @property int|null $inline_help
 *
 * @property \App\Model\Entity\Auth $auth
 * @property \App\Model\Entity\EmailLog[] $email_log
 * @property \App\Model\Entity\UserSetting[] $user_settings
 */
class UsersOj extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'password' => true,
        'email' => true,
        'url' => true,
        'phone' => true,
        'mailing_address' => true,
        'billing_address' => true,
        'country' => true,
        'locales' => true,
        'gossip' => true,
        'date_last_email' => true,
        'date_registered' => true,
        'date_validated' => true,
        'date_last_login' => true,
        'must_change_password' => true,
        'auth_id' => true,
        'auth_str' => true,
        'disabled' => true,
        'disabled_reason' => true,
        'inline_help' => true,
        'auth' => true,
        'email_log' => true,
        'user_settings' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];
}
