<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AuthorsUpdateEvent Entity
 *
 * @property int $id
 * @property int $update_event_id
 * @property int $author_id
 *
 * @property \App\Model\Entity\UpdateEvent $update_event
 * @property \App\Model\Entity\Author $author
 */
class AuthorsUpdateEvent extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'update_event_id' => true,
        'author_id' => true,
        'update_event' => true,
        'author' => true
    ];
}
