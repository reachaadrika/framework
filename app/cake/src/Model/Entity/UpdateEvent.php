<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UpdateEvent Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property int|null $created_by
 * @property int|null $external_resource_id
 * @property string|null $update_type
 * @property string|null $event_comments
 * @property int|null $approved_by
 * @property string $status
 *
 * @property \App\Model\Entity\Author[] $authors
 */
class UpdateEvent extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'created_by' => true,
        'external_resource_id' => true,
        'update_type' => true,
        'event_comments' => true,
        'approved_by' => true,
        'status' => true,
        'authors' => true,
    ];
}
