<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReviewAssignment Entity
 *
 * @property int $review_id
 * @property int $submission_id
 * @property int $reviewer_id
 * @property string|null $competing_interests
 * @property int|null $recommendation
 * @property \Cake\I18n\FrozenTime|null $date_assigned
 * @property \Cake\I18n\FrozenTime|null $date_notified
 * @property \Cake\I18n\FrozenTime|null $date_confirmed
 * @property \Cake\I18n\FrozenTime|null $date_completed
 * @property \Cake\I18n\FrozenTime|null $date_acknowledged
 * @property \Cake\I18n\FrozenTime|null $date_due
 * @property \Cake\I18n\FrozenTime|null $date_response_due
 * @property \Cake\I18n\FrozenTime|null $last_modified
 * @property int $reminder_was_automatic
 * @property int $declined
 * @property int $cancelled
 * @property int|null $reviewer_file_id
 * @property \Cake\I18n\FrozenTime|null $date_rated
 * @property \Cake\I18n\FrozenTime|null $date_reminded
 * @property int|null $quality
 * @property int|null $review_round_id
 * @property int $stage_id
 * @property int $review_method
 * @property int $round
 * @property int $step
 * @property int|null $review_form_id
 * @property int|null $unconsidered
 *
 * @property \App\Model\Entity\Submission $submission
 * @property \App\Model\Entity\ReviewRound $review_round
 * @property \App\Model\Entity\UserSetting[] $user_settings
 * @property \App\Model\Entity\SubmissionComment[] $submission_comments
 */
class ReviewAssignment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'submission_id' => true,
        'reviewer_id' => true,
        'competing_interests' => true,
        'recommendation' => true,
        'date_assigned' => true,
        'date_notified' => true,
        'date_confirmed' => true,
        'date_completed' => true,
        'date_acknowledged' => true,
        'date_due' => true,
        'date_response_due' => true,
        'last_modified' => true,
        'reminder_was_automatic' => true,
        'declined' => true,
        'cancelled' => true,
        'reviewer_file_id' => true,
        'date_rated' => true,
        'date_reminded' => true,
        'quality' => true,
        'review_round_id' => true,
        'stage_id' => true,
        'review_method' => true,
        'round' => true,
        'step' => true,
        'review_form_id' => true,
        'unconsidered' => true,
        'submission' => true,
        'reviewer' => true,
        'reviewer_file' => true,
        'review_round' => true,
        'stage' => true,
        'user_settings' => true,
        'submission_comments' => true,
    ];
}
