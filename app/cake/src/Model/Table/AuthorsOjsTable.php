<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AuthorsOjs Model
 *
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\BelongsTo $Publications
 * @property \App\Model\Table\SubmissionsTable&\Cake\ORM\Association\BelongsTo $Submissions
 * @property \App\Model\Table\UserGroupsTable&\Cake\ORM\Association\BelongsTo $UserGroups
 * @property \App\Model\Table\AuthorSettingsTable&\Cake\ORM\Association\HasMany $AuthorSettings
 *
 * @method \App\Model\Entity\AuthorsOj newEmptyEntity()
 * @method \App\Model\Entity\AuthorsOj newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\AuthorsOj[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AuthorsOj get($primaryKey, $options = [])
 * @method \App\Model\Entity\AuthorsOj findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\AuthorsOj patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AuthorsOj[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\AuthorsOj|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AuthorsOj saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AuthorsOj[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\AuthorsOj[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\AuthorsOj[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\AuthorsOj[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class AuthorsOjsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('authors');
        $this->setDisplayField('author_id');
        $this->setPrimaryKey('author_id');

        $this->belongsTo('Publications', [
            'foreignKey' => 'publication_id',
        ]);
        $this->belongsTo('Submissions', [
            'foreignKey' => 'submission_id',
        ]);
        $this->belongsTo('UserGroups', [
            'foreignKey' => 'user_group_id',
        ]);
        $this->hasMany('AuthorSettings');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('author_id', null, 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->notEmptyString('include_in_browse');

        $validator
            ->numeric('seq')
            ->notEmptyString('seq');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['publication_id'], 'Publications'), ['errorField' => 'publication_id']);
        $rules->add($rules->existsIn(['submission_id'], 'Submissions'), ['errorField' => 'submission_id']);
        $rules->add($rules->existsIn(['user_group_id'], 'UserGroups'), ['errorField' => 'user_group_id']);

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
