<?php
namespace App\Model\Table;

use Cake\Database\Expression\IdentifierExpression;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactsComposites Model
 *
 * @method \App\Model\Entity\ArtifactsComposite newEmptyEntity()
 * @method \App\Model\Entity\ArtifactsComposite newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsComposite[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsComposite get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsComposite findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactsComposite patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsComposite[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsComposite|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsComposite saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsComposite[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsComposite[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsComposite[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsComposite[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactsCompositesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifacts_composites');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Composites', [
            'bindingKey' => 'composite_no',
            'foreignKey' => 'composite_no',
            'className' => 'Artifacts',
            'conditions' => ['composite_no !=' => '']
        ]);
        $this->belongsTo('Witnesses', [
            'bindingKey' => 'id',
            'foreignKey' => 'artifact_id',
            'className' => 'Artifacts',
            'conditions' => [
                'Witnesses.composite_no !=' => new IdentifierExpression('composite_no')
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('composite_no')
            ->maxLength('composite_no', 10)
            ->allowEmptyString('composite_no');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'), ['errorField' => 'artifact_id']);

        return $rules;
    }
}
