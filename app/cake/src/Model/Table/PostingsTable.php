<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * Postings Model
 *
 * @property \App\Model\Table\PostingTypesTable&\Cake\ORM\Association\BelongsTo $PostingTypes
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 *
 * @method \App\Model\Entity\Posting newEmptyEntity()
 * @method \App\Model\Entity\Posting newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Posting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Posting get($primaryKey, $options = [])
 * @method \App\Model\Entity\Posting findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Posting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Posting[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Posting|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Posting saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Posting[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Posting[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Posting[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Posting[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PostingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('postings');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PostingTypes', [
            'foreignKey' => 'posting_type_id'
        ]);
        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->add('artifact_id', ['artifactIdCheck' => ['rule' => 'artifactIdCheck', 'provider' => 'table']]);

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->add('slug', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->boolean('published')
            ->allowEmptyString('published');

        $validator
            ->scalar('title');

        $validator
            ->scalar('body')
            ->maxLength('body', 16777215);

        $validator
            ->scalar('lang')
            ->maxLength('lang', 3);

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->scalar('modified_by')
            ->maxLength('modified_by', 45)
            ->allowEmptyString('modified_by');

        $validator
            ->dateTime('publish_start')
            ->allowEmptyDateTime('publish_start');

        $validator
            ->dateTime('publish_end')
            ->allowEmptyDateTime('publish_end');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->isUnique(['slug']), ['errorField' => 'slug']);
        $rules->add($rules->existsIn(['posting_type_id'], 'PostingTypes'), ['errorField' => 'posting_type_id']);

        return $rules;
    }

    public function artifactIdCheck($value)
    {
        $value = strtolower($value);
        $value = ltrim($value, 'p');
        $artifacts = TableRegistry::get('artifacts');
        $queryid = $artifacts
        ->find()
        ->select(['id'])
        ->where(['id =' => $value]);

        if ($value == "0") {
            return true;
        }

        foreach ($queryid as $artifact) {
            if ($artifact->id) {
                return true;
            }
            return false;
        }
    }
}
