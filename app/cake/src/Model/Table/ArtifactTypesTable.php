<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactTypes Model
 *
 * @property \App\Model\Table\ArtifactTypesTable&\Cake\ORM\Association\BelongsTo $ParentArtifactTypes
 * @property \App\Model\Table\ArtifactTypesTable&\Cake\ORM\Association\HasMany $ChildArtifactTypes
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\HasMany $Artifacts
 *
 * @method \App\Model\Entity\ArtifactType newEmptyEntity()
 * @method \App\Model\Entity\ArtifactType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactType get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifact_types');
        $this->setDisplayField('artifact_type');
        $this->setPrimaryKey('id');

        $this->belongsTo('ParentArtifactTypes', [
            'className' => 'ArtifactTypes',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildArtifactTypes', [
            'className' => 'ArtifactTypes',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('Artifacts', [
            'foreignKey' => 'artifact_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('artifact_type')
            ->maxLength('artifact_type', 45)
            ->requirePresence('artifact_type', 'create')
            ->notEmptyString('artifact_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['parent_id'], 'ParentArtifactTypes'), ['errorField' => 'parent_id']);

        return $rules;
    }
}
