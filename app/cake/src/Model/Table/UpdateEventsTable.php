<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UpdateEvents Model
 *
 * @property \App\Model\Table\ExternalResourcesTable&\Cake\ORM\Association\BelongsTo $ExternalResources
 * @property \App\Model\Table\InscriptionsTable&\Cake\ORM\Association\HasMany $Inscriptions
 * @property \App\Model\Table\ArtifactsUpdatesTable&\Cake\ORM\Association\HasMany $ArtifactsUpdates
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsToMany $Authors
 *
 * @method \App\Model\Entity\UpdateEvent newEmptyEntity()
 * @method \App\Model\Entity\UpdateEvent newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\UpdateEvent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UpdateEvent get($primaryKey, $options = [])
 * @method \App\Model\Entity\UpdateEvent findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\UpdateEvent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UpdateEvent[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\UpdateEvent|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UpdateEvent saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UpdateEventsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('update_events');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ExternalResources', [
            'foreignKey' => 'external_resource_id'
        ]);
        $this->hasMany('Inscriptions', [
            'foreignKey' => 'update_event_id'
        ]);
        $this->hasMany('ArtifactsUpdates', [
            'foreignKey' => 'update_event_id'
        ]);
        $this->belongsToMany('Authors', [
            'foreignKey' => 'update_event_id',
            'targetForeignKey' => 'author_id',
            'joinTable' => 'authors_update_events'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->nonNegativeInteger('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->scalar('update_type')
            ->allowEmptyString('update_type');

        $validator
            ->scalar('event_comments')
            ->allowEmptyString('event_comments');

        $validator
            ->nonNegativeInteger('approved_by')
            ->allowEmptyString('approved_by');

        $validator
            ->scalar('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['external_resource_id'], 'ExternalResources'), ['errorField' => 'external_resource_id']);

        return $rules;
    }
}
