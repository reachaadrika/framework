<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RetiredArtifacts Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $NewArtifacts
 *
 * @method \App\Model\Entity\RetiredArtifact newEmptyEntity()
 * @method \App\Model\Entity\RetiredArtifact newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\RetiredArtifact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RetiredArtifact get($primaryKey, $options = [])
 * @method \App\Model\Entity\RetiredArtifact findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\RetiredArtifact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RetiredArtifact[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\RetiredArtifact|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RetiredArtifact saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RetiredArtifact[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RetiredArtifact[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\RetiredArtifact[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RetiredArtifact[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class RetiredArtifactsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('retired_artifacts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->belongsTo('NewArtifacts', [
            'foreignKey' => 'new_artifact_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('retired_by')
            ->requirePresence('retired_by', 'create')
            ->notEmptyString('retired_by');

        $validator
            ->scalar('retired_for')
            ->allowEmptyString('retired_for');

        $validator
            ->boolean('is_public')
            ->allowEmptyString('is_public');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'), ['errorField' => 'artifact_id']);
        $rules->add($rules->existsIn(['new_artifact_id'], 'Artifacts'), ['errorField' => 'new_artifact_id']);

        return $rules;
    }
}
