<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Abbreviations Model
 *
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\BelongsTo $Publications
 *
 * @method \App\Model\Entity\Abbreviation newEmptyEntity()
 * @method \App\Model\Entity\Abbreviation newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Abbreviation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Abbreviation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Abbreviation findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Abbreviation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Abbreviation[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Abbreviation|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Abbreviation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Abbreviation[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Abbreviation[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Abbreviation[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Abbreviation[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class AbbreviationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('abbreviations');
        $this->setDisplayField('abbreviation');
        $this->setPrimaryKey('id');

        $this->belongsTo('Publications', [
            'foreignKey' => 'publication_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('abbreviation')
            ->maxLength('abbreviation', 100)
            ->requirePresence('abbreviation', 'create')
            ->notEmptyString('abbreviation');

        $validator
            ->scalar('fullform')
            ->allowEmptyString('fullform');

        $validator
            ->scalar('type')
            ->allowEmptyString('type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['publication_id'], 'Publications'), ['errorField' => 'publication_id']);

        return $rules;
    }
}
