<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

/**
 * Authors Model
 *
 * @property \App\Model\Table\SignReadingsCommentsTable&\Cake\ORM\Association\HasMany $SignReadingsComments
 * @property \App\Model\Table\StaffTable&\Cake\ORM\Association\HasMany $Staff
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\HasMany $Users
 * @property \App\Model\Table\ArticlesTable&\Cake\ORM\Association\BelongsToMany $Articles
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\BelongsToMany $Publications
 * @property \App\Model\Table\UpdateEventsTable&\Cake\ORM\Association\BelongsToMany $UpdateEvents
 *
 * @method \App\Model\Entity\Author newEmptyEntity()
 * @method \App\Model\Entity\Author newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Author[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Author get($primaryKey, $options = [])
 * @method \App\Model\Entity\Author findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Author patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Author[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Author|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Author saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AuthorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('authors');
        $this->setDisplayField('author');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('SignReadingsComments', [
            'foreignKey' => 'author_id'
        ]);
        $this->hasMany('Staff', [
            'foreignKey' => 'author_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'author_id'
        ]);
        $this->belongsToMany('Articles', [
            'foreignKey' => 'author_id',
            'targetForeignKey' => 'article_id',
            'joinTable' => 'articles_authors'
        ]);
        $this->belongsToMany('Publications', [
            'foreignKey' => 'author_id',
            'targetForeignKey' => 'publication_id',
            'joinTable' => 'authors_publications'
        ]);

        $this->belongsToMany('UpdateEvents', [
            'foreignKey' => 'author_id',
            'targetForeignKey' => 'update_event_id',
            'joinTable' => 'authors_update_events'
            ]);

        $this->addBehavior('Timestamp');
    }

    /**
     * Convert input data to required format.
     *
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        foreach ($data as $key => $value) {
            if ($key != '_joinData') {
                $data[$key] = trim($value);
            }
        }

        if (isset($data['first']) and isset($data['last'])) {
            if ($data['first'] == '') {
                $data['author'] = $data['last'];
            } elseif ($data['last'] == '') {
                $data['author'] = $data['first'];
            } else {
                if ($data['east_asian_order']) {
                    $data['author'] = $data['last'].' '.$data['first'];
                } else {
                    $data['author'] = $data['last'].', '.$data['first'];
                }
            }
        }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('author')
            ->maxLength('author', 300)
            ->requirePresence('author', 'create')
            ->notEmptyString('author');

        $validator
            ->scalar('last')
            ->maxLength('last', 150)
            ->allowEmptyString('last');

        $validator
            ->scalar('first')
            ->maxLength('first', 150)
            ->allowEmptyString('first');

        $validator
            ->boolean('east_asian_order')
            ->notEmptyString('east_asian_order');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->scalar('institution')
            ->maxLength('institution', 255)
            ->allowEmptyString('institution');

        $validator
            ->scalar('orcid_id')
            ->maxLength('orcid_id', 16, 'The ORCID ID needs to be a 16 digit number')
            ->minLength('orcid_id', 16, 'The ORCID ID needs to be a 16 digit number')
            ->allowEmpty('orcid_id');

        $validator
            ->boolean('deceased')
            ->notEmptyString('deceased');

        return $validator;
    }
}
