<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactsGenres Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\GenresTable&\Cake\ORM\Association\BelongsTo $Genres
 *
 * @method \App\Model\Entity\ArtifactsGenre newEmptyEntity()
 * @method \App\Model\Entity\ArtifactsGenre newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsGenre[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsGenre get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsGenre findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactsGenre patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsGenre[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsGenre|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsGenre saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsGenre[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsGenre[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsGenre[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsGenre[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactsGenresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifacts_genres');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->belongsTo('Genres', [
            'foreignKey' => 'genre_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('is_genre_uncertain')
            ->allowEmptyString('is_genre_uncertain');

        $validator
            ->scalar('comments')
            ->allowEmptyString('comments');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'), ['errorField' => 'artifact_id']);
        $rules->add($rules->existsIn(['genre_id'], 'Genres'), ['errorField' => 'genre_id']);

        return $rules;
    }
}
