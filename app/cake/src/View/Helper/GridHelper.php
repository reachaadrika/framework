<?php
namespace App\View\Helper;

use Cake\View\Helper;

class GridHelper extends Helper
{
    public function Alphabetical($arr)
    {
        $prev = array_values($arr)[0][0];
        $render = ["<div class='alphabetical-anchors'><div><span>".$prev."</span>"];
        foreach ($arr as $element => $key) {
            if ($prev != $key[0]) {
                $prev = $key[0];
                array_push($render, "</div><div><span>".$key[0]."</span>");
            }
            array_push($render, "<a href='/{$key}'>".$element."</a>");
        }

        array_push($render, "</div></div>");

        foreach ($render as $r) {
            echo $r;
        }
    }
}
