<?php
namespace App\View\Helper;

use App\Model\Entity\Inscription;
use Cake\Cache\Cache;
use Cake\Http\Client;
use Cake\Http\Exception\HttpException;
use Cake\ORM\Entity;
use Cake\View\Helper;

class ScriptsHelper extends Helper
{
    /**
     * Convert data via an external script.
     *
     * @param string $url
     * @param array $data
     * @return string
     * @throws \Cake\Http\Exception\HttpException
     */
    private function _post(string $url, array $data): string
    {
        $http = new Client();
        $response = $http->post(
            $url,
            json_encode($data),
            ['type' => 'json']
        );

        $status = $response->getStatusCode();
        $body = $response->getStringBody();
        if ($status >= 400) {
            throw new HttpException($body, $status);
        }

        return $body;
    }

    /**
     * Convert data via an external script.
     *
     * @param string $url
     * @param array $data
     * @param string [$key]
     * @return string
     * @throws \Cake\Http\Exception\HttpException
     */
    private function post(string $url, array $data, ?string $key = null): string
    {
        if (empty($key)) {
            return $this->_post($url, $data);
        } else {
            return Cache::remember($key, function () use ($url, $data) {
                return $this->_post($url, $data);
            });
        }
    }

    /**
     * Format any data into a reference.
     *
     * Option and format documentation: see /app/tools/citation.js/README.md.
     *
     * @param array $data
     * @param string $format
     * @param array $options
     * @return string
     * @throws \Cake\Http\Exception\HttpException
     */
    public function formatReference(array $data, string $format, array $options): string
    {
        $url = sprintf('http://node-tools:3000/format/' . $format . '?' . http_build_query($options));

        return $this->post($url, $this->removeFields($data, [
            'content_html',
            'content_latex',
            'atf'
        ]));
    }

    /**
     * Get CoNLL-RDF from an inscription.
     *
     * Option array may contain the base URL.
     *
     * @param \App\Model\Entity\Inscription $data
     * @param array $options
     * @return string
     * @throws \Cake\Http\Exception\HttpException
     */
    public function formatConllRdf(Inscription $data, array $options): string
    {
        $url = 'http://node-tools:3001/format/rdf?' . http_build_query($options);
        $data = array_intersect_key($data->toArray(), array_flip([
            'annotation',
            'id',
            'artifact_id'
        ]));

        // return $this->post($url, $data);
        return $this->post($url, $data, 'conll-rdf.' . $data['id']);
    }

    /**
     * Get CoNLL-U from an inscription.
     *
     * @param \App\Model\Entity\Inscription $data
     * @return string
     * @throws \Cake\Http\Exception\HttpException
     */
    public function formatConllU(Inscription $data): string
    {
        $url = 'http://node-tools:3002/format/u';
        $data = array_intersect_key($data->toArray(), array_flip([
            'annotation',
            'id',
            'artifact_id'
        ]));

        return $this->post($url, $data, 'conll-u.' . $data['id']);
    }

    /**
     * Remove certain fields from associative arrays.
     */
    private function removeFields(array $data, array $keysToRemove): array
    {
        $keys = array_keys($data);
        if (array_keys($keys) !== $keys) {
            return $this->removeFields([$data], $keysToRemove)[0];
        }

        return array_map(function ($entry) use ($keysToRemove) {
            if (!empty($entry)) {
                if ($entry instanceof Entity) {
                    $entry = $entry->toArray();
                }

                if (!is_array($entry)) {
                    return $entry;
                }

                foreach ($keysToRemove as $key) {
                    unset($entry[$key]);
                }

                foreach ($entry as $key => $value) {
                    if (is_array($value)) {
                        $entry[$key] = $this->removeFields($value, $keysToRemove);
                    }
                }
            }

            return $entry;
        }, $data);
    }
}
