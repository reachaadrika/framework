<?php
namespace App\View;

use CsvView\View\CsvView as CsvView_;

class CsvView extends CsvView_
{
    use TableTrait;

    protected $_responseType = 'csv';

    /**
     * Returns data to be serialized.
     *
     * @param array|string|bool $serialize
     * @return string
     */
    protected function _serialize($serialize): string
    {
        $data = $this->_rowsToSerialize($this->viewVars['_serialize']);
        $this->set([
            'table' => $this->prepareTableData($data),
            '_header' => $this->prepareTableHeader($data),
            '_serialize' => 'table'
        ]);
        return parent::_serialize(null);
    }
}
