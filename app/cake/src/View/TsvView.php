<?php
namespace App\View;

use CsvView\View\CsvView;

class TsvView extends CsvView
{
    use TableTrait;

    protected $_responseType = 'tsv';

    /**
     * Returns data to be serialized.
     *
     * @param array|string|bool $serialize
     * @return string
     */
    protected function _serialize($serialize): string
    {
        $data = $this->_rowsToSerialize($this->viewVars['_serialize']);
        $this->set([
            'table' => $this->prepareTableData($data),
            '_header' => $this->prepareTableHeader($data),
            '_delimiter' => chr(9), // 0x09 TAB
            '_serialize' => 'table'
        ]);
        return parent::_serialize(null);
    }
}
