<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Archives Controller
 *
 * @property \App\Model\Table\ArchivesTable $Archives
 *
 * @method \App\Model\Entity\Archive[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArchivalsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
        $this->loadComponent('Fileservice');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    }

    /**
     * View method
     *
     * @param string|null $id Archive id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if ($this->Fileservice->checkFile((string)$id . '.jpg', 'archivals')) {
            $this->log($id . '.jpg', 'debug');
            $image = $this->Fileservice->getPresignedFile($id . '.jpg', 'archivals');
            $this->set('image', $image);
            $this->set('pno', $id);
            $this->set('result', true);
        } else {
            $this->set('pno', $id);
            $this->set('result', false);
        }
    }
}
