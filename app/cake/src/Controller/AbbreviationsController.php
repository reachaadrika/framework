<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Abbreviations Controller
 *
 * @property \App\Model\Table\AbbreviationsTable $Abbreviations
 *
 * @method \App\Model\Entity\Abbreviation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AbbreviationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => 'Publications',
            'limit' => 20,
            'order' => [
                'abbreviation' => 'ASC'
                ]
            ];

        $data = $this->request->getQueryParams();

        if (isset($data['letter'])) {
            $page = ($this->Abbreviations->find('all', ['order' => ['abbreviation' => 'ASC']])->where(['Abbreviations.abbreviation <=' => $data['letter']])->count() + 1) / $this->paginate['limit'];
            $page = ceil($page);
            $this->redirect('?page='.$page);
        }

        
        $abbreviations = $this->paginate($this->Abbreviations);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
        $this->set(compact('abbreviations', 'access_granted'));
    }

    /**
    * View method
    *
    * @param string|null $id Abbreviation id.
    * @return \Cake\Http\Response|null
    * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function view($id = null)
    {
        $abbreviation = $this->Abbreviations->get($id, [
            'contain' => 'Publications',
        ]);

        $this->set(compact('abbreviation'));
    }
}
