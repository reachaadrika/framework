<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class AdvancedsearchController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'ElasticSearch'
        $this->loadComponent('ElasticSearch');

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * beforeFilter method
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->session = $this->getRequest()->getSession();

        // Get search Settings for Session
        $this->searchSettings = $this->session->read('searchSettings');

        // Searchable parameters on view
        $this->searchableFields = [
            "pdesignation", "authors", "editors", "year",
            "title", "ptype", "publisher", "series",
            "atype", "materials", "collection", "provenience",
            "archive", "period", "acomments", "translation",
            "transliteration", "icomments", "structure", "genres",
            "languages", "adesignation", "museum_no", "accession_no",
            "id", "seal_no", "composite_no", "bibtexkey", "designation_exactref", "written_in", "transliteration_permutation"
        ];

        // Search Result View Settings
        $this->settings = [
            'LayoutType' => 1,
            'Page' => 1,
            'PageSize' => $this->searchSettings['PageSize'],
            'lastPage' => 0,
            'canViewPrivateArtifacts' => $this->GeneralFunctions->checkIfRolesExists([1, 4]) == 1 ? 1 : 0,
            'canViewPrivateInscriptions' => $this->GeneralFunctions->checkIfRolesExists([1, 5]) == 1 ? 1 : 0,
            'sortBy' => 'relevance',
            'filter_dirty' => 0
        ];

        $this->filters = [
            'collection' => [],
            'period' => [],
            'provenience' => [],
            'atype' => [],
            'materials' => [],
            'authors' => [],
            'year' => []
        ];

        // To store whole array result with key as searchId (as timestamp) in session vaiable
        // Max searchId to be store = 4
        if (is_null($this->session->read('resultsStored'))) {
            $this->session->write('resultsStored', []);
            $this->resultsStored = [];
        } else {
            $this->resultsStored = $this->session->read('resultsStored');
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 300);

        $queryData = $this->getDataFromRequest($this->getRequest()->getData());

        if (!empty($queryData)) {
            $searchId = time();

            //pass advanced search param to search controller
            $queryData = array_merge($queryData, ['advanced_search' => 1]);

            return $this->redirect([
                'controller' => 'Search',
                'action' => 'view',
                $searchId,
                '?' => $queryData
            ]);
        }
    }

    /**
     * getDataFromRequest method
     *
     *  It returns array of required Key=>Value Pair.
     *
     * @param
     * param : Type of Request i.e. data(URL params) or query(form POST) parameters.
     *
     * @return Array of Key=>Value pair.
     */
    public function getDataFromRequest($requestType)
    {
        $queryData = [];
        
        foreach ($requestType as $key => $value) {
            if ($value !== '') {
                if (array_key_exists($key, $this->settings)) {
                    $this->settings[$key] = $value;
                } elseif (in_array($key, $this->searchableFields)) {
                    $queryData[$key] = trim($value, '" || \'');
                }
            }
        }

        return $queryData;
    }
}
