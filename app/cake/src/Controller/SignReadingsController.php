<?php
declare(strict_types=1);

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * SignReadings Controller
 *
 * @property \App\Model\Table\SignReadingsTable $SignReadings
 *
 * @method \App\Model\Entity\SignReading[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SignReadingsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Periods', 'Proveniences', 'Languages', 'SignReadingsComments'],
        ];
        $signReadings = $this->paginate($this->SignReadings);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('signReadings', 'access_granted'));
    }
    /**
     * View method
     *
     * @param string|null $id Sign Reading id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function view($id=null, $id2=null)
    {
        if ($id=='preferred') {
            $this->paginate = [
                'contain' => ['Periods', 'Proveniences', 'Languages', 'SignReadingsComments'],
            ];
            $signReadings = $this->paginate($this->SignReadings);

            $this->set(compact('signReadings'));
            $this->render('/SignReadings/preferred');
        } elseif ($id=='view_preferred') {
            $signReading = $this->SignReadings->get($id2, [
                'contain' => ['Periods', 'Proveniences', 'Languages', 'SignReadingsComments'],
            ]);

            $this->set('signReading', $signReading);
            $this->render('/SignReadings/view_preferred');
        } else {
            $signReading = $this->SignReadings->get($id, [
                'contain' => ['Periods', 'Proveniences', 'Languages', 'SignReadingsComments'],
            ]);
            $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

            $this->set(compact('access_granted'));
            $this->set('signReading', $signReading);
        }
    }
}
