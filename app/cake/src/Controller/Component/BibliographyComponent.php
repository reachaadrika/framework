<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Event\Event;

class BibliographyComponent extends Component
{
    public $components = ['RequestHandler'];

    // Bibliographic content types (MIME types from https://citation.crosscite.org/docs.html#sec-4)
    public $types = [
        // w/ accepted file extension
        'bib' => 'application/x-bibtex',
        'ris' => 'application/x-research-info-systems',

        // no defined file extension
        'csljson' => 'application/vnd.citationstyles.csl+json',
        'biblatex' => 'application/x-biblatex',
        'bibliography' => 'text/x-bibliography'
    ];

    public function initialize(array $config): void
    {
        $controller = $this->_registry->getController();

        foreach ($this->types as $ext => $header) {
            $controller->getResponse()->setTypeMap($ext, $header);
        }

        $this->RequestHandler->setConfig([
            'viewClassMap.bib' => 'Bibtex',
            'viewClassMap.ris' => 'Ris',
            'viewClassMap.csljson' => 'CslJson',
            'viewClassMap.biblatex' => 'Biblatex',
            'viewClassMap.bibliography' => 'Bibliography'
        ]);
    }

    public function beforeRender(Event $event)
    {
        $type = $this->RequestHandler->prefers();
        if (!$this->requestsBibliography($type)) {
            return;
        }

        $controller = $event->getSubject();
        $controller->setResponse($controller->getResponse()->withType($type));

        $options = ['format' => 'text'];
        if ($type == 'bibliography') {
            $options['format'] = 'html';
            $style = $controller->getRequest()->getQuery('style');
            if (!empty($style)) {
                $options['template'] = $style;
            }
        }

        $controller->set('_format', $type);
        $controller->set('_options', $options);
    }

    public function requestsBibliography($type = null)
    {
        if (empty($type)) {
            $type = $this->RequestHandler->prefers();
        }

        return isset($this->types[$type]);
    }
}
