<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;

class GranularAccessComponent extends Component
{
    public $components = ['GeneralFunctions'];

    public function initialize(array $config): void
    {
        $this->Artifacts = TableRegistry::get('Artifacts');
    }

    /**
     * Load inscriptions and related artifacts when allowed.
     *
     * @param \App\Model\Entity\Artifact $artifact
     */
    public function amendArtifact($artifact)
    {
        // Load related artifacts (composite/seal and the reverse)
        if ($this->canViewPrivateArtifact()) {
            $this->Artifacts->loadInto($artifact, [
                'Witnesses',
                'Impressions',
                'Composites',
                'Seals'
            ]);
        } else {
            $this->Artifacts->loadInto($artifact, [
                'Witnesses' => ['conditions' => ['Witnesses.is_public' => true]],
                'Impressions' => ['conditions' => ['Impressions.is_public' => true]],
                'Composites' => ['conditions' => ['Composites.is_public' => true]],
                'Seals' => ['conditions' => ['Seals.is_public' => true]]
            ]);
        }

        // Load related inscriptions
        if ($artifact->is_atf_public || $this->canViewPrivateInscriptions()) {
            $this->Artifacts->loadInto($artifact, ['Inscriptions']);
        }

        if (is_array($artifact->witnesses)) {
            foreach ($artifact->witnesses as $witness) {
                if ($witness->is_atf_public || $this->canViewPrivateInscriptions()) {
                    $this->Artifacts->loadInto($witness, ['Inscriptions']);
                }
            }
        }
    }

    /**
     * @return bool
     */
    public function canViewPrivateArtifact()
    {
        return $this->GeneralFunctions->checkIfRolesExists([1, 4]);
    }

    /**
     * @return bool
     */
    public function canViewPrivateImage()
    {
        return $this->GeneralFunctions->checkIfRolesExists([1, 7]);
    }

    /**
     * @return bool
     */
    public function canViewPrivateInscriptions()
    {
        return $this->GeneralFunctions->checkIfRolesExists([1, 5]);
    }
}
