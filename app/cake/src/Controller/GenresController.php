<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Genres Controller
 *
 * @property \App\Model\Table\GenresTable $Genres
 *
 * @method \App\Model\Entity\Genre[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GenresController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('LinkedData');
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentGenres', 'ChildGenres']
        ];
        $genres = $this->paginate($this->Genres);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('genres', 'access_granted'));
        $this->set('_serialize', 'genres');
    }

    /**
     * View method
     *
     * @param string|null $id Genre id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $genre = $this->Genres->get($id, [
            'contain' => ['ParentGenres', 'ChildGenres'],
        ]);

        $artifacts = TableRegistry::get('ArtifactsGenres');
        $count = $artifacts->find('list', ['conditions' => ['genre_id' => $id]])->count();
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('genre', 'count', 'access_granted'));
        $this->set('_serialize', 'genre');
    }
}
