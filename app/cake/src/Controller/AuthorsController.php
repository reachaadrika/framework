<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Authors Controller
 *
 * @property \App\Model\Table\AuthorsTable $Authors
 *
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AuthorsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 10,
            'order' => [
                'author' => 'ASC'
                ]
            ];
            
        // Get page number for alphabetical pagination
        $data = $this->request->getQueryParams();
        //echo $data;
        if (isset($data['letter'])) {
            $page = ($this->Authors->find('all', ['order' => ['author' => 'ASC']])->where(['Authors.author <=' => $data['letter']])->count() + 1) / $this->paginate['limit'];
            $page = ceil($page);
            $this->redirect('?page='.$page);
        }

        $authors = $this->paginate($this->Authors);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
        $this->set(compact('authors', 'access_granted'));
    }

    /**
     * View method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $author = $this->Authors->get($id);

        $this->loadModel('Publications');
        $query = $this->Publications->find('all', [
            'contain' => [
                'EntryTypes',
                'Journals',
                'Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC']
                ],
                'Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC']
                ]
            ],
            'sort' => [
                'COALESCE(Publications.bibtexkey, "zz") ASC',
                'COALESCE(Publications.title, "zz") ASC',
                'COALESCE(Publications.designation, "zz") ASC'
            ]
        ]);

        $filter = ['Authors.author' => $author->author];
        $query = $query->innerJoinWith(
            'Authors',
            function ($q) use ($filter) {
                return $q->where($filter);
            }
        );
        $publications = $this->paginate($query);

        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
        $this->set(compact('author', 'access_granted', 'publications'));
    }
}
