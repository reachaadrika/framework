<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * MaterialColors Controller
 *
 * @property \App\Model\Table\MaterialColorsTable $MaterialColors
 *
 * @method \App\Model\Entity\MaterialColor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MaterialColorsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $materialColor = $this->MaterialColors->newEntity();
        if ($this->getRequest()->is('post')) {
            $materialColor = $this->MaterialColors->patchEntity($materialColor, $this->getRequest()->getData());
            if ($this->MaterialColors->save($materialColor)) {
                $this->Flash->success(__('The material color has been saved.'));

                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The material color could not be saved. Please, try again.'));
        }
        $this->set(compact('materialColor'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Material Color id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $materialColor = $this->MaterialColors->get($id, [
            'contain' => []
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $materialColor = $this->MaterialColors->patchEntity($materialColor, $this->getRequest()->getData());
            if ($this->MaterialColors->save($materialColor)) {
                $this->Flash->success(__('The material color has been saved.'));

                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The material color could not be saved. Please, try again.'));
        }
        $this->set(compact('materialColor'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Material Color id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $materialColor = $this->MaterialColors->get($id);
        if ($this->MaterialColors->delete($materialColor)) {
            $this->Flash->success(__('The material color has been deleted.'));
        } else {
            $this->Flash->error(__('The material color could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false,'action' => 'index']);
    }
}
