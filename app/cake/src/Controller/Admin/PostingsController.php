<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Postings Controller
 *
 * @property \App\Model\Table\PostingsTable $Postings
 *
 * @method \App\Model\Entity\Posting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostingsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->paginate = [
            'contain' => ['PostingTypes', 'Artifacts']
        ];
        $postings = $this->paginate($this->Postings);

        $this->set(compact('postings'));
    }

    /**
     * View method
     *
     * @param string|null $id Posting id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $posting = $this->Postings->get($id, [
            'contain' => ['PostingTypes', 'Artifacts']
        ]);

        $this->set('posting', $posting);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $posting = $this->Postings->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $data['artifact_id'] = strtolower($data['artifact_id']);
            $data['artifact_id'] = ltrim($data['artifact_id'], 'p');
            $posting = $this->Postings->patchEntity($posting, $data);
            if ($this->Postings->save($posting)) {
                $this->Flash->success(__('The posting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The posting could not be saved. Please, try again.'));
        }
        $lang = ['eng' => 'eng', 'fr' => 'fr', 'de' => 'de', 'es' => 'es', 'ar' => 'ar'];
        $postingTypes = $this->Postings->PostingTypes->find('list', ['limit' => 200]);
        $artifacts = $this->Postings->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('posting', 'postingTypes', 'artifacts', 'lang'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Posting id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $posting = $this->Postings->get($id, [
            'contain' => []
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $data = $this->getRequest()->getData();
            $data['artifact_id'] = strtolower($data['artifact_id']);
            $data['artifact_id'] = ltrim($data['artifact_id'], 'p');
            $posting = $this->Postings->patchEntity($posting, $data);
            if ($this->Postings->save($posting)) {
                $this->Flash->success(__('The posting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The posting could not be saved. Please, try again.'));
        }
        $lang = ['eng' => 'eng', 'fr' => 'fr', 'de' => 'de', 'es' => 'es', 'ar' => 'ar'];
        $postingTypes = $this->Postings->PostingTypes->find('list', ['limit' => 200]);
        $artifacts = $this->Postings->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('posting', 'postingTypes', 'artifacts', 'lang'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Posting id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $posting = $this->Postings->get($id);
        if ($this->Postings->delete($posting)) {
            $this->Flash->success(__('The posting has been deleted.'));
        } else {
            $this->Flash->error(__('The posting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
