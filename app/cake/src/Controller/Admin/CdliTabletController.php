<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Security;
use Cake\Routing\Router;

/**
 * CdliTablet Controller
 */
class CdliTabletController extends AppController
{
    /**
     * Intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('CdliTablet');
        $this->loadComponent('RequestHandler');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        // Check if Authenticated User
        if (!is_null($this->Auth->user())) {
            // Check if Admin
            if (in_array(1, $this->Auth->user('roles'))) {
                $this->Auth->allow(['index', 'view', 'viewEntry', 'add', 'edit', 'delete', 'deleteAll']);
            }
        } else {
            $this->Auth->deny(['index', 'view', 'viewEntry', 'add', 'edit', 'delete', 'deleteAll']);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Date to date search
        $start_date = $this->getRequest()->getQuery('start_date');
        $end_date = $this->getRequest()->getQuery('end_date');

        $conditions = [];
        if ($start_date && $end_date) {
            $conditions[] = [
                'DATE(CdliTablet.displaydate) >=' => $start_date,
                'DATE(CdliTablet.displaydate) <=' => $end_date,
            ];
        }

        // Search by title
        $key = $this->getRequest()->getQuery('key');

        if ($key) {
            $query = $this->CdliTablet->find('all')->where(['theme LIKE'=>'%'.$this->getRequest()->getQuery('key').'%']);
        } else {
            $query = $this->CdliTablet;
        }

        $this->paginate = [
            'limit' => 30,
            'order' => [
                'CdliTablet.displaydate' => 'ASC',
            ],
            'conditions' => $conditions // Date to date search
        ];

        $cdli_tablet = $this->paginate($query);

        $this->set(compact('cdli_tablet'));
    }

    /**
     * View method
     */
    public function view($displaydate)
    {
        $this->loadModel('CdliTablet');

        $cdli_tablet = $this->CdliTablet->find('all', array(
            'conditions' => array('CdliTablet.displaydate' => $displaydate)
        ));

        $this->set(compact('cdli_tablet'));
    }

    /**
     * Add method
     */
    public function add()
    {
        $this->loadModel('CdliTablet');
        $cdliTablet = '';

        if ($this->getRequest()->is('post')) {
            $displaydate = $this->getRequest()->getData('displaydate');
            $theme = $this->getRequest()->getData('theme');
            $shorttitle = $this->getRequest()->getData('shorttitle');
            $longtitle = $this->getRequest()->getData('longtitle');
            $shortdesc = $this->getRequest()->getData('shortdesc');
            $longdesc = $this->getRequest()->getData('longdesc');
            $createdby = $this->getRequest()->getData('createdby');

            if (!empty($this->getRequest()->getData('file')->getClientFilename())) {
                $filename = $this->getRequest()->getData('file')->getClientFilename();
                $url = Router::url('/', true) . 'img/tablets/' . $filename; // Path
                $uploadpath = 'webroot/img/tablets/';
                $uploadfile = $uploadpath . $filename;

                if ($this->getRequest()->getData('file')->moveTo($uploadfile)) {
                    $cdli_tablet_table = TableRegistry::get('CdliTablet');
                    $cdli_tablet = $cdli_tablet_table->newEntity($this->getRequest()->getData());
                    // Store the image path??
                    $cdli_tablet->imagefilename = $filename;
                    $cdli_tablet->displaydate = $displaydate;
                    $cdli_tablet->theme = $theme;
                    $cdli_tablet->shorttitle = $shorttitle;
                    $cdli_tablet->longtitle = $longtitle;
                    $cdli_tablet->shortdesc = $shortdesc;
                    $cdli_tablet->longdesc = $longdesc;
                    $cdli_tablet->createdby = $createdby;

                    if ($cdli_tablet_table->save($cdli_tablet)) {
                        $this->Flash->success('New entry has been saved successfully.');
                        $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error('New entry could not be saved. Please, try again.');
                    }
                } else {
                    $this->Flash->error('New entry could not be saved. Please, try again.');
                }
            }
            $this->set('cdli_tablet', $cdli_tablet);
        }
    }

    /*
     * Edit method
     */
    public function edit($id)
    {
        if ($this->getRequest()->is('post')) {
            $displaydate = $this->getRequest()->getData('displaydate');
            $theme = $this->getRequest()->getData('theme');
            $shorttitle = $this->getRequest()->getData('shorttitle');
            $longtitle = $this->getRequest()->getData('longtitle');
            $shortdesc = $this->getRequest()->getData('shortdesc');
            $longdesc = $this->getRequest()->getData('longdesc');
            $createdby = $this->getRequest()->getData('createdby');

            $cdli_tablet_table = TableRegistry::get('CdliTablet');
            $cdli_tablet = $cdli_tablet_table->get($id);
            $cdli_tablet->displaydate = $displaydate;
            $cdli_tablet->theme = $theme;
            $cdli_tablet->shorttitle = $shorttitle;
            $cdli_tablet->longtitle = $longtitle;
            $cdli_tablet->shortdesc = $shortdesc;
            $cdli_tablet->longdesc = $longdesc;
            $cdli_tablet->createdby = $createdby;

            if ($cdli_tablet_table->save($cdli_tablet)) {
                $this->Flash->success('Entry has been updated successfully.');
                $this->redirect(['action'=>'index']);
            } else {
                $this->Flash->success('Entry could not be updated. Please, try again.');
            }
        } else {
            $cdli_tablet_table = TableRegistry::get('CdliTablet')->find();
            $cdli_tablet = $cdli_tablet_table->where(['id'=>$id])->first();

            $this->set('displaydate', $cdli_tablet->displaydate);
            $this->set('theme', $cdli_tablet->theme);
            $this->set('shorttitle', $cdli_tablet->shorttitle);
            $this->set('longtitle', $cdli_tablet->longtitle);
            $this->set('shortdesc', $cdli_tablet->shortdesc);
            $this->set('longdesc', $cdli_tablet->longdesc);
            $this->set('createdby', $cdli_tablet->createdby);
            $this->set('id', $id);
        }
    }

    /**
     * Delete method
     */
    public function delete($id)
    {
        $cdli_tablet_table = TableRegistry::get('CdliTablet');
        $cdli_tablet = $cdli_tablet_table->get($id);

        if ($cdli_tablet_table->delete($cdli_tablet)) {
            $this->Flash->success('Entry has been deleted successfully.');
            $this->redirect(['action'=>'index']);
        } else {
            $this->Flash->error('Entry could not be deleted. Please, try again.');
        }
    }

    /**
     * Delete all method
     */
    public function deleteAll()
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $ids = $this->getRequest()->getData('ids');
        if ($this->CdliTablet->deleteAll(['CdliTablet.id IN'=>$ids])) {
            $this->Flash->success('All selected entries have been deleted successfully.');
            $this->redirect(['action'=>'index']);
        } else {
            $this->redirect(['action'=>'index']);
            $this->Flash->error('Selected entries could not be deleted. Please, try again.');
        }
    }
}
