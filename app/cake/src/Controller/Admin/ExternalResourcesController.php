<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ExternalResources Controller
 *
 * @property \App\Model\Table\ExternalResourcesTable $ExternalResources
 *
 * @method \App\Model\Entity\ExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExternalResourcesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $externalResource = $this->ExternalResources->newEntity();
        if ($this->getRequest()->is('post')) {
            $externalResource = $this->ExternalResources->patchEntity($externalResource, $this->getRequest()->getData());
            if ($this->ExternalResources->save($externalResource)) {
                $this->Flash->success(__('The external resource has been saved.'));

                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The external resource could not be saved. Please, try again.'));
        }
        $artifacts = $this->ExternalResources->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('externalResource', 'artifacts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $externalResource = $this->ExternalResources->get($id, [
            'contain' => []
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $externalResource = $this->ExternalResources->patchEntity($externalResource, $this->getRequest()->getData());
            if ($this->ExternalResources->save($externalResource)) {
                $this->Flash->success(__('The external resource has been saved.'));

                return $this->redirect(['prefix' => false,'action' => 'index']);
            }
            $this->Flash->error(__('The external resource could not be saved. Please, try again.'));
        }
        $artifacts = $this->ExternalResources->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('externalResource', 'artifacts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $externalResource = $this->ExternalResources->get($id);
        if ($this->ExternalResources->delete($externalResource)) {
            $this->Flash->success(__('The external resource has been deleted.'));
        } else {
            $this->Flash->error(__('The external resource could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false,'action' => 'index']);
    }
}
