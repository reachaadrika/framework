<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Exception\NotAcceptableException;

/**
 * Inscriptions Controller
 *
 * @property \App\Model\Table\InscriptionsTable $Inscriptions
 *
 * @method \App\Model\Entity\Inscription[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InscriptionsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GranularAccess');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('LinkedData');
        $this->loadComponent('Inscription');
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Artifacts',
                'Artifacts.Dates',
                'Artifacts.Languages',
                'Artifacts.Composites' => [
                    'conditions' => ['Composites.is_public' => true]
                ]
            ],
            'conditions' => [
                'Artifacts.is_public' => true,
                'Artifacts.is_atf_public' => true,
                'Inscriptions.is_latest' => true
            ]
        ];

        if ($this->GranularAccess->canViewPrivateInscriptions()) {
            unset($this->paginate['conditions']['Artifacts.is_public']);
            unset($this->paginate['conditions']['Artifacts.is_atf_public']);
            unset($this->paginate['contain']['Artifacts.Composites']['conditions']);
        }

        $inscriptions = $this->paginate($this->Inscriptions);

        $this->set(compact('inscriptions'));
        $this->set('_serialize', 'inscriptions');
    }

    /**
     * View method
     *
     * @param string|null $id Inscription id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $inscription = $this->Inscriptions->get($id, [
            'contain' => [
                'Artifacts',
                'Artifacts.Languages',
                'Artifacts.Dates',
                'Artifacts.Composites' => [
                    'conditions' => ['Composites.is_public' => true]
                ]
            ]
        ]);

        $artifact = $inscription->artifact;

        if ($this->GranularAccess->canViewPrivateInscriptions()) {
            unset($this->paginate['contain']['Artifacts.Composites']['conditions']);
        } elseif (!$artifact->is_public || !$artifact->is_atf_public) {
            throw new UnauthorizedException();
        }

        $type = $this->Inscription->getPreferredType($inscription);
        if (empty($type)) {
            throw new NotAcceptableException();
        }
        $this->RequestHandler->renderAs($this, $type);

        $this->set('inscription', $inscription);
        $this->set('_serialize', 'inscription');
    }
}
