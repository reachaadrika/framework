<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    //$routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    // Any controller will interpret the variable as id run through view action.

    // Router::scope('/abbreviations', function ($routes) {

    //     $routes->connect('/view/*', ['controller' => 'Abbreviations', 'action' => 'view']);
    //     $routes->fallbacks('InflectedRoute');
    // });

    $routes->addExtensions([
        'pdf',
        'json',

        // Linked data
        'xml',
        'rdf',
        'ttl',
        'nt',
        'jsonld',

        // Tables
        'csv',
        'tsv',
        'xlsx',

        // Bibliographies
        'bib',
        'ris'
    ]);

    // Permalinks
    $routes->connect('/:id', ['controller' => 'Artifacts', 'action' => 'resolve'])
        ->setPatterns(['id' => '[PQS]\d{6}|S\d{6}\.\d'])
        ->setPass(['id']);

    // Admin prefix
    Router::prefix('admin', function ($routes) {

        // Pages for each controller
        $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
        $routes->connect('/:controller/edit/*', ['action' => 'edit']);
        $routes->connect('/:controller/add/*', ['action' => 'add']);
        $routes->connect('/:controller/delete/*', ['action' => 'delete']);
        $routes->connect('/:controller/export/*', ['action' => 'export']);
        $routes->connect('/:controller/*', ['action' => 'view']);

        // All Users Profile Edit
        $routes->connect('/users/:username/edit', ['controller' => 'Users', 'action' => 'edit'], ['pass' => ['username']]);

        // Publications merge feature
        $routes->connect('/publications/merge-select', ['controller' => 'Publications', 'action' => 'mergeSelect']);
        $routes->connect('/publications/merge/*', ['controller' => 'Publications', 'action' => 'merge']);


        // All Users Profile Edit
        $routes->connect('/users/:username/edit', ['controller' => 'Users', 'action' => 'edit'], ['pass' => ['username']]);
        $routes->connect('/publications/autocomplete', ['controller' => 'Publications', 'action' => 'autocomplete']);
        // Dashboard
        $routes->connect('/dashboard', ['controller' => 'Home', 'action' => 'dashboard']);

        // Routes for Admin journals dashboard.
        $routes->connect('/articles/link/publications', ['controller' => 'Articles', 'action' => 'link_publications']);
        $routes->connect('/api/view/link/:article/:publication', ['controller' => 'Articles', 'action' => 'view_link']);
        $routes->connect('/api/link/:article/:publication', ['controller' => 'Articles', 'action' => 'complete_link']);
        $routes->connect('/api/view/links/:id/all/:type', ['controller' => 'Articles', 'action' => 'view_all_article_links']);
        $routes->connect('/api/unlink/:article/:publication', ['controller' => 'Articles', 'action' => 'complete_unlink']);
        $routes->connect('/api/get/:id/:type', ['controller' => 'Articles', 'action' => 'get_article_by_id']);
        $routes->connect('/api/suggest/:key/:type', ['controller' => 'Articles', 'action' => 'link_suggest']);
        $routes->connect('/articles/add/CDLN', ['controller' => 'Articles', 'action' => 'add_cdln']);
        $routes->connect('/articles/bib/upload', ['controller' => 'Articles', 'action' => 'bib_upload']);
        $routes->connect('/articles/add/CDLP', ['controller' => 'Articles', 'action' => 'add_cdlp']);
        $routes->connect('/articles/add/CDLJ', ['controller' => 'Articles', 'action' => 'add_cdlj']);
        $routes->connect('/articles/add/CDLB', ['controller' => 'Articles', 'action' => 'add_cdlb']);
        $routes->connect('/articles/edit/CDLN/:article', ['controller' => 'Articles', 'action' => 'edit_cdln'])
        ->setPass(['article']);
        $routes->connect('/articles/edit/CDLP/:article', ['controller' => 'Articles', 'action' => 'edit_cdlp'])
        ->setPass(['article']);
        $routes->connect('/articles/edit/CDLJ/:article', ['controller' => 'Articles', 'action' => 'edit_cdlj'])
        ->setPass(['article']);
        $routes->connect('/articles/edit/CDLB/:article', ['controller' => 'Articles', 'action' => 'edit_cdlb'])
        ->setPass(['article']);
        $routes->connect('/articles/image/manager/upload/:article', ['controller' => 'Articles', 'action' => 'image_manager_upload'])
        ->setPass(['article']);

        // Routes for Journals display at admin dashboard.
        $routes->connect('/articles/cdln', ['controller' => 'Articles', 'action' => 'index_cdln']);
        $routes->connect('/articles/cdlp', ['controller' => 'Articles', 'action' => 'index_cdlp']);
        $routes->connect('/articles/cdlj', ['controller' => 'Articles', 'action' => 'index_cdlj']);
        $routes->connect('/articles/cdlb', ['controller' => 'Articles', 'action' => 'index_cdlb']);

        $routes->connect('/articles/cdln/view/:cdln', ['controller' => 'Articles', 'action' => 'view_cdln']);
        $routes->connect('/articles/cdlj/view/:cdlj', ['controller' => 'Articles', 'action' => 'view_cdlj']);
        $routes->connect('/articles/cdln/view/:cdln/web', ['controller' => 'Articles', 'action' => 'view_cdln_web']);
        $routes->connect('/articles/cdlb/view/:cdlb/web', ['controller' => 'Articles', 'action' => 'view_cdlb_web']);
        $routes->connect('/articles/cdlj/view/:cdlj/web', ['controller' => 'Articles', 'action' => 'view_cdlj_web']);
        $routes->connect('/articles/cdlp/view/:cdlp', ['controller' => 'Articles', 'action' => 'view_cdlp']);
        $routes->connect('/articles/cdlb/view/:cdlb', ['controller' => 'Articles', 'action' => 'view_cdlb']);

        $routes->connect('/articles/convert/latex/html/:article', ['controller' => 'Articles', 'action' => 'convert_latex_to_html']) ->setPass(['article']);

        $routes->connect('/articles/convert/latex/content/:article', ['controller' => 'Articles', 'action' => 'convert_latex_content'])
        ->setPass(['article']);
        $routes->connect('/articles/write/latex/content/:article', ['controller' => 'Articles', 'action' => 'write_latex_content'])->setPass(['article']);

        $routes->connect('/articles/add/:article/upload', ['controller' => 'Articles', 'action' => 'upload_article_pdf'])
        ->setPass(['article']);
        $routes->connect('/articles/add/:article/latex/upload', ['controller' => 'Articles', 'action' => 'upload_article_latex'])
        ->setPass(['article']);
        $routes->connect('/articles/delete/:article', ['controller' => 'Articles', 'action' => 'delete_article'])
        ->setPass(['article']);

        // Routes for Admin Author Ajax.
        $routes->connect('/authors/search/:author', ['controller' => 'Authors', 'action' => 'author_search_ajax'])
        ->setPass(['author']);
        $routes->connect('/authors/add/:author', ['controller' => 'Authors', 'action' => 'add_author_ajax'])
        ->setPass(['author']);

        $routes->connect('/uploads/geturl', ['controller' => 'Uploads ', 'action' => 'geturl']);
        $routes->connect('/uploads/dashboard', ['controller' => 'Uploads ', 'action' => 'dashboard']);
        // Routes for Admin CdliTablet
        $routes->connect('/CdliTablet', ['controller' => 'CdliTablet', 'action' => 'index']);
        $routes->connect('/CdliTablet/view/*', ['controller' => 'CdliTablet', 'action' => 'view']);
        $routes->connect('/CdliTablet/add', ['controller' => 'CdliTablet', 'action' => 'add']);
        $routes->connect('/CdliTablet/edit/*', ['controller' => 'CdliTablet', 'action' => 'edit']);
        $routes->connect('/CdliTablet/deleteAll', ['controller' => 'CdliTablet', 'action' => 'deleteAll']);

        $routes->fallbacks('DashedRoute');
    });

    Router::prefix('editor', function ($routes) {
        // Because you are in the admin scope,
        // you do not need to include the /admin prefix
        // or the admin route element.
        $routes->fallbacks('DashedRoute');
    });

    // Add prefix rule for API
    Router::scope('/api', function ($routes) {
        $routes->setExtensions(['json']);
        $routes->resources('CdliTablet', ['path'=>'dailytablets']);
    });

    // Artifacts (see also controllers below)
    $routes->connect('/artifacts/browse', ['controller' => 'Artifacts', 'action' => 'browse']);
    $routes->connect('/artifacts/auth-image/*', ['controller' => 'Artifacts', 'action' => 'authImage']);
    $routes->connect('/artifacts/:id/inscription', [
        'controller' => 'Artifacts',
        'action' => 'resolveInscription'
    ])->setPass(['id']);
    $routes->connect('/artifacts/:id/inscription/:format', [
        'controller' => 'Artifacts',
        'action' => 'resolveInscription'
    ])->setPass(['id']);

    // Routes for Journals.
    $routes->connect('/articles/:type', ['controller' => 'Articles', 'action' => 'index'])->setPass(['type']);
    $routes->connect('/articles/:type/:id', ['controller' => 'Articles', 'action' => 'view'])->setPass(['id']);
    $routes->connect('/articles/image/:id/:file', ['controller' => 'Articles', 'action' => 'image'])->setPass(['id', 'file']);

    // Routes for Staff Image.
    $routes->connect('/webroot/staff-img/:id', ['controller' => 'Staff', 'action' => 'image'])->setPass(['id']);

    // Routes for CdliTablet
    $routes->connect('/CdliTablet', ['controller'=>'CdliTablet', 'action'=>'index']);
    $routes->connect('/CdliTablet/view/*', ['controller'=>'CdliTablet', 'action'=>'view']);

    // Visualization routes
    $routes->connect('/AdvancedSearch/getUpdatedLineChartData', ['controller' => 'AdvancedSearch', 'action' => 'getUpdatedLineChartData']);
    $routes->connect('/stats/:table', ['controller' => 'Stats', 'action' => 'index'], ['pass' => ['table']]);
    $routes->connect('/viz/:action', ['controller' => 'Viz']);
    $routes->connect('/heatmap/post', ['controller' => 'Heatmap', 'action' => 'post']);

    // For User Controller
    $routes->connect('/users/profile/*', ['controller' => 'Users', 'action' => 'profile']);
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/register', ['controller' => 'Users', 'action' => 'register']);

    //For Contribute Page
    $routes->connect('/contribute', ['controller' => 'Postings', 'action'=>'view', ['id'=>190]]);

    // Documentation
    $routes->connect('/docs/*', ['controller' => 'Pages', 'action' => 'display']);

    // Pages for each controller
    $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
    $routes->connect('/:controller/:id/:format', ['action' => 'view'])->setPass(['id']);
    $routes->connect('/:controller/*', ['action' => 'view']);

    // Home page
    $routes->connect('/', ['controller' => 'Home']);

    // Browse
    $routes->connect('/browse', ['controller' => 'Home', 'action' => 'browse']);

    // Forgot Controller
    $routes->connect('/forgot/:type', ['controller' => 'Forgot', 'action' => 'index'], ['pass' => ['type']]);
    // Password Reset (New Password Page)
    $routes->connect('/forgot/password/reset/:token', ['controller' => 'Forgot', 'action' => 'password'], ['pass' => ['token']]);
    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
    */
    //Set up router for REST API
    $routes->resources('Artifacts');
    $routes->resources('CdliTablet');

    $routes->fallbacks('DashedRoute');
    // Routes for 3dviewer
    $routes->connect('/3dviewer/:id', ['controller'=>'Threedviewer', 'action'=>'index'], ['pass' => ['id']]);
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */

// Plugin::routes();            // Raises Deprecated Warning as no longer requirement to call `Plugin::routes()` after upgrading to use Http\Server
