<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QueriesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QueriesTable Test Case
 */
class QueriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\QueriesTable
     */
    protected $Queries;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Queries',
        'app.Assocs',
        'app.Stages',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Queries') ? [] : ['className' => QueriesTable::class];
        $this->Queries = $this->getTableLocator()->get('Queries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Queries);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\QueriesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\QueriesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     * @uses \App\Model\Table\QueriesTable::defaultConnectionName()
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
