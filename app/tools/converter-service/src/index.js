const citation = require('./citation')
const conllRdf = require('./conll-rdf')
const conllU = require('./conll-u')

citation.app.listen(3000, () => console.log(`3000: ${citation.message}`))
conllRdf.app.listen(3001, () => console.log(`3001: ${conllRdf.message}`))
conllU.app.listen(3002, () => console.log(`3002: ${conllU.message}`))
