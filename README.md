<div width="200" align="center"><img src="webroot/images/cdlilogo.svg" width="300"/></div>

## CDLI Framework

[![pipeline status](https://gitlab.com/cdli/framework/badges/phoenix/develop/pipeline.svg)](https://gitlab.com/cdli/framework/-/commits/phoenix/develop)  [![Donate CDLI](https://img.shields.io/badge/Donate-CDLI-brightgreen)](https://opencollective.com/cdli/donate)

**NOTE:** This branch is for Phoenix (aka "Framework 2.0") development only. For the current "1.0" codebase, see the mainline [master](https://gitlab.com/cdli/framework/tree/master) or [develop](https://gitlab.com/cdli/framework/tree/develop) branches of this repository.

Follow these Instructions to install the framework on your local machine.
* For local development/Setup  :  [FRAMEWORK_INSTALL.md](FRAMEWORK_INSTALL.md). 
* Front-end developement/Setup  :  [dev/README.md](dev/README.md).
* Testing CLI Framework  :  [tests/README.md](tests/README.md)
* Brat Servlet  :  [app/brat/tools/servlet/README.md](app/brat/tools/servlet/README.md)


## Repository layout

The following is a non-exhaustive list of folders explaining the general layout strategy of the repository:

* `/` : Repo root *(mounted as '/srv' in container filesystem)*
* `/app` : Web applications and tools *(live code, served at specified routes)*
    * `/brat`  :  Annotation GUI server
        * `/client`  :  Javascript Libraries
        * `/server`  :  Backend Server files
        * `/static`  :  Static files (`img/`, `fonts/` etc.)
    * `/cake`  :  CDLI Framerwork's new heart
        * `/config`  :  Configuration files
        * `/src`  :  CakePHP source files (`Template/`, `View/` etc.)
        * `/test`  :  Application test
    * `/common` : Common/shared application files
  	* `/private` : Private static files
        * `/logs` : logs directory for cake
	* `/tools` : Contains helper Python tools like "upload"
* `/dev` : CLI/scripts for development environment.
    * `/assets`  :  Bootstrap and Scss Styles
* `/tests` : Lint tests for ( Php, Sass, etc.)
* `/webroot` : Public static files *(served by webserver as default `/`)*
    * `/assets` : web assets (`css/`, `js/`, etc.)
    * `/bulk_data` : CDLI bulk data *(stub)*
    * `/dl` : CDLI artifact images *(stub)*
    * `/files` : other static assets
    * `/images` : Static image assets
    * `/pubs` : Static CDLI publication documents/PDFS


## Submodules

There are submodules in this repository and will be cloned as empty directories. 
To populate these submodules, you need to initialize and update them with the command:

`git submodule update --init --recursive`


## Containerization

* Every app in the `/app` folder will run in its own Docker container.

* Each app container will follow the same filesystem layout, but internally will only contain/have access to appropriate relevant folders (i.e. every container should see at least `/app/common/` and `/app/[myapp]/`).

* Since the user-facing webserver container (nginx) handles static assets, most containers should not even need to see `/webroot`.

* Developers can work on individual apps on their local machines using the provided Development CLI. Using Docker Compose, this provisions a virtual set of infrastructure containers (nginx, mariadb, etc) that mirror the configuration of the live servers, while allowing users to modify app source files directly without building container images. More info is available in the [CLI's README](dev/README.md).


## Contribution & Development

**Want to contribute to CDLI?**  [Join the Community](https://docs.google.com/forms/d/e/1FAIpQLSeHPVb0actA8DDE1NFLgDCCYs2F2HbYy563MJFJcbFYyy3ivQ/viewform?usp=sf_link).

* Since all the apps will share one repository, it is crucial that developers employ good Git practices.
* The Git workflow that this project will adhere to (more or less) is called *Git Flow*. A longer explanation of it (with charts!) is available [here](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). The basic summary with respect to the Phoenix/Version 2 branches is:
  * The `phoenix/develop` branch is for merging feature branches and applying fixes. It may not always be stable, but should aim for "Beta" quality stability. Significant new development should never occur directly in the `phoenix/develop` branch.
  * Development on individual apps, features, or parts of the repository should take place in a branch of `phoenix/develop` named `phoenix/feature/[app]` (or `phoenix/feature/[app]_[description]`). These branches are unprotected and can be modified by anyone with "Developer" access to the repository.
  * Once a `feature/` branch is working, a pull request (aka "merge request") can be made to merge it back into the `phoenix/develop` branch.
  * Since the `phoenix/*` branches are meant for the development of a large-scale update to the Framework codebase and apps, _there is no `phoenix/master` branch_. When ready for release, the `phoenix/develop` code will be merged into the mainline (`develop` and `master`) branches of the repository.
* If multiple developers are working on a single app, it is important to coordinate to ensure that they are working in different sub-branches and that their changes may be merged with one another cleanly.
* Only people with "Master" repository access will be able merge/push to the `phoenix/develop` branch. They will handle merging in updates from the app branches.
* Read following guides before :  
    * Issue a Bug  :  [Bug.md](.gitlab/issue_templates/Bug.md)
    * Adding a Feature  :  [Feature.md](.gitlab/issue_templates/Feature.md)
    * Merge Request  :  [Merge.md](.gitlab/merge_request_templates/Merge.md)


## License & Copyright

Non-software components (including publications, collections, and associated data) are copyright their respective owners and no license is implied except where provided in the relevant folders. Unless otherwise noted, all original code and documentation is licensed under the [MIT License](LICENSE.txt).

## Bibliography formatting

Formatting bibliographies is done with [Citation.js](https://citation.js.org)
which in turn uses [citeproc-js](https://github.com/Juris-M/citeproc-js)
and styles from the [Citation Style Language](https://citationstyles.org/)
project. citeproc-js is licensed under [CPAL/AGPLv3](https://github.com/Juris-M/citeproc-js/blob/master/LICENSE)
with [a special note](https://github.com/Juris-M/citeproc-js/issues/77) about the
'infectiousness' of the license. It gets installed when the `node-tools` Docker
container is built. The CSL styles are licensed under [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/).
It gets installed when the git submodule in `dev/assets/csl-styles` is loaded.


## Links

- [CakePHP Application Skeleton](app/cake/README.md)

- [API development guide](dev/docs/API.md)

- [SEO development guide](dev/docs/SEO.md)

- [Join the community](https://docs.google.com/forms/d/e/1FAIpQLSeHPVb0actA8DDE1NFLgDCCYs2F2HbYy563MJFJcbFYyy3ivQ/viewform?usp=sf_link)


    ### Tools

    - [Towards building the first Semantic Role Labeling(SRL) system for Sumerian](https://github.com/cdli-gh/Semantic-Role-Labeler/blob/222cd0b72db87a391205d905415d66cae04059ef/README.md)

    - [Morphology Pre-annotation Tool](https://github.com/cdli-gh/morphology-pre-annotation-tool/blob/98923fda721741e97dc95bc2fb850eddf35878f2/README.md)

    - [ATF2CONLL Convertor](https://github.com/cdli-gh/atf2conll-convertor/blob/85613eab49297556de9ad2f88dd89d38785176eb/README.md)

    - [CQP4RDF](https://github.com/cdli-gh/cqp4rdf/tree/abb72d24aa3cf2f22b5d5c867a24563f165eeef2)

        - [CQP4RDF](https://github.com/cdli-gh/cqp4rdf/blob/abb72d24aa3cf2f22b5d5c867a24563f165eeef2/README.md)

        - [CDLI-CoNLL to RDF data conversion](https://github.com/cdli-gh/cqp4rdf/blob/abb72d24aa3cf2f22b5d5c867a24563f165eeef2/data-converter/README.md)

        - [Instructions to use the GUI for CQP4RDF](https://github.com/cdli-gh/cqp4rdf/blob/abb72d24aa3cf2f22b5d5c867a24563f165eeef2/QueryGeneratorTutorial.md)

        - [CQP Dialect used for CQP2SPARQL](https://github.com/cdli-gh/cqp4rdf/blob/abb72d24aa3cf2f22b5d5c867a24563f165eeef2/docs/cqp_dialect.md)

        - [CQP2SPARQL Web](https://github.com/cdli-gh/cqp4rdf/blob/abb72d24aa3cf2f22b5d5c867a24563f165eeef2/docs/web_interface.md)

    - [Brat standoff to CDLI-CoNLL Converter](https://github.com/cdli-gh/brat_to_cdli_conll_converter/blob/5278af5aa1f7db8eb32574a84a37b4d7420dd23f/README.md)

    - [CoNLL-U format library for Python](https://github.com/cdli-gh/conllu.py/blob/725501d880e807179badca5c26bbc27621bc165a/README.md)

    - [Pyoracc](https://github.com/cdli-gh/pyoracc/blob/acebd8f3cb4d22d55a15c42694adb442f3b7b6aa/README.md)

    - [CDLI-CoNLL-to-CoNLLU-Converter](https://github.com/cdli-gh/CDLI-CoNLL-to-CoNLLU-Converter/blob/2131fc85b01c41aed1762ab2fc652deaa486af53/README.md)

    - [Commodity data visualization](https://github.com/MrLogarithm/cdli-accounting-viz/blob/master/README.md)

    - [Open Journal Systems(ojs)](https://github.com/cdli-gh/ojs/blob/main/README.md)
